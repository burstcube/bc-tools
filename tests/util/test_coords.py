from bctools.util.coords import *

import bctools.util.units as u

from bctools.exceptions import ParsingError

import pytest
from pytest import approx

from math import sqrt

def test_spherical():

    sphe = SphericalCoords(45*u.deg, 45*u.deg, 2)

    cart = sphe.cartesian

    assert cart.x == approx(1)
    assert cart.y == approx(1)
    assert cart.z == approx(sqrt(2))

    
def test_cartesian():

    cart = CartesianCoords(1, 1, sqrt(2))

    sphe = cart.spherical

    assert sphe.theta == approx(45*u.deg)
    assert sphe.phi == approx(45*u.deg)
    assert sphe.radius == approx(2)

def test_spacrecraft_coords():

    coords = SpacecraftCoords.from_megalib(["PointSource", '0.5', '0.5', '0.70710678118'])

    assert coords.zenith == approx(45*u.deg)
    assert coords.azimuth == approx(45*u.deg)
    assert coords.distance == approx(1)

    #Make sure there are enough sigfigs for at least double precision
    assert all([a==b for a,b in zip(coords.to_megalib(),
                                   ['RestrictedPoint',
                                    '{:.17E}'.format(coords.cartesian.x),
                                    '{:.17E}'.format(coords.cartesian.y),
                                    '{:.17E}'.format(coords.cartesian.z)])])
    
    coords = SpacecraftCoords.from_megalib(["FarFieldPointSource", '45', '45'])

    assert coords.zenith == approx(45*u.deg)
    assert coords.azimuth == approx(45*u.deg)
    assert coords.distance == u.inf

    assert all([a==b for a,b in zip(coords.to_megalib(),
                                   ['FarFieldPointSource',
                                    '{:.17E}'.format(coords.zenith/u.deg),
                                    '{:.17E}'.format(coords.azimuth/u.deg)])])
    
    #Exceptions
    with pytest.raises(TypeError):
        SpacecraftCoords.from_megalib("PointSource")

    with pytest.raises(ParsingError):
        SpacecraftCoords.from_megalib(["PointSource", '1'])

    with pytest.raises(ParsingError):
        SpacecraftCoords.from_megalib(["FarFieldPointSource", '1'])

    with pytest.raises(ParsingError):
        SpacecraftCoords.from_megalib([])

    with pytest.raises(ParsingError):
        SpacecraftCoords.from_megalib(["PointySource", '1', '1', '1'])
