from bctools.config import Configurator

import pytest

import io
from contextlib import redirect_stdout

from pathlib import Path

def test_configurator():

    # Get config example. This just checks that it exists
    config_example = Configurator.config_example_path()

    assert config_example.exists()
    
    #Check that user can get it from commandline
    f = io.StringIO()
    with redirect_stdout(f):
        Configurator.print_config_example_path([])
        assert str(config_example) == f.getvalue().rstrip()
    
    config  = Configurator(config_example)

    # [] operator getter
    assert config['simulations:geometry'] == "geometry/BurstCube.geo.setup"

    with pytest.raises(KeyError):
        config['simulation:doesntexist']

    # [] operator setter
    config['simulations:geometry'] = "/path/to/new/geometry.geo"

    assert config['simulations:geometry'] == "/path/to/new/geometry.geo"

    with pytest.raises(KeyError):
        config['simulationdoesntexist:geometry'] = 5

    with pytest.raises(KeyError):
        config['simulations:doesntexist'] = 5

    # Using override
    config.override("simulations:geometry = geometry/BurstCube.geo.setup",
                    ['simulations:spectrum:args:norm=2',
                    'simulations:spectrum:args:index=3'])

    assert config['simulations:geometry'] == "geometry/BurstCube.geo.setup"

    assert config['simulations:spectrum:args:norm'] == 2

    assert config['simulations:spectrum:args:index'] == 3

    # Absolute path
    assert config.absolute_path("rel_path") == (config_example.parent / "rel_path").resolve()

    assert config.absolute_path("/abs/path") == Path("/abs/path")
    
    # Dump
    # Not really a test, but at least check that it doesn't break anything
    config.dump()
    
