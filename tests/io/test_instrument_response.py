import bctools.data as data
from bctools.io import InstrumentResponse
from bctools.io.InstrumentResponse import rsp_extractor
from bctools.util.coords import SpacecraftCoords
from bctools.sims import DRM
import bctools.util.units as u

from pytest import approx
from numpy import array_equal as arr_eq

test_file = data.path/'sims/drm.h5'

def test_header():

    with InstrumentResponse(test_file) as irf:    

        assert irf.nside == 2
        assert irf.filename == data.path/'sims/drm.h5'
        assert arr_eq(irf.detectors, ['SQD0', 'SQD1', 'SQD2', 'SQD3'])

def test_get_drm():

    with InstrumentResponse(test_file) as irf:    
        
        coords = SpacecraftCoords(45*u.deg, 45*u.deg)
        
        drm = irf.get_drm("SQD0", coords = coords)
        
        assert drm.effective_area(100*u.keV) == approx(68.29956459)
        assert drm.channel_effective_area(100*u.keV, 20) == approx(17.4013527)
        
        drm = irf.get_drm("SQD0", coords = coords, interp = False)
        
        assert drm.effective_area(100*u.keV) == approx(65.604081)
        assert drm.channel_effective_area(100*u.keV, 20) == approx(16.34757006)

def test_extractor(tmp_path):

    rsp_extractor([str(test_file),
                   "--spacecraft_coords","45","45",
                   "--prefix", str(tmp_path)+'/'])
    
    drm = DRM.from_gbm_rsp_file(tmp_path/"SQD0.rsp")

    with InstrumentResponse(test_file) as irf:    
        
        coords = SpacecraftCoords(45*u.deg, 45*u.deg)
        
        drm2 = irf.get_drm("SQD0", coords = coords)

        assert (drm.effective_area(100*u.keV)
                == approx(drm2.effective_area(100*u.keV)))
