from bctools.containers import Histogram
from bctools.containers.histogram import Axes,Axis
import numpy as np
from numpy import array_equal as arr_eq
from numpy import random
import pytest
from pytest import approx

from copy import deepcopy

nbinsx = 5
nbinsy = 4
nbinsz = 3

x = range(0,nbinsx+1)
y = list(np.linspace(10,20,nbinsy+1))
z = np.linspace(20,30,nbinsz+1)

def test_axis():
    '''
    Test Axis class for lines missed by general tests
    '''

    
def test_axes():
    '''
    Test Axes class for lines missed by general tests
    '''

    # Label override
    assert Axes(Axes(x, labels='axis'), 'axis2') == Axes(x, labels='axis2')

    axes = Axes([x,y], labels=['x','y'])
    assert (Axes(axes, labels=['x2','y2'])
            == Axes([x,y], labels=['x2','y2']))

    assert arr_eq(axes.labels, ['x','y'])

    #Conversion to numpy array
    assert isinstance(axes.__array__(), np.ndarray)
    assert all([arr_eq(a,b) for a,b in zip(axes.__array__(),[x,y])])

    #Properties
    assert all([arr_eq(a,b) for a,b in zip(axes.lower_bounds(0,1),[x[:-1],y[:-1]])])
    assert all([arr_eq(a,b) for a,b in zip(axes.upper_bounds(0,1),[x[1:],y[1:]])])
    assert all([arr_eq(a,b) for a,b in zip(axes.bounds(0,1),
                                           [np.transpose([x[:-1], x[1:]]),
                                            np.transpose([y[:-1], y[1:]])])])
    assert arr_eq(axes.min('x','y'), [x[0],y[0]])
    assert arr_eq(axes.max('x','y'), [x[-1],y[-1]])
    assert arr_eq(axes.max(['x'],['y']), [[x[-1]],[y[-1]]])
    
def test_histogram_init():
    '''
    Check the various ways a histogram can be initialized
    '''
    
    # 1D from range
    h = Histogram(x)
    assert h.ndim == 1
    print(h.axes.edges())
    assert arr_eq(h.axes.edges(), np.array(x))
    assert arr_eq(h.axes.widths(), np.ones(nbinsx))
    assert arr_eq(h.axes.centers(), np.arange(0,nbinsx)+.5)
    assert arr_eq(h[...], np.zeros(nbinsx+2))
    assert arr_eq(h[:], np.zeros(nbinsx))
    assert h.axes.nbins() == nbinsx
    assert h.find_bin([0]) == 0
    assert h.find_bin([.5]) == 0
    assert h.find_bin([1]) == 1
    
    # 1D from list
    h = Histogram(y)
    assert h.ndim == 1
    assert arr_eq(h.axes.edges(), np.array(y))

    # 1D from array
    h = Histogram(z)
    assert h.ndim == 1
    assert arr_eq(h.axes.edges(), np.array(z))

    # 1D from list of list
    h = Histogram([y])
    assert h.ndim == 1
    assert arr_eq(h.axes.edges(), np.array(y))

    # Multi-D from list of lists
    h = Histogram([y,y,y])
    assert h.ndim == 3
    assert all(arr_eq(axis1,axis2) for axis1,axis2 in zip(h.axes,[np.array(y),np.array(y),np.array(y)]))

    # Multi-D from list of arrays
    h = Histogram([z,z,z])
    assert h.ndim == 3
    assert all(arr_eq(axis1,axis2) for axis1,axis2 in zip(h.axes,[np.array(z),np.array(z),np.array(z)]))

    # Multi-D from list of arrays and lists
    h = Histogram(Axes([x,y,z], labels=['x','y','z']))
    assert h.ndim == 3
    assert all(arr_eq(axis1,axis2) for axis1,axis2 in zip(h.axes,[np.array(x),np.array(y),np.array(z)]))
    assert all(arr_eq(a.centers,axis2) for a,axis2 in zip(h.axes,[x[:-1]+np.diff(x)/2,y[:-1]+np.diff(y)/2,z[:-1]+np.diff(z)/2])) 
    assert all(arr_eq(a.widths,axis2) for a,axis2 in zip(h.axes,[np.diff(x),np.diff(y),np.diff(z)])) 
    assert arr_eq(h[:], np.zeros([nbinsx, nbinsy, nbinsz]))
    assert arr_eq(h[...], np.zeros([nbinsx+2, nbinsy+2, nbinsz+2]))
    assert arr_eq(h.shape, [nbinsx, nbinsy, nbinsz])

    assert arr_eq(h.find_bin(0.5, 13, 27), [0,1,2])
    assert h.find_bin(13, axis = 1) == 1
    assert h.find_bin(13, axis = [1]) == 1
    assert arr_eq(h.find_bin(13, 27, axis = [1,2]), [1,2])
    
    # Multi-D from array
    h = Histogram(np.reshape(np.linspace(0,100,30),[3,10]))
    assert h.ndim == 3
    assert all(arr_eq(axis1,axis2) for axis1,axis2 in zip(h.axes,list(np.reshape(np.linspace(0,100,30),[3,10]))))

def test_histogram_fill_and_index():
    """
    Check the fill() method

    Also check slicing by the [] operator. Not that this is different than
    the slice() method, which returns a histogram rather than just the contents.
    """
    
    # Check 1D filling and indexing
    h = Histogram(x)

    for i in range(-1,nbinsx+1):
        h.fill(i+0.5, weight = i)

    assert arr_eq(h[-1:h.NBINS+1], h[-1:nbinsx+1])
    assert arr_eq(h[-1:nbinsx+1], range(-1,nbinsx+1))
    assert arr_eq(h[-1:h.NBINS], h[-1:nbinsx])
    assert arr_eq(h[-1:h.NBINS-1], h[-1:nbinsx-1])
    assert h[h.NBINS] == nbinsx
    
    assert arr_eq(h[:], range(0,nbinsx))
    assert arr_eq(h[-1:5], range(-1,5))
    assert arr_eq(h[:5], range(0,5))
    assert arr_eq(h[[0,2,3]], [0,2,3])

    # Chek 3D filling and indexing
    h = Histogram(Axes([x,y,z], labels = ['x','y','z']), sumw2 = True)

    for ix,vx in enumerate(x[:-1]):
        for iy,vy in enumerate(y[:-1]):
            for iz,vz in enumerate(z[:-1]):

                h.fill(vx,vy,vz, weight = (ix*nbinsy + iy)*nbinsz + iz)

    assert arr_eq(h[:], np.reshape(range(0, nbinsx*nbinsy*nbinsz), (nbinsx,nbinsy,nbinsz)))
    first_xbin = np.zeros((nbinsy+2,nbinsz+2))
    first_xbin[1:-1,1:-1] = np.reshape(range(0, nbinsy*nbinsz), (nbinsy,nbinsz))
    assert arr_eq(h[0,...], first_xbin)
    assert arr_eq(h.sumw2[:], np.reshape(range(0, nbinsx*nbinsy*nbinsz), (nbinsx,nbinsy,nbinsz))**2)
    assert arr_eq(h[0,0,-1:h.NBINS+1], [0] + list(range(0,nbinsz)) + [0])
    h.fill(x[0],y[0],z[nbinsz], weight = 10)
    assert arr_eq(h[0,0,-1:h.NBINS+1], [0] + list(range(0,nbinsz)) + [10])

    assert np.sum(h[:]) == np.sum(range(0,nbinsx*nbinsy*nbinsz))
    assert np.sum(h[...]) == np.sum(h[:]) + 10

    assert arr_eq(h[:,1:3,:], h[{'y':slice(1,3)}])
    assert arr_eq(h[2:,:,:2], h[{'x':slice(2,None), 'z':slice(None,2)}])
    
    h2 = Histogram(h.axes, h[...], h.sumw2[...])
    assert h == h2

    # Clear
    h.clear()
    assert h == Histogram(Axes([x,y,z], labels = ['x','y','z']), sumw2 = True)

def test_concatenate():

    h1 = Histogram([x,y], labels=['x','y'],
                   contents = random.rand(nbinsx, nbinsy))
    h2 = Histogram([x,y], labels=['x','y'],
                   contents = random.rand(nbinsx+2, nbinsy+2),
                   sumw2 = random.rand(nbinsx+2, nbinsy+2))


    # Without under/overflow
    hc = Histogram.concatenate(z, [h2,h2,h1], label = 'z')

    hc_contents = np.zeros([nbinsz+2, nbinsx+2, nbinsy+2])
    hc_contents[1] = h2[...]
    hc_contents[2] = h2[...]
    hc_contents[3] = h1[...]

    hc_check = Histogram([z,x,y], labels=['z','x','y'],
                         contents = hc_contents)

    assert hc == hc_check

    # With under/overflow
    hc = Histogram.concatenate(z, [h1,h2,h2,h1,h2], label = 'z')

    hc_contents = np.zeros([nbinsz+2, nbinsx+2, nbinsy+2])
    hc_contents[0] = h1[...]
    hc_contents[1] = h2[...]
    hc_contents[2] = h2[...]
    hc_contents[3] = h1[...]
    hc_contents[4] = h2[...]

    hc_check = Histogram([z,x,y], labels=['z','x','y'],
                         contents = hc_contents)

    assert hc == hc_check

    # With sumw2
    hc = Histogram.concatenate(z, [h2,h2,h2], label = 'z')

    hc_contents = np.zeros([nbinsz+2, nbinsx+2, nbinsy+2])
    hc_contents[1] = h2[...]
    hc_contents[2] = h2[...]
    hc_contents[3] = h2[...]

    hc_sumw2 = np.zeros([nbinsz+2, nbinsx+2, nbinsy+2])
    hc_sumw2[1] = h2.sumw2[...]
    hc_sumw2[2] = h2.sumw2[...]
    hc_sumw2[3] = h2.sumw2[...]

    hc_check = Histogram([z,x,y], labels=['z','x','y'],
                         contents = hc_contents,
                         sumw2 = hc_sumw2)
    
    assert hc == hc_check

    # Axes mismatch
    h3 = Histogram([y,x], labels=['y','x'],
                   contents = random.rand(nbinsy, nbinsx))

    with pytest.raises(ValueError):
        Histogram.concatenate(z, [h3,h3,h1], label = 'z')

    # Size mismatch
    with pytest.raises(ValueError):
        Histogram.concatenate(z, [h2,h1], label = 'z')

        
def test_histogram_project_slice():
    """
    Check the project and slice methods
    """
    
    # Project
    h = Histogram([x,y,z],
                  np.ones([nbinsx,nbinsy,nbinsz]), np.ones([nbinsx,nbinsy,nbinsz]),
                  labels=['x','y','z'])

    xproj = h.project('x')

    assert arr_eq(xproj[:], nbinsy*nbinsz*np.ones(nbinsx))
    assert arr_eq(h.axes.edges('x'), xproj.axes.edges())
    assert xproj != h
    
    xzproj = h.project('x','z')
    assert arr_eq(xzproj[:], nbinsy*np.ones([nbinsx,nbinsz]))
    assert arr_eq(xzproj.sumw2[:], nbinsy*np.ones([nbinsx,nbinsz]))
    assert all(arr_eq(axis1,axis2) for axis1,axis2 in zip(h.axes.edges('x','z'), xzproj.axes))

    #Project can be use to transpose
    yzx_transpose = h.project('y','z','x')
    assert arr_eq(yzx_transpose[:], np.ones([nbinsy, nbinsz, nbinsx]))
    assert all(arr_eq(axis1,axis2) for axis1,axis2 in
               zip(yzx_transpose.axes, [y,z,x]))

    
    #Slice
    hslice = h.slice('x')
    assert h == hslice

    hslice = h.slice('y', h.NBINS-nbinsy, h.NBINS)
    assert h == hslice

    hslice = h.slice('z',1,2).slice('y',0,1).slice('x',3,4)
    goodslice = [[[0.,0.,0.],[3.,3.,3.],[9.,9.,9.]],
                 [[0.,0.,0.],[1.,1.,1.],[3.,3.,3.]],
                 [[0.,0.,0.],[1.,1.,1.],[3.,3.,3.]]]
    assert arr_eq(hslice[...], goodslice)
    assert arr_eq(hslice.sumw2[...], goodslice)

    h = Histogram([x,y],
                  np.ones([nbinsx+2,nbinsy+2]), np.ones([nbinsx+2,nbinsy+2]),
                  labels=['x','y'])

    hslice = h.slice('x', 0, h.NBINS, underflow=False, overflow=False)
    hslice = hslice.slice('y', 0, hslice.NBINS, underflow=False, overflow=False)
    assert arr_eq(hslice[...],
                  Histogram([x,y],
                            np.ones([nbinsx,nbinsy]), np.ones([nbinsx,nbinsy]),
                            labels=['x','y'])[...])

def test_histogram_operator():

    ones = np.ones([nbinsx+2, nbinsy+2])
    h0 = Histogram([x,y], ones, ones, labels = ['x','y'])

    h = deepcopy(h0)
    h *= 2

    assert h == Histogram([x,y], 2*ones, 4*ones, labels = ['x','y'])

    h = deepcopy(h0)
    h = h * 2

    assert h == Histogram([x,y], 2*ones, 4*ones, labels = ['x','y'])

    h = deepcopy(h0)
    h += h0

    assert h == Histogram([x,y], 2*ones, 2*ones, labels = ['x','y'])

    h -= h0

    assert h == h0
    
    h = h + h0
    assert h == Histogram([x,y], 2*ones, 2*ones, labels = ['x','y'])

    h = h - h0
    assert h == h0

    # If one has sumw2=None, then sumw2 is None
    h0_noW2 = Histogram([x,y], ones, labels = ['x','y'])

    h += h0_noW2

    assert h == Histogram([x,y], 2*ones, labels = ['x','y'])

    h = Histogram([x,y], 2*ones, ones, labels = ['x','y'])
    h -= h0_noW2

    assert h == h0_noW2

    h = deepcopy(h0)
    h = h + h0_noW2
    assert h == Histogram([x,y], 2*ones, labels = ['x','y'])

    h = Histogram([x,y], 2*ones, ones, labels = ['x','y'])
    h = h - h0_noW2
    assert h == h0_noW2

    # Scale
    h = deepcopy(h0)
    h2 = deepcopy(h0)

    h.scale(2)
    h2 *= 2
    assert h == h2

    h = deepcopy(h0)
    h.scale(range(0,nbinsx+2), axis='x')
    assert arr_eq(h.project('x')[...], (nbinsy+2)*np.arange(nbinsx+2))

def test_histogram_interpolation():

    h = Histogram([np.linspace(-0.5,5.5,7),
                   np.linspace(-0.5,3.5,5)],
                  np.repeat([range(0,4)],6, axis=0))

    assert h.interp(5,2.6) == 2.6

    h = Histogram([np.linspace(-0.5,3.5,5),
                   np.linspace(-0.5,5.5,7)],
                  np.repeat(np.transpose([range(0,4)]), 6, axis=1))

    assert h.interp(2.6, 5) == 2.6

    h = Histogram([np.linspace(-0.5,3.5,5),
                   np.linspace(-0.5,5.5,7)],
                  (np.repeat(np.transpose([range(0,4)]), 6, axis=1)
                   * np.repeat([range(0,6)],4, axis=0)))

    assert h.interp(2,3) == 6
    assert h.interp(2,2.5) == 5
    assert h.interp(1.5,2.5) == 3.75

    h = Histogram([10**np.linspace(-0.5,3.5,5),
                   10**np.linspace(-0.5,5.5,7)],
                  (np.repeat(np.transpose([range(0,4)]), 6, axis=1)
                   * np.repeat([range(0,6)],4, axis=0)),
                  scale = 'log')

    assert h.interp(10**2, 10**3) == approx(6)
    assert h.interp(10**2, 10**2.5) == approx(5)
    assert h.interp(10**1.5, 10**2.5) == approx(3.75)

    c = np.linspace(.5,9.5,10)
    c[0] = 0
    h = Histogram(np.linspace(0,10,11), c)
    h.axes[0].scale('symmetric')
    
    assert h.interp(.5) == 0.5
    
def test_histogram_sanity_checks():
    """
    Check expected exceptions
    """
    
    # Bad axes shape
    with pytest.raises(Exception):
        Histogram(edges = np.array([[[1,2,3,4],[1,2,3,4]]]))

    with pytest.raises(Exception):
        Histogram(edges = 5)

    with pytest.raises(Exception):
        Histogram(edges = [])

    with pytest.raises(Exception):
        Histogram(edges = [5])

    with pytest.raises(Exception):
        Histogram(edges = [[5,6],[5]])

    # Bad labels
    with pytest.raises(Exception):
        Axes([1,2,3,4], labels=['x','y'])
        
    with pytest.raises(Exception):
        Axes([range(10), range(20)], labels=['x','x'])

    # Axes - contents size mistmatch
    with pytest.raises(Exception):
        Histogram(edges = [0,1,2,3], contents = [0,0])

    # Not strictly monotically increasing axes
    with pytest.raises(Exception):
        Histogram(edges = [0,1,2,3,3,4,5])
        
    with pytest.raises(Exception):
        Histogram(edges = [0,1,2,3,4,3,5])
        
    # Out of bounds
    h = Histogram(edges = [0,1,2,3], labels=['x'])
    with pytest.raises(Exception):
        h[-2]

    with pytest.raises(Exception):
        h[h]

    with pytest.raises(Exception):
        h[h.NBINS+2]

    with pytest.raises(Exception):
        h[-2:h.NBINS+2]

    with pytest.raises(Exception):
        h[:h.NBINS+2]

    with pytest.raises(Exception):
        h[[-2,-1,1]]

    with pytest.raises(Exception):
        h[[-1,1,6]]

    with pytest.raises(Exception):
        h.axes.edges(1)

    with pytest.raises(Exception):
        h.axes.edges('y')

    # Filling and find bin dimensions
    h = Histogram(edges = [0,1,2,3])
    with pytest.raises(Exception):
        h.find_bin(1,2) # 2 inputs, 1 dim

    h.fill(1, weight = 2) # Correst, weight by key
    with pytest.raises(Exception):
        h.fill(1,2) # 2 inputs, 1 dim.

    # Project
    h = Histogram(edges = [0,1,2,3])
    with pytest.raises(Exception):
        h.project(0) #Can't project 1D histogram 

    h = Histogram(edges = [range(0,10), range(0,20)])
    with pytest.raises(Exception):
        h.project([0,0]) #Axes repeat

    h = Histogram(edges = [range(0,10), range(0,20)])
    with pytest.raises(Exception):
        h.project(2) # Axis out of bounds

    # Slicing
    h = Histogram(edges = [range(0,10+1), range(0,20+1)])
    
    h.slice(0, 0) # Slices never start from underflow
    with pytest.raises(Exception):
        h.slice(0, -1) # Out of bounds

    h.slice(0, 0, 10) # Slices never stop at overflow
    with pytest.raises(Exception):
        h.slice(0, 0, 11) # Out of bounds

    with pytest.raises(Exception):
        h.slice(0, 6, 3) # Backward slice not supported

    # Operators
    h = Histogram(edges = [range(0,10+1), range(0,20+1)])
    h2 = Histogram(edges = [range(0,10), range(0,20)])

    with pytest.raises(Exception):
        h *= [1,2]

    with pytest.raises(Exception):
        h += h2

    with pytest.raises(Exception):
        h -= h2

    with pytest.raises(Exception):
        h.scale([[1],[2]], axis=0)

    with pytest.raises(Exception):
        h.scale(2, axis=3)
