"""
Test for bc-rsp app
"""

from bctools.sims import rsp_gen
from bctools.config import Configurator

from gbm.data import RSP

from pytest import approx

import numpy as np

import glob
import os

def test_main(tmp_path):

    rsp_gen.main([str(Configurator.config_example_path()),str(tmp_path),
                  '--ntriggers','4000',
                  '--direction','0','0',
                  '--seed', '0',
                  '--override', 'simulations:spectrum:args:norm = 2'])

    config = Configurator(Configurator.config_example_path())

    rsp_path = tmp_path / (config['detector:detector_names'][0] + '.rsp')
    
    rsp = RSP.open(str(rsp_path))

    aeff = rsp.photon_effective_area(0)
    
    assert np.mean(aeff.counts) == approx(26.7124, rel = .001)

    arg_maxaeff = np.argmax(aeff.counts)
    
    assert aeff.counts[arg_maxaeff] == approx(94.8680, rel = 0.001)
    assert aeff.centroids[arg_maxaeff] == approx(299.5)
    assert np.std(rsp.drm(0)[arg_maxaeff]) == approx(4.3158, rel = 0.001)

    # Using existing data
    for rsp_file in glob.glob(str(tmp_path/"*.rsp")):
        os.remove(rsp_file)

    rsp_gen.main([str(Configurator.config_example_path()),str(tmp_path),
                  '--direction','0','0',
                  '--seed', '0'])

    rsp = RSP.open(str(rsp_path))

    aeff = rsp.photon_effective_area(0)

    assert np.mean(aeff.counts) == approx(26.7123, rel = 0.001)

    arg_maxaeff = np.argmax(aeff.counts)
    
    assert aeff.counts[arg_maxaeff] == approx(94.8680, rel = 0.001)
    assert aeff.centroids[arg_maxaeff] == approx(299.5)
    assert np.std(rsp.drm(0)[arg_maxaeff]) == approx(4.3158, rel = 0.001)
