from gcn_kafka import Producer
# Connect as a producer.
# Warning: don't share the client secret with others.
producer = Producer(client_id='', client_secret='')
# any topic starting with 'mission.'
topic = 'gcn.notices.burstcube.testnotice'
# JSON data converted to byte string format
data = json.dumps({
    '$schema': 'https://gcn.nasa.gov/schema/main/gcn/notices/mission/SchemaName.schema.json',
    'key': 'Test Notice for BurstCube'
}).encode()
# Method to send notice - takes topic string, data json dump
producer.produce(topic, data)