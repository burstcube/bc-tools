API
***

.. toctree::
   :maxdepth: 2

   io
   containers
   spectra
   fitting
   sims
   loc
   util
   misc
