Simulations
===========

Detector effects
----------------

.. automodule:: bctools.sims.detector_effects
   :members:
   :undoc-members:

Response generator
------------------

.. automodule:: bctools.sims.rsp_gen
   :members:
   :undoc-members:

Detector response matrix
------------------------

.. autoclass:: bctools.sims.DRM
   :show-inheritance:
   :members:
   :inherited-members:
