Fitting
=======

Fast Norm Fit
-------------

.. autoclass:: bctools.fitting.FastNormFit
   :show-inheritance:
   :members:
   :inherited-members:
