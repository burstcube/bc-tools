.. _api_containers:

Containers
==========

Simulations
-----------

Hit
^^^

.. autoclass:: bctools.containers.Hit
   :show-inheritance:
   :members:
   :inherited-members:

SimEvent
^^^^^^^^

.. autoclass:: bctools.containers.SimEvent
   :show-inheritance:
   :members:
   :inherited-members:

Detector
--------

Detector
^^^^^^^^
      
.. autoclass:: bctools.containers.Detector
   :show-inheritance:
   :members:
   :inherited-members:


Histogram
---------

Histogram
^^^^^^^^^

.. autoclass:: bctools.containers.Histogram
   :show-inheritance:
   :members:
   :inherited-members:

Axis
^^^^

.. autoclass:: bctools.containers.histogram.Axis
   :show-inheritance:
   :members:
   :inherited-members:

Axes
^^^^

.. autoclass:: bctools.containers.histogram.Axes
   :show-inheritance:
   :members:
   :inherited-members:
