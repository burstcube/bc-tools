Localization
============

Localization tables
-------------------

LocTable
^^^^^^^^

.. autoclass:: bctools.loc.LocTable
   :show-inheritance:
   :members:

HealpixLocTable
^^^^^^^^^^^^^^^

.. autoclass:: bctools.loc.HealpixLocTable
   :show-inheritance:
   :members:


Localization likelihood
-----------------------

LocLike
^^^^^^^

.. autoclass:: bctools.loc.LocLike
   :show-inheritance:
   :members:

NormLocLike
^^^^^^^^^^^

.. autoclass:: bctools.loc.NormLocLike
   :show-inheritance:
   :members:

Maps
----

TSMap
^^^^^

.. autoclass:: bctools.loc.TSMap
   :show-inheritance:
   :members:





