Input/Output
============

Cosima
------

CosimaSim
^^^^^^^^^

.. autoclass:: bctools.io.CosimaSim
   :show-inheritance:
   :members:
   :inherited-members:

CosimaSource
^^^^^^^^^^^^

.. autoclass:: bctools.io.CosimaSource
   :show-inheritance:
   :members:
   :inherited-members:


Instrument Response File
------------------------

.. autoclass:: bctools.io.InstrumentResponse
   :show-inheritance:
   :members:
   :inherited-members:


