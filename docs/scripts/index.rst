Scripts
*******

The scripts are easy ways to call certain functions of the library from the
command line. While you could perform the same tasks using Python code, it is
usually easier to just call an executable that is in your path.

Most of the parameters needed by the scripts are defined in a yaml configuration
file. An example of this file comes with bc-tools, within the `config` module. You
can get the path to it either using
:code:`bctools.config.Configurator.config_example_path()` or from the command line
calling ``bc-config-example-path``. Sometime though it is impractical to change the
configuration file for every run. In these situations you can override any parameter
using the :code:`--override` command-line option, which accepts pairs keys and
values, separated by '``=``'. For example::

  $ bc-script --override parameter1=5 'group:subgroup:parameter2 = ["example", 5, True]'

is equivalent to have a configuration file with::

  parameter1: 5

  group:
    subgroup:
      parameter2: ["example", 5, True]

Note that the groups/subgroups are separated by :code:`:`, and that you need to use
quotes such that each overriding statement is passed as single argument to the
exceutable (spaces are trimmed). The value will by parsed by yaml in the same was as
if it were in the configuration file.

The following is an overview of the available scripts. Use :code:`-h`/:code:`--help`
for more details and other options.

bc-config-example-path
----------------------

Prints the path in your system to the BurstCube configuration file example. e.g.::

  $ bc-config-example-path
  /path/to/bc-tools/bctools/config/bc_config.yaml

This is an entry point for ``bctools.config.Configurator.config_example_path()``


bc-rsp
------

Generate BurstCube detector reponse files.

Basic usage::

  bc-rsp config_file working_dir --ntriggers NTRIGGERS

where ``NTRIGGERS`` is the stopping conditions, the number of thrown events that
resulted in an interaction with a sensitive component (per simulated direction).

By default, it generates an HDF5 file containing the detector response matrices
for multiple directions, specifically the center of the pixels in a HEALPix grid
(the pixelization is controlled by `simulations:nside`). This can then be opened
by `bctools.io.DRM` which can interpolate in between to obtain a full 4pi response.
The user can specify a specific direction to simulate with `--direction`, in which case
a GBM RSP will be output for each detector.
  
The program performs the following steps for each simulated directions:

1. Creates a working directory.
2. Generates a Cosima .source file.
3. Runs Cosima, resulting in a .sim file.
4. Using this output, it generates a detector response matrix which is saved as
   GBM RSP files. There is one for each detector labeled ``detector_id.rsp``.
   The file called ``detector_id_photopeak.rsp`` includes only the events
   corresponding to the photopeak.

If ``--direction`` was not set, then at the end all detector response matrices are
combined in ``working_dir/drm[_peak].h5``.
   
If the program detects that the subproduct of any of these steps already exists
in the working directory, the step is skipped.

.. warning::

   If a subproduct is already available, the configurations parameters or command-line
   arguments needed for the step will be ignored. For example, if the Cosima mc.source
   file already exists, it will be used without checking that the spectrum corresponds
   to the one input by the user during the current run.

Generating a full detector response can be CPU intensive. There are two ways to
parallelize and speed this process:

1. Using ``--nthreads N``: the program launches a subprocess for each pixel, where ``N``
   is the maximum subprocesses running at a given time
2. Using ``--part PART NPARTS``: in this case, the program will divide the number of
   pixels into ``NPARTS`` number of jobs, and will only run number ``PART``
   (zero indexed).
   Note that in this case the DRMs will not be combined into a single HDF5 at the end.
   A separate job without this flag needs to be run at the end.

Both ways can be combined.
   
.. note::

   You need to have `MEGAlib <http://megalibtoolkit.com/>`_ installed and loaded.

This is an entry point for the ``bctools.sims.rsp_gen`` module.

bc-rsp-extractor
----------------

For convenience, when you generate a full detector response with ``bc-rsp``, you can
extract a particular direction with `bc-rsp-extractor`.

Basic usage::

  bc-rsp-extractor input_hdf5 --spacecraft_coords RA(deg) DEC(deg)

This will output a .rsp file per detector, prefixed with the detector name. 

