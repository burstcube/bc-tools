Tutorials
*********

Tutorial for various components of the `bc-tools` library. These are Python
notebooks that you can execute interactively.

.. toctree::
   :maxdepth: 1

   signal_identification.ipynb
   DetectorResponseGenerator.ipynb
   FullDRM.ipynb
   Localization.ipynb
   
