Installation
============

Install the code via

.. code-block:: bash

   $ pip install -e .

Additional requirements
-----------------------

If you need to run simulations, you have to install `MEGALib <https://megalibtoolkit.com/>`_ separately. If you are starting from an existing detector response, then MEGALib is optional.
  
Docker
------

Docker containers are available at https://hub.docker.com/u/burstcube
  
