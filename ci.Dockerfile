FROM scientificlinux/sl:7

# === General dependencies ===
# These need to be installed as root
RUN yum -y update
RUN yum -y install \
    avahi-compat-libdns_sd-devel \
    binutils \
    cmake \
    curl \
    fftw-devel \
    gcc \
    gcc-c++ \
    gcc-gfortran \
    git \
    glew-devel \
    graphviz-devel \
    libX11-devel \
    libXext-devel \
    libXft-devel \
    libXpm-devel \
    libxml2-devel \
    make \
    mesa-libGL-devel \
    mesa-libGLU-devel \
    mysql-devel \
    openssl \
    openssl-devel \
    pcre-devel \
    python39 \
    python3-devel \
    python3-tkinter

# === User setup ===
# MEGAlib can't be installaed as root and pip warns you

RUN groupadd -r user && \
    useradd -g user user

USER user

ARG HOME=/home/user
ARG WORKDIR=$HOME
WORKDIR $WORKDIR

# === pip dependencies ===

#Workaround to install ephem. Error otherwise:
#UnicodeDecodeError: 'ascii' codec can't decode byte 0xe2 in position 4461: ord  inal not in range(128)
ENV LC_CTYPE en_US.UTF-8
ENV LANG en_US.UTF-8

RUN pip3 install --user \
    ephem \
    numpy \
    pytest \
    PyYAML

# Put pytest in path
ENV PATH="$HOME/.local/bin:${PATH}"


# === cmake ===

# MEGAlibs needs cmake >3.6, yum installs 2.8
ADD --chown=user:user https://github.com/Kitware/CMake/releases/download/v3.16.5/cmake-3.16.5.tar.gz ./
ARG CMAKEDIR=$WORKDIR/cmake3
RUN tar -xf cmake-3.16.5.tar.gz && \
    cd cmake-3.16.5 && \
    ./configure --prefix=$CMAKEDIR && \
    make && \
    make install
ENV PATH="$CMAKEDIR/bin:${PATH}"

# === MEGAlib ===

# Needs a lot of memory. Check your docker preferences if it fails

ARG MEGALIBDIR=$WORKDIR/MEGAlib
RUN git clone https://github.com/zoglauer/megalib $MEGALIBDIR && \
    cd $MEGALIBDIR && \
    bash setup.sh --clean=yes && \
    echo . $MEGALIBDIR/bin/source-megalib.sh >> $HOME/.bashrc


#=== gbm-data-tools ===

#When gbm-data-tools is releases change this to pull from repo
COPY gbm-data-tools-v1.1.0-7-ga5766ce.tar.gz .

# gmb-data-tools need basemap, but there is an issue installing it
# automatically, so this is done manually
# basemap need geos, but there is an issue installing it
# automatically, so this is done manually...
# Also, specifying pyproj version because of cython issue,
# see https://github.com/pyproj4/pyproj/issues/177
ARG BASEMAPDIR=$WORKDIR/basemap
ARG GEOS_DIR=$WORKDIR/geos
RUN git clone --single-branch --branch v1.2.1rel --depth 1 https://github.com/matplotlib/basemap && \
    cd $BASEMAPDIR/geos-3.3.3 && \
    ./configure --prefix=$GEOS_DIR && \
    make && \
    make install && \
    cd .. && \
    pip3 install --user pyproj==1.9.6 && \
    pip3 install --user .
    
RUN pip3 install --user gbm-data-tools-v1.1.0-7-ga5766ce.tar.gz

# === Appendix ===
# Small addition added at the end to used cached layers,
# Will move to appropiate sections eventually

RUN pip3 install --user pytest-cov

RUN pip3 install --user sphinx sphinx_rtd_theme

RUN pip3 install --user mhealpy

#To build tutorials
RUN pip3 install --user nbsphinx jupyter_client ipykernel
ADD --chown=user:user https://github.com/jgm/pandoc/releases/download/2.9.2.1/pandoc-2.9.2.1-linux-amd64.tar.gz $HOME
RUN tar xvzf $HOME/pandoc-2.9.2.1-linux-amd64.tar.gz --strip-components 1 -C $HOME/.local

# For full instrument response
RUN pip3 install --user h5py

# MEGALib env needs to be sourced by bash
RUN cd $HOME/.local/bin && \
    echo "#!/bin/bash" >>  entrypoint.sh &&\
    echo ". $MEGALIBDIR/bin/source-megalib.sh" >> entrypoint.sh && \
    echo '/bin/bash -c "$@"' >> entrypoint.sh && \
    chmod +x entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]
CMD ["/bin/bash"]

