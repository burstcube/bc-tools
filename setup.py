#!/usr/bin/env python

from setuptools import setup, find_packages
from version import get_git_version

setup(name='bctools',
      version=get_git_version(),
      author='Jeremy Perkins',
      author_email='jsperki1@umd.edu',
      url='https://gitlab.com/burstcube/bc-tools',
      include_package_data=True,
      packages = find_packages(include=["bctools","bctools.*"]),
      install_requires = ["matplotlib>=3.7.1",
                          "scoords",
                          "histpy",
                          "mhealpy",
                          'astro-gdt  @ git+https://github.com/BurstCube/gdt-core.git@develop',
                          'yayc',
                          ],
      entry_points={"console_scripts":[
                       "bc-rsp = bctools.sims.rsp_gen:main",
                       "bc-rsp-extractor = bctools.io.InstrumentResponse:rsp_extractor",
                       "bc-config-example-path = bctools.config:_print_config_example_path",
                    ]},
      package_data={
          'bctools': [  'data/sims/*',
                        'config/bc_config.yaml',
                        'config/geometry/*']
          }
      )

