from astropy.coordinates import SkyCoord, PhysicsSphericalRepresentation

import astropy.units as u

from mhealpy import HealpixMap
from mhealpy.util import find_peaks

import numpy as np

from copy import deepcopy

from scipy.stats import chi2

from .skymap import SkyMap

class TSMap(HealpixMap):
    """
    Test statistic map.

    Args:
        *args, **kwargs: Passed to mhealpy.HealpixMap
    """
    
    def __init__(self, *args, **kwargs):

        density = kwargs.pop('density', True)

        if not density:
            raise ValueError("TS maps should be density maps")
        
        HealpixMap.__init__(self, *args, density = True, **kwargs)
        
    def compute(self, *loclikes):
        """
        Compute the TS at each pixel based on a series of LocLike plugins.
        """
        
        # Location for all pixel centers
        coords = self.pix2skycoord(range(self.npix))
        
        for loclike in loclikes:

            self._data += 2*(loclike.get_log_like(coords) -
                             loclike.get_null_log_like())
                        
    def plot(self, *args, cont = .9, **kwargs):
        """
        Plot map.

        Args:
            cont (float): Set the minimum of the colorbar to show only this 
                fraction containments. Will be overridden by vmin
            *args, **kwargs: Passed to mhealpy.HealpixMap

        Return:
            Same as mhealpy.HealpixMap
        """
        
        vmin = np.max(self._data) - chi2.ppf(cont, 2)
        vmin = kwargs.pop('vmin', vmin)

        return HealpixMap.plot(self, *args, **kwargs, vmin = vmin)

    def get_hotspots(self, thresh = 0):
        """
        Get the pixel numbers for all local maxima above a given threshold

        Args:
            thresh (float): Threshold

        Return:
            array
        """

        return find_peaks(self, thresh)        
    
    def error_area(self, cont = .9):
        """
        Area of the confidence region at a given confidence level.

        Args:
            cont (float): Containment fraction

        Return:
            astropy.Quantity
        """
        
        max_ts = np.max(self._data)

        pix_loc = np.argwhere(self._data >= max_ts - chi2.ppf(cont, 2)).flatten()

        return np.sum(self.pixarea(pix_loc))


    def best_loc(self):
        """
        Coordinate of best estimate
        
        Return:
            SkyCoord
        """
        
        pix = np.argmax(self._data)

        coord = self.pix2skycoord(pix)
        
        return coord
    
        
