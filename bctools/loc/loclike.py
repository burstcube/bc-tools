
from abc import ABC, abstractmethod

class LocLike(ABC):

    @abstractmethod
    def get_log_like(self, coords):
        """
        Return the log-likelihood for a given coordinate
        under the source hypothesis

        Return:
           float or array
        """

    @abstractmethod
    def get_null_log_like(self):
        """
        Return the log-likelihood
        under a background-only hypothesis
        
        Return:
            float or array
        """
        
