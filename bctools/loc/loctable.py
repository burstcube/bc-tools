from mhealpy import HealpixMap, HealpixBase

import h5py as h5

from astropy.coordinates import PhysicsSphericalRepresentation

from abc import ABC, abstractmethod

import numpy as np

class LocTable(ABC):
    """
    Class prototype to store the neccessary numbers to estimate the localization from
    a series of counting detectors
    """
    
    @abstractmethod
    def get_expectation(self, coord):
        """
        The expected exccess given a normalized spectral hypothesis

        Args:
           coord (SkyCoord): Sky coordinate

        Return:
           array: One per labeled measurement
        """

    @abstractmethod
    def get_background(self):
        """
        Get estimated background counts. One per measurement

        Return:
            array
        """

    @abstractmethod
    def get_data(self):
        """
        Get measured data.

        Return:
            array
        """

    @property
    @abstractmethod
    def labels(self):
        """
        Labels for each measurement. e.g. detector number or name

        Return:
            array
        """

    @property
    def nmeasurements(self):
        """
        Total number of measurements
        """
        
        return np.shape(self.labels)[0]
        
class HealpixLocTable(LocTable, HealpixBase):
    """
    Localization table based on a HEALPix grid. Can be stored in HDF5 format.

    Args:
       base (HealpixBase): Defines the healpix geometry
       labels (array): Label each measurements
       expectation (array): Expected excess for a normalized spectral hypothesis
       data (array): Measured counts. Optional.
       background (array): Estimated background the data. Optional.
       frame (BaseCoordinateFrame or str): Coordinate frame object or name. 
           NOTE: use only names of built in frames if you need to save the table 
           to disc.
    """

    def __init__(self, base, labels, expectation,
                 data = None, background = None,
                     frame = 'icrs'):

        HealpixBase.__init__(self, base = base)

        self._frame = frame

        self._labels = np.array(labels, ndmin = 1)

        if len(np.unique(self._labels)) != len(self._labels):
            raise ValueError("All labels must be uniq")
        
        self._expectation = np.array(expectation, ndmin = 2)
        
        if self._expectation.shape != (self.nmeasurements, self.npix):
            raise ValueError("Wrong 'expectation' shape ({})".format(self._expectation.shape))
        
        if data is None:
            self._data = None
        else:
            self._data = np.array(data, ndmin = 1)

            if self._data.shape != (self.nmeasurements,):
                raise ValueError("Wrong 'data' shape ({})".format(self._data.shape))

        if background is None:
            self._background = None
        else:
            self._background = np.array(background, ndmin = 1)

            if self._background.shape != (self.nmeasurements,):
                raise ValueError("Wrong 'background' shape ({})".format(self._background.shape))

    @property
    def frame(self):
        """
        Coordinate frame

        Return:
            BaseCoordinateFrame or str
        """
        return self._frame

    @property
    def labels(self):
        """
        Labels for each measurement. e.g. detector number or name

        Return:
            array
        """
        
        return self._labels

    def set_background(self, background):
        """
        Set estimated background counts. One per measurement

        Return:
            array
        """

        self._background = np.array(background, ndmin = 1)

        if self._background.shape != (self.nmeasurements,):
            raise ValueError("Wrong 'background' shape ({})".format(self._background.shape))
    
    def get_background(self):
        """
        Get estimated background counts. One per measurement

        Return:
            array
        """

        if self._background is None:
            raise RuntimeError("Background hasn't been set yet")

        return self._background
        
    def set_data(self, data):
        """
        Set measured counts.

        Return:
            array
        """
        
        self._data = np.array(data, ndmin = 1)
        
        if self._data.shape != (self.nmeasurements,):
            raise ValueError("Wrong 'data' shape ({})".format(self._data.shape))

    def get_data(self):
        """
        Set measured counts.

        Return:
            array
        """

        if self._data is None:
            raise RuntimeError("Data hasn't been set yet")
        
        return self._data

    def get_expectation(self, coords):
        """
        The expected exccess given a normalized spectral hypothesis

        Args:
           coord (SkyCoord): Sky coordinate

        Return:
           array: One per labeled measurement
        """

        coords = coords.transform_to(self.frame)
        coords = coords.represent_as(PhysicsSphericalRepresentation)

        theta = coords.theta.rad
        phi = coords.phi.rad

        pixels,weights = self.get_interp_weights(theta, phi)
        
        expectation = np.sum([w*self._expectation[:, p]
                              for p,w in zip(pixels, weights)],
                             axis = 0)

        return expectation

    def get_expectation_map(self, key):

        if key in self._labels:
            key = np.argwhere(self._labels == key).squeeze()
        else:
            raise KeyError(f'Label or detector {key} not found')
            
        return HealpixMap(data = self._expectation[key],
                          base = self)
        
    
    def write(self, filename, name = 'LocTable', overwrite = False):
        """
        Write table to HDF5 file.

        Args:
            filename (str): Path to file
            name (str): Group name inside file
            overwrite (bool): Overwrite group is already exists
        """
        
        with h5.File(filename, 'a') as f:

            # Will fail on existing group by default
            if name in f:
                if overwrite:
                    del f[name]
                else:
                    raise ValueError("Unable to write localization table. "
                                     "Another entry with the same name already "
                                     "exists. Choose a different name or use "
                                     "overwrite")
            
            group = f.require_group(name)

            group.create_dataset("expectation", data = self._expectation)

            if self._labels.dtype.kind in ['S','U']:
                try:
                    label_dataset = group.create_dataset("labels",
                                                         data = self._labels.astype('S'))
                except UnicodeEncodeError as e:
                    raise RuntimeError("Unicode is note supported in label "
                                       "when writing to disc") from e
            else:
                label_dataset = group.create_dataset("labels", data = self._labels)

            label_dataset.attrs['dtype'] = self._labels.dtype.kind
                
            if self._background is not None:
                group.create_dataset("background", data = self._background)

            if self._data is not None:
                group.create_dataset("data", data = self._data)

            if self.is_moc:
                group.create_dataset("uniq", data = self.uniq)

            group.attrs['nside'] = self.nside 
            group.attrs['scheme'] = self.scheme

            group.attrs['frame'] = self.frame #Will break with non-str frame!

    @classmethod
    def open(cls, filename, name = 'LocTable'):
        """
        Load table from HDF5 file.

        Args:
            filename (str): Path to file
            name (str): Group name inside file
        """

        with h5.File(filename, 'r') as f:

            group = f[name]

            if 'uniq' in group:
                uniq = group['uniq']
            else:
                uniq = None
            
            base = HealpixBase(scheme = group.attrs['scheme'],
                               nside = group.attrs['nside'],
                               uniq = group.get('uniq', None))

            labels = np.array(group['labels']).astype(group['labels'].attrs['dtype'])
            
            return cls(base = base,
                       frame = group.attrs['frame'],
                       labels = labels,
                       expectation = group['expectation'],
                       data = group.get('data', None),
                       background = group.get('background', None))
            
            
