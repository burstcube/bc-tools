from mhealpy import HealpixMap
from mhealpy.util import find_peaks

import numpy as np

import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap, to_rgb

from scipy.special import logsumexp

from gdt.core.file import FitsFileContextManager

class SkyMap(HealpixMap, FitsFileContextManager):

    def __init__(self, *args, **kwargs):

        density = kwargs.pop('density', False)

        if density:
            raise ValueError("Sky probability maps should be histogram-like maps")
        
        HealpixMap.__init__(self, *args, density = False, **kwargs)
        FitsFileContextManager.__init__(self)
        
    def compute(self, *loclikes, prior = None):
        """
        Compute the TS at each pixel based on a series of LocLike plugins.
        
        Args:
            loclike (LocLike): LocLike plugins
            prior (HealpixMap): Probability prior map. Defaults to uniform prior.
        
        """
        
        # Location for all pixel centers
        coords = self.pix2skycoord(range(self.npix))

        log_like = self._data
        log_like[:] = 0
        
        for loclike in loclikes:

            log_like += loclike.get_log_like(coords)

        if prior is None:
            # Uniform
            prior = HealpixMap(base = self,
                               data = self.pixarea(np.arange(self.npix)).value)

            
        # Get log prior.
        if prior.coordsys is None or prior.coordsys != self.coordsys:
            raise RuntimeError(f"Map with priors has coordsys ({prior.coordsys}) must have the same coordsys ({self.coordsys})")

        if prior.density():
            raise RuntimeError(f"Prior map must be histogram-like --i.e. density = False")
            
        # Adapt grid
        prior_aux = prior
        prior = prior.rasterize(base = self)
        
        #Prevent error message for log(0)
        prior = np.array(prior)
        prior[prior == 0] = np.finfo(prior.dtype).tiny 
        
        # Sum log of prior
        log_like[:] += np.log(prior)
            
        # This also normalizes the probability, while at the same time
        # it makes sure exp() is not blowing up
        self._data = np.exp(log_like - logsumexp(log_like))        
    
    def error_area(self, cont = .9):
        """
        Area of the credibility region at a given probability containment fraction.

        Args:
            cont (float): Containment fraction

        Return:
            astropy.Quantity
        """

        # Assuming it is normalized
        
        sorting = np.argsort(self.data)
        
        acc = np.cumsum(self[sorting])
        
        return np.sum(self.pixarea(sorting[acc > 1-cont]))

        
    def best_loc(self):
        """
        Coordinate of the most likely location
        
        Return:
            SkyCoord
        """

        pix = np.argmax(self.data)

        return self.pix2skycoord(pix)
        
    def get_hotspots(self, thresh = 0):
        """
        Get the pixel numbers for all local maxima above a given threshold

        Args:
            thresh (float): Threshold

        Return:
            array
        """

        return find_peaks(self, thresh)        

    def plot(self, *args, color = 'red', **kwargs):
        """
        Plots the cummulative probability.

        Args:
            color (bool): By default, the colormap corresponds to this color with an alpha
                value proportional to the cummulative probability. This can be overriden by
                specifying 'cmap'.
            *args, **kwargs: Passed to mhealpy.HealpixMap.plot

        Return:
            Same as mhealpy.HealpixMap.plot
        """

        # Default colormap
        # Just red with alpha values. Good for overlays
        cmap = np.zeros([256, 4])
        cmap[:,0:3] = to_rgb(color)
        cmap[:,-1] = np.linspace(0, 1, 256) # alpha
        cmap = ListedColormap(cmap)
        
        cmap = kwargs.pop('cmap', cmap)
            
        sorting = np.argsort(self.data)
        
        acc = np.cumsum(self[sorting])

        cummap = HealpixMap(base = self)
        cummap[sorting] = acc
        
        plot,ax =  cummap.plot(*args, cmap  = cmap, **kwargs)

        return plot,ax

