from astropy.coordinates import SkyCoord, PhysicsSphericalRepresentation
from astropy import units as u

from scoords import SpacecraftFrame, Attitude

from .loctable import HealpixLocTable

import numpy as np
from mhealpy import HealpixBase,HealpixMap

import h5py as h5

class LocalLocTable(HealpixBase):
    """
    Stores the expected rates on each detector, for one or more groups of
    channels, after convolving the instrument response with a spectrum

    You can either the rates for a pixel with :code:`[]` or an interpolation
    with :code:`get_rates`.
    """

    def __init__(self, rates = None, channel_mask = None):
        self._rates = None
        self._channel_mask = None
        
    @classmethod
    def from_irf(cls,
                 irf,
                 spectrum,
                 detectors = None,
                 energy_channels = None):
        """
        Args:
           irf (InstrumentResponse): All-sky intrument response file handle
           spectrum (Spectrum): Flux (keV^-1 cm^-2 s^-1)
           detectors (str or array): Detectors to include, in that order. All 
               by default
           energy_channels (int, array, slice, tuple or dict): List of 
              energy channels to sum over. Use a tuple to specify more than one
              group. Can be detector-wise using a dictionary. All channels by 
              default.
        """

        obj = cls()
        
        # Healpix map
        super(LocalLocTable, obj).__init__(nside = irf.nside, scheme = irf.scheme)

        # Standarize detectors
        if detectors is None:
            detectors = irf.detectors
        elif isinstance(detectors, str):
            detectors = (detectors,)
        
        # Standarize energy channels
        ndetectors = len(detectors)

        if isinstance(energy_channels, dict):
            # Detector-wise
            
            energy_channels = energy_channels

        elif energy_channels is None:
            # Add all channels
            
            energy_channels = {d:i for d,i in
                                    np.broadcast(detectors,slice(None))}

        else:
            # Same channels for all detectors

            energy_channels = {d:energy_channels for d in detectors}

        for det,channels in energy_channels.items():

            # Single channel to 1 element tuple
            if not isinstance(channels, tuple):
                energy_channels[det] = (channels,)
                
        # Compute total rates
        obj._rates = dict()
        obj._channel_mask = dict()
        
        for detector in detectors:

            channels = energy_channels[detector]

            obj._rates[detector] = np.empty((irf.npix,
                                             len(channels)))
            rates = obj._rates[detector] #Convenience alias
            
            # Loop through each pixel
            for pix in range(irf.npix):
            
                drm = irf.get_drm(detector, pix = pix)

                all_rates = drm.fold_spectrum(spectrum)

                #Loop through each energy channel range
                rates[pix] = [np.sum(all_rates[chan]) for chan in channels]

            # Turn energy channels into masks
            obj._channel_mask[detector] = np.zeros((len(channels),
                                                    irf.energy_channels(detector).nbins),
                                                    dtype = bool)
            
            for group,chan in enumerate(channels):
                obj._channel_mask[detector][group,chan] = True

        return obj
                
    def write(self, filename, name = "LocalLocTable", overwrite = False):
        """
        Save localization table to disk as an HDF5 file

        Args:
            filename (str): Path to file
            name (str): Group name inside file
            overwrite (bool): Overwrite group is already exists
        """

        with h5.File(filename, 'a') as f:
            
            # Will fail on existing group by default
            if name in f:
                if overwrite:
                    del f[name]
                else:
                    raise ValueError("Unable to write LocalLocTable. Another group "
                                     "with the same name already exists. Choose "
                                     "a different name or use overwrite")

            # Main folder
            loctable = f.create_group(name) 
                
            # Overall attributes
            loctable.attrs['nside'] = self.nside
            loctable.attrs['scheme'] = self.scheme

            # Rates for each detectors
            for det in self.detectors:

                det_folder = loctable.create_group(det)

                det_folder.create_dataset("channel_mask",
                                          data = self._channel_mask[det])
                det_folder.create_dataset("rates",
                                          data = self._rates[det])

    @classmethod
    def open(cls, filename, name = "LocalLocTable"):
        """
        Read localization table from file

        Args:
           filename (str): Path to file
           name (str): Name of group in file
        """

        obj = cls()
        
        with h5.File(filename, 'r') as f:

            loctable = f[name]

            super(LocalLocTable, obj).__init__(nside = loctable.attrs['nside'],
                                               scheme = loctable.attrs['scheme'])

            obj._rates = dict()
            obj._channel_mask = dict()
            
            for det,det_folder in loctable.items():

                obj._rates[det] = det_folder['rates'][:]
                obj._channel_mask[det] = det_folder['channel_mask'][:]

        return obj
                
    @property
    def detectors(self):
        """
        List of detectors
        """

        return self._rates.keys()

    def channel_mask(self, detector):
        """
        Return the channel mask of energy group
        """

        return self._channel_mask[detector]

    def nchannels(self, detector):
        """
        Number of energy channels
        """
        
        return self._channel_mask[detector].shape[1]
        
    def ngroups(self, detector):
        """
        Number of groups of channels
        """
        
        return self._channel_mask[detector].shape[0]
    
    def __getitem__(self, pix):

        return {det:rate[pix] for det,rate in self._rates.items()}
        
    def get_rates(self, coords, attitude = None):
        """
        Return the expected rate corresponding to a given local coordinate for 
        each of the energy channels ranges and all detectors. The output will 
        be a bilinear interpolation from the 4 closest pixels to the coordinate

        cArgs:
            coords (SkyCoord): Either a local or a 
                sky coordinates.
            attitude (Attitude): Spacecraft attitude quaternion.
            
        Return:
            dict: An array for each detetor.
        """

        # Coords transformation
        coords = coords.transform_to(SpacecraftFrame(attitude = Attitude.from_quat(attitude)))
                
        # Nearest pixels and weights
        coords = coords.represent_as('physicsspherical')
        pix,weights = self.get_interp_weights(coords.theta.rad,
                                              coords.phi.rad)
        
        # Sum over, for all detectors
        return {det : np.sum(weights[...,None]*rates[pix], axis = 0)
                for det,rates in self._rates.items()}
                
    def to_skyloctable(self, attitude, duration, frame = 'icrs'):
        """
        Convert localization rates table in spacecraft coordinates to counts in 
        sky coordinates
        
        Args:
            attitude (Attitude or array): Spacecract attitude quaternion.
            duration (float or array): Duration.
            frame (BaseCoordinateFrame): Coordinate frame
        """

        # Standarize
        durations = np.atleast_1d(duration)

        attitudes = np.atleast_2d(attitude)

        # Location for all pixel centers
        theta,phi = self.pix2ang(range(self.npix))

        coords = PhysicsSphericalRepresentation(theta = theta * u.rad,
                                                phi = phi * u.rad,
                                                r = 1)
        coords = SkyCoord(coords, frame = frame)

        # Get the rates step by step adding to the total counts
        nentries = 0
        for det in self.detectors:
            nentries += self.ngroups(det)
        
        skyloctable_ex = np.zeros((nentries, self.npix))
                
        for attitude,duration in zip(attitudes, durations):

            # Get counts for this attitude step
            step_rates = self.get_rates(coords,
                                        attitude = attitude)

            # Add to total
            nentry = 0
            for ndet,det in enumerate(self.detectors):

                rates = step_rates[det]
                
                for ngroup,group_rate in enumerate(np.transpose(rates)):

                    skyloctable_ex[nentry] += group_rate * duration

                    nentry += 1

        labels = [f"{det}_g{group:d}" if self.ngroups(det) > 1 else f"{det}"
                  for det in self.detectors
                  for group in range(self.ngroups(det))]
                    
        skyloctable = HealpixLocTable(base = self,
                                      frame = frame,
                                      labels = labels,
                                      expectation = skyloctable_ex)
                    
        return skyloctable
