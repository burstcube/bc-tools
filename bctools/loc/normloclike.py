
from .loclike import LocLike

from .loctable import LocTable

from collections.abc import Iterable

from bctools.fitting import FastNormFit

import numpy as np

from scipy.special import gammaln

import logging
logger = logging.getLogger(__name__)

class NormLocLike(LocLike):
    """
    Compute the likelihood from a series of observed counts (:math:`d_i`),
    the estimated background counts (:math:`b_i`) and the expected excess
    (:math:`e_i`) given a model hypothesis:

    .. math::
    
        \log \mathcal{L}(N) = \sum_i \log P(\lambda = b_i + N e_i; d_i)

        P(\lambda; d) = \log \\frac{\lambda^d e^{\lambda}}{d}

    The likelihood is maximized against the normalization (:math:`N`). It is 
    the only free (nuisance) parameter.

    Args:
        loctable (LocTable): One or more localization tables
    """

    def __init__(self, *loctable):

        # Check and standarize input
        for lt in loctable:
            if not isinstance(lt, LocTable):
                raise TypeError("One of more inputs are not LocTable instances")
            
        self._loctable = loctable

        # Cache null log like, which does not depend on the coordinates anyways
        data = self.get_data()
        bkg = self.get_background()
        
        self._null_log_like = np.sum(data * np.log(bkg) - bkg - gammaln(data))

        
    def get_log_like(self, coords):
        """
        Return the log-likelihood for a given sky coordinate. 

        Note that the likelihood is maximized against the normalization, 
        not marginalized (integrated).

        Args:
            coords (SkyCoord): Sky coordinates

        Return:
            array
        """
        
        data = self.get_data()
        background = self.get_background()
        expectation = self.get_expectation(coords)

        nf = FastNormFit()

        ts = np.empty(coords.shape)
        
        for i in np.ndindex(coords.shape):

            ts_i,norm,norm_err,status = nf.solve(data,
                                                 background,
                                                 expectation[(slice(None),)+i])

            if status:
                logger.warning(f"Fit failed at coordinate {coords[i]}")
                
            ts[i] = ts_i

        return ts/2 + self.get_null_log_like()

    def get_background(self):
        """
        List of background counts estimate for each measurement.
        
        Return:
            array
        """
        
        background = np.array([])
        
        for loctable in self._loctable:

            background = np.append(background, loctable.get_background())

        return background
            
    def get_data(self):
        """
        List of measured counts
        
        Return:
            array
        """
        
        data = np.array([])
        
        for loctable in self._loctable:

            data = np.append(data, loctable.get_data())

        return data
            
    def get_expectation(self, coords):
        """
        List of expected excess.

        Return:
            array
        """
        
        expectation = np.zeros((0,) + coords.shape)

        for loctable in self._loctable:

            expectation = np.append(expectation, loctable.get_expectation(coords),
                                    axis = 0)

        return expectation

    @property
    def labels(self):
        """
        Label for each measurement entry.

        Return:
            array
        """
        
        labels = np.array([])
        
        for loctable in self._loctable:

            labels = np.append(labels, loctable.labels)

        return labels        
    
    def get_null_log_like(self):
        """
        Get the log likelihood for N=0. This is a constant, there are no
        free parameters

        """
        
        return self._null_log_like
            




            
            
