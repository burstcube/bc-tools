import astropy.io.fits as fits
import numpy as np

from bctools.config import get_config

#from bctools.io.headers import new_header_class, new_file_headers_class
import bctools.io.headers

from gdt.core.response import Rsp as RSPCore
from gdt.core.data_primitives import Ebounds, ResponseMatrix

class RSP(RSPCore):

    @classmethod
    def open(cls, file_path, **kwargs):
        """Read a single-DRM response file from disk.

        Args:
            file_path (str): The file path 

        Returns:
           (:class:`GbmRsp`)
        """
        obj = super().open(file_path, **kwargs)
        if len(obj.hdulist) > 3:
            raise RuntimeError('{} is not a RSP file; it may be a RSP2 ' \
                               'file'.format(filename))
        # get the headers
        hdrs = [hdu.header for hdu in obj.hdulist]
        RSPHeaders = bctools.io.headers.new_file_headers_class(get_config()['io:headers:RSP'])
        headers = RSPHeaders.from_headers(hdrs)
        
        ebounds = Ebounds.from_bounds(obj.column(1, 'E_MIN'), 
                                      obj.column(1, 'E_MAX'))

        num_chans = ebounds.num_intervals
        
        fchan = np.copy(obj.column(2, 'F_CHAN'))
        nchan = np.copy(obj.column(2, 'N_CHAN'))
        ngrp = np.copy(obj.column(2, 'N_GRP'))

        num_ebins = ngrp.size

        breakpoint()
        matrix = cls._decompress_drm(obj.column(2, 'MATRIX'), num_ebins,
                                      num_chans, fchan, nchan)
            
        drm = ResponseMatrix(matrix, obj.column(2, 'ENERG_LO'),
                             obj.column(2, 'ENERG_HI'), obj.column(1, 'E_MIN'),
                             obj.column(1, 'E_MAX'))

        det = obj.hdulist["PRIMARY"].header["INSTRUME"]
        
        obj.close()
        obj = cls.from_data(drm, filename=obj.filename,
                            headers=headers,
                            detector=det)

        obj._fchan = fchan
        obj._nchan = nchan
        obj._ngrp = ngrp

        return obj

    def _build_hdulist(self):

        # create FITS and primary header
        hdulist = fits.HDUList()
        primary_hdu = fits.PrimaryHDU(header=self.headers['PRIMARY'])
        for key, val in self.headers['PRIMARY'].items():
            primary_hdu.header[key] = val
        hdulist.append(primary_hdu)
        
        # the ebounds extension
        ebounds_hdu = self._ebounds_table()
        hdulist.append(ebounds_hdu)
        
        # the drm extension
        drm_hdu = self._drm_table()
        hdulist.append(drm_hdu)        
        
        return hdulist

    def _build_headers(self, num_chans, num_ebins):
        
        headers = self.headers.copy()
        headers['EBOUNDS']['DETCHANS'] = num_chans
        headers['SPECRESP MATRIX']['DETCHANS'] = num_chans
        headers['SPECRESP MATRIX']['NUMEBINS'] = num_ebins
        return headers

    def _ebounds_table(self):
        chan_col = fits.Column(name='CHANNEL', format='1I', 
                               array=np.arange(self.num_chans, dtype=int))
        emin_col = fits.Column(name='E_MIN', format='1E', unit='keV', 
                               array=self.ebounds.low_edges())
        emax_col = fits.Column(name='E_MAX', format='1E', unit='keV', 
                               array=self.ebounds.high_edges())

        hdu = fits.BinTableHDU.from_columns([chan_col, emin_col, emax_col], 
                                             header=self.headers['EBOUNDS'])
        for key, val in self.headers['EBOUNDS'].items():
            hdu.header[key] = val

        return hdu

    def _drm_table(self):

        elo_col = fits.Column(name='ENERG_LO', format='1E', 
                              array=self.drm.photon_bins.low_edges(),
                              unit='keV')
        ehi_col = fits.Column(name='ENERG_HI', format='1E', 
                              array=self.drm.photon_bins.high_edges(),
                              unit='keV')

        # Thre is currently no compression
        # N_GRP = The number of channel subsets in this row
        # F_CHAN = First channel number of each channel subset (FITS is 1-indexed)
        # N_CHAN = Number of channels for each channel subset
        ngrp_col = fits.Column(name='N_GRP', format='1I',
                               array = np.ones(self.drm.num_ebins))
        fchan_col = fits.Column(name='F_CHAN', format='PI(1)', 
                                array= np.ones([self.drm.num_ebins, 1]))
        nchan_col = fits.Column(name='N_CHAN', format='PI(1)', 
                                array= self.drm.num_chans*np.ones([self.drm.num_ebins, 1]))
        
        matrix_col = fits.Column(name='MATRIX', array=self.drm.matrix,
                                 format='PE({})'.format(self.num_chans),
                                 unit='cm^2')
        
        hdu = fits.BinTableHDU.from_columns([elo_col, ehi_col, ngrp_col, 
                                             fchan_col, nchan_col, matrix_col],
                                         header=self.headers['SPECRESP MATRIX'])
        for key, val in self.headers['SPECRESP MATRIX'].items():
            hdu.header[key] = val

        return hdu

    # mark FIXME: This assumes NGRP=1 (no compression) not for general use
    @staticmethod
    def _decompress_drm(matrix, num_photon_bins, num_channels, _fchan, _nchan):
        """Decompresses a DRM using the standard F_CHAN, N_CHAN, and N_GRP
        keywords.
        
        Args:
            drm_data (np.recarray): The DRM data
        
        Returns:        
            (np.array)
        """
        # The format of the compress matrix is a series of groups, for each
        # energy bin, of channels with non-zero values.
        # fchan stands for the first channel of each of these groups
        # and nchan for the number of channels in the group group.
        # Each row in the matrix is a 1D list consisting on the contatenated
        # values of all groups for a given energy bin 
        # Note that in FITS the first index is 1
        drm = np.zeros((num_photon_bins, num_channels))
        for fchans, nchans, effective_area, drm_row \
            in zip(_fchan, _nchan, matrix, drm):

            channel_offset = 0

            for fchan, nchan in zip(fchans, nchans):
                
                start_idx = fchan - 1
                end_idx = start_idx + nchan

                drm_row[start_idx:end_idx] = \
                    effective_area[channel_offset:channel_offset + nchan]
                
                channel_offset += nchan

        return drm

