'''
Class to interact with .sim Cosima files
'''

import logging
logger = logging.getLogger(__name__)

from gdt.core.data_primitives import EventList

import numpy as np

from pathlib import Path

from bctools.containers import Hit,SimEvent, Detector
from bctools.util	import units as u
from bctools.exceptions import ParsingError,NotSupported

from astropy.time import TimeDelta
import astropy.units as u

from scoords import SpacecraftFrame

#MEGAlib python's bindings
try:
    import ROOT as M

    M.gSystem.Load("$(MEGALIB)/lib/libMEGAlib.so")

    G = M.MGlobal()
    G.Initialize()

    have_megalib = True
except Exception as e:
    have_megalib = False
    megalib_error = e


class CosimaSim(object):
    '''
    Contains info associated with a Cosima output file (.sim)
    
    Attributes:
        tstart (float): Start of simulated time
        tstop (float): Stop of simulated time
        nthrown (int): Total number of simulated particles
        ntriggers (int): Total number of triggered events
        events (list of :obj:`SimEvent`): Collection of all events, which 
            contain hits. These might not be loaded
    
    '''
    
    def __init__(self):

        # Check if ROOT and MEGAlib are available
        if not have_megalib:
            logging.error("Failed to load MEGAlib. Is it installed?")
            raise megalib_error
        
        self.tstart = 0
        self.tstop = 0
        self.nthrown = 0
        self.ntriggers = 0
        self.events = []

    @property
    def duration(self):

        return self.tstop - self.tstart
        
    @classmethod
    def open(cls, filename, geometry, load_events = True):
    
        '''
        Parameters:
            filename (Path): Path to .sim file.
            geometry (Path): 
            load_events (bool): If false, only load the header information of
                the file but do not store the events
    
        Returns:
            CosimaSim object
    
        Warnings:
            Concatenating files with NF and IN keywords is not supported
    
        '''

        obj = cls()

        #In case it's a string
        filename = Path(filename)
        geofilename = Path(geometry)
                
        # Load MEGAlib object
        logger.info(f"Using with  geometry {geofilename.resolve()}")
        obj._mgeo = M.MDGeometryQuest()
        obj._mgeo.ScanSetupFile(str(geofilename))
        
        logger.info(f"Loading {filename.resolve()}")
        obj._msim = M.MFileEventsSim(obj._mgeo)
        obj._msim.Open(str(filename))
        
        # We first obtain the general information of the file, such as
        # starting and end time, number of events, etc.

        obj.tstart = 0

        obj.tstop = (obj._msim.GetObservationTime().GetInternalSeconds() + 
                     1e-9*obj._msim.GetObservationTime().GetInternalNanoSeconds())
                    
        obj.nthrown = obj._msim.GetSimulatedEvents();

        obj.ntriggers = obj._msim.GetNEvents(True) #True = count from file, not cached
                                        
        # Load the event in memory if needed
        if load_events:

            for event in obj.event_iter():
                obj.events = np.append(obj.events, event)
        
        return obj

    def event_iter(self):
        '''
        Loops through the event section in a .sim file and yields a SimEvent
        object for each one. This avoids loading a large file into memory. 
        e.g.::

           for hit in sim.event_iter():

               do_something(hit)

        Return:
            Yields SimEvent objects
        '''


        self._msim.Rewind()

        while True:
    
            mevent = self._msim.GetNextEvent()
    
            if not mevent:
                break

            event = SimEvent(None, None, None, None, None, None) 

            event.energy = mevent.GetICEnergy() * u.keV

            mtime = mevent.GetTime()

            # No precision is lost by combining sec and nanosec
            # The .sim file stores the time as a float anyways
            event.time = TimeDelta(mtime.GetSeconds()*u.s + mtime.GetNanoSeconds() * u.ns,
                                   format = 'sec')

            event.particle = mevent.GetIAAt(0).GetSecondaryParticleID()

            mdirection = mevent.GetICOrigin()
                    
            event.direction = SpacecraftFrame(x = -mdirection.GetX(),
                                              y = -mdirection.GetY(),
                                              z = -mdirection.GetZ(),
                                              representation_type = 'cartesian') 

            for i in range(mevent.GetNHTs()):
                    
                mhit = mevent.GetHTAt(i)
                    
                mpos = mhit.GetPosition()
                
                det = str(self._mgeo.GetDetector(mpos).GetName())

                pos = SpacecraftFrame(x = mpos.GetX()*u.cm,
                                      y = mpos.GetX()*u.cm,
                                      z = mpos.GetY()*u.cm,
                                      representation_type = 'cartesian')
                    
                event.add_hit(Hit(det, pos,
                                  mhit.GetTime() * u.s,
                                  mhit.GetEnergy() * u.keV))

            yield event

    def time_shift(self, shift):
        '''
        Shift time of events
        
        Args:
           shift (float): duration of shift
        '''

        #Note that time of the events is relative to this time
        self.tstart += shift
        self.tstop += shift
        
    def merge(self, sim2, shift = 0):
        
        '''
        Combine CosimaSim object with a shift.
        Hits will be time sorted
        
        Parameters:
            sim2 (CosimaSim): Hits to be merged
            shift (float, optional): Apply a time shift to obj2
            
        '''
        
        # Metadata
        self.tstart = min(self.tstart, sim2.tstart + shift)
        self.tstop = max(self.tstop, sim2.tstop + shift)
        self.nthrown += sim2.nthrown
        self.ntriggers += sim2.ntriggers

        # Hits
        for event in sim2.events:
            self.events = np.append(self.events, events)
            
        # Time sorting
        event_sort = np.argsort(self.events)
        self.events = [self.events[n] for n in event_sort]
        
    def to_TTE(self, energy_channel_edges, *args, **kwargs):
        
        '''
        Convert the hits into a mocked Time-Tagged Event object
        
        Parameters:
            energy_channel_edges (list): Energy channel definition for discreatization
            \*args: passed to TTE.from_data()
            \*\*kwargs: passed to TTE.from_data()
                
        Return:
            list of :obj:`gbm.data.phaii.TTE`: One TTE for each detector
        
        '''

        # Imported here to prevent a circular import
        from bctools.io import EventData

        det = Detector()
        
        nchannels = len(energy_channel_edges)-1
        
        # Time and energy channels (aka PHA=Pulse Height Analysis), one for each detector
        times = {d:[] for d in det.detectors}
        energy_channels = {d:[] for d in det.detectors}
    
        for event in self.event:

            for hits in event.hits:
            
                times[hit.detector] += [hit.time]

                energy_channel = np.digitize(hit.energy, energy_channel_edges) - 1
                energy_channel = min(nchannels-1, max(0, energy_channel)) #Overflow/underflow

                energy_channels[hit.detector] += [energy_channel]
        
        # Instantiate objects
        TTEs = []
        for detector in det.detectors:
            evtlist = EventList.from_lists(times_list = times[detector], 
                                           pha_list = energy_channels[detector], 
                                           chan_lo = energy_channel_edges[:-1],
                                           chan_hi = energy_channel_edges[1:])

            TTEs = np.append(TTEs,EventData.from_data(data = evtlist, 
                                                detector = detector, 
                                                *args, **kwargs))
        
        return TTEs
        
