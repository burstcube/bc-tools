from .phaii import BinnedData
from bctools.config import get_config
from .headers import new_header_class, new_file_headers_class

from gdt.core.data_primitives import TimeEnergyBins

from astropy.time import Time
import astropy.units as u
from astropy.coordinates import ITRS

from scoords import Attitude

import numpy as np

from copy import copy, deepcopy

from astropy.io import fits

from .sc_hk import SpacecraftFile

class AlertTriggerData():

    def __init__(self, binned_data = None, sc_file = None):

        self._binned_data = binned_data
        self._sc_file = sc_file
    
    @classmethod
    def open(cls, file_path, *args, **kwargs):

        hdul = fits.open(file_path, **kwargs)

        # Data & headers
        data = cls._get_combined_lightcurve(hdul)
        
        ATDHeaders = new_file_headers_class(get_config()['io:headers:ATD'])
        headers = ATDHeaders.from_headers([hdu.header for hdu in hdul])

        binned_data = BinnedData.from_data(data, 
                                           filename = file_path,
                                           headers=headers)
        
        # SC
        timestamps = Time(hdul["ATTITUDE"].data['TIME'], format = 'bcmet')

        attitude = Attitude.from_quat(hdul["ATTITUDE"].data["QPARAM"],
                                      frame = 'icrs')

        orbit = hdul["ORBIT"].data
        orbit = ITRS(x = orbit['X'] * u.m,
                     y = orbit['Y'] * u.m,
                     z = orbit['Z'] * u.m,
                     v_x = orbit['Vx'] * u.m / u.s,
                     v_y = orbit['Vy'] * u.m / u.s,
                     v_z = orbit['Vz'] * u.m / u.s,
                     )
        
        sc_file = SpacecraftFile(timestamps, attitude, orbit)
        
        return cls(binned_data, sc_file)

    @property
    def detector(self):
        return self._binned_data.detector
        
    def get_binned_data(self):
        """
        Get the data binned in time and energy.

        Return:
           BinnedData
        """
        
        return self._binned_data
        
    def get_spacecraft_file(self):
        """
        Get SC attitude and orbit information contained in the ATD file.

        Return:
           SpacecraftFile
        """

        return self._sc_file

    @staticmethod
    def _merge_timescales(h_lores, h_hires):
        """
        Combines two light curves with different time widths but matching time edges, subtracting counts
        appropiately with no loss of information
        
        This assumes:
        - h_lores and h_hires overlap
        - h_hires is fully included within h_lores
        """

        if not (np.all(h_lores.emin == h_hires.emin) and np.all(h_lores.emax == h_hires.emax)):
            raise RuntimeError("Energy bounds must be the same")

        # Find start of h_hires in h_lores bins, and how many hires bin on each lores bin
        lores_bins = np.digitize(h_hires.tstop, h_lores.tstop, right = True)

        bin_i, bin_f = lores_bins[0], lores_bins[-1]

        _, repeats = np.unique(lores_bins, return_counts = True)

        # Lo res bins beginning

        begin_h_lores = TimeEnergyBins(counts = h_lores.counts[:bin_i],
                                       tstart = h_lores.tstart[:bin_i],
                                       tstop = h_lores.tstop[:bin_i],
                                       exposure = h_lores.exposure[:bin_i],
                                       emin = h_lores.emin,
                                       emax = h_lores.emax)

        new_h = begin_h_lores

        # Low end possible gap

        # Need to use isclose() because tstart was derived from tstop, so it 
        # has a floating point error and might not be exact
        # Absolute tolerace is what matter, to .1ms
        hires_start_match = np.isclose(h_hires.tstart[0], h_lores.tstart[bin_i], rtol = 0, atol = 1e-4)

        #print(h_hires.tstart[0], h_lores.tstart[bin_i], hires_start_match)

        if not hires_start_match:

            gap_h = TimeEnergyBins(counts = [h_lores.counts[bin_i] - np.sum(h_hires.counts[:repeats[0]], axis = 0)],
                                   tstart = [h_lores.tstart[bin_i]],
                                   tstop = [h_hires.tstart[0]],
                                   exposure = [h_lores.exposure[bin_i] - np.sum(h_hires.exposure[:repeats[0]])],
                                   emin = h_lores.emin,
                                   emax = h_lores.emax)

            new_h = TimeEnergyBins.merge_time([new_h, gap_h])

        # Hi res bins

        new_h = TimeEnergyBins.merge_time([new_h, h_hires])

        if hires_start_match: 
           # Prevent floating point errors by making them match exactly
           new_h.tstart[bin_i] = h_lores.tstart[bin_i]

        # Upper end possible gap

        if h_hires.tstop[-1] != h_lores.tstop[bin_f]:

            gap_h = TimeEnergyBins(counts = [h_lores.counts[bin_f] - np.sum(h_hires.counts[-repeats[-1]:], axis = 0)],
                                    tstart = [h_hires.tstop[-1]],
                                    tstop = [h_lores.tstop[bin_f]],
                                    exposure = [h_lores.exposure[bin_f] - np.sum(h_hires.exposure[-repeats[-1]:])],
                                    emin = h_lores.emin,
                                    emax = h_lores.emax)

            new_h = TimeEnergyBins.merge_time([new_h, gap_h])

        # Rest of coarse bins

        end_h_lores = TimeEnergyBins(counts = h_lores.counts[bin_f+1:],
                                    tstart = h_lores.tstart[bin_f+ 1:],
                                    tstop = h_lores.tstop[bin_f+1:],
                                    exposure = h_lores.exposure[bin_f+1:],
                                    emin = h_lores.emin,
                                    emax = h_lores.emax)


        new_h = TimeEnergyBins.merge_time([new_h, end_h_lores])

        return new_h

    @staticmethod
    def _get_combined_lightcurve(hdul):
        """
        Call _merge_timescales iteratively one timescale as a time.
        """
        
        hist = None

        # Ebounds
        # TODO: query this from ebounds caldb file
        channel_energy_edges = np.concatenate([[0],
                                               np.array(get_config()["pipelines:l1:trigger:tte:channel_energy_edges"]),
                                               [2000]])
        
        # Loop through all timescales, from finer to coarser
        for hdu in hdul:
            if hdu.name[:3] != "ATD":
                continue

            # Create TimeEnergyBins object for timescale
            dt_ms = int(hdu.name[3:])

            dt =  dt_ms*1e-3

            tstop = hdu.data['Time']

            tstart = tstop - dt

            # Prevent floating point errors by making edges match exactly
            # Absolute tolerace is what matter, to .1ms
            approx_equal_edges =  np.isclose(tstop[:-1], tstart[1:], rtol = 0, atol = 1e-4)
            tstart[1:][approx_equal_edges] = tstop[:-1][approx_equal_edges]

            counts = copy(hdu.data['PHA'])

            hist_i = TimeEnergyBins(counts = counts,
                                 tstart = tstart,
                                 tstop = tstop,
                                 exposure = dt*np.ones(tstop.size),
                                 emin = channel_energy_edges[:-1],
                                 emax = channel_energy_edges[1:])

            # Merge
            if hist is None:
                hist = hist_i
            else:
                hist = AlertTriggerData._merge_timescales(hist_i, hist)
                
        return hist
