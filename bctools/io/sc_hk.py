

class SpacecraftFile:

    def __init__(self, timestamps = None, attitude = None, orbit = None):

        if timestamps is not None or attitude is not None:

            if len(timestamps) < 2:
                raise ValueError("SpacecraftHousekeeping must have at least 2 timestamps")
            
            if len(timestamps) != len(attitude):
                raise ValueError("Number of timestamps and attitude must be the same")

            if orbit is not None and len(timestamps) != len(orbit):
                raise ValueError("Number of timestamps and orbit must be the same")
            
        self._attitude = attitude
        self._timestaps = timestamps
        self._orbit = orbit

    @property
    def attitude(self):
        return self._attitude

    @property
    def timestamps(self):
        return self._timestaps

    @property
    def orbit(self):
        """
        Spacecraft location and valocity

        Returns: :py:class:`astropy.coordinates.ITRS`
        """
        return self._orbit
