'''
Class to interact with .source Cosima files
'''

import logging
logger = logging.getLogger(__name__)

import subprocess

from pathlib import Path

from numpy import random

from bctools.util	import units as u
from bctools.exceptions import UserError,ParsingError
from bctools.spectra import Spectrum
from bctools.util.coords import SpacecraftCoords

class CosimaSource:
    '''
    Class to read/write .source Cosima files
    
    Parameters:
        geometry (Path): Path to Geomega geometry file
        ntriggers (int): Stop simulation when this number of triggers was reached
        
    Note:
        Besides the parameters initialized during consutruction, note that
        ou still need to call set_point_source

    Attributes:
        avail_spec_help (str): List of available spectral shapes
        spectrum (Spectrum): Parsed Cosima spectrum and flux
        geometry (Path): Path to Geomega geometry file
        ntriggers (str): Stop simulation when this number of triggers was reached
        location (SpacecraftCoords): Source location. Distance might be infinity
        source_path (Path): Path to .source Cosima file
        sim_path (Path): Path to Cosima's results .sim file

    Warnings:
        Reading general .source files not written by this class is not 
        guaranteed to work
    '''

    def __init__(self, geometry=None, ntriggers=None):

        # Make path absolute in case the current directory changes
        self.geometry = geometry
        if self.geometry is not None:
            self.geometry = Path(geometry).resolve()

        self.ntriggers = ntriggers

        self.location = None

        self.spectrum = None
        
        self.source_path = None
        self.sim_path = None

    @classmethod
    def open(cls, source_path):
        '''
        Read file from disk

        Args:
            source_path (Path): Path to .source file

        Warnings:
            Reading general .source files not written by this class is not 
            guaranteed to work
        '''
        
        obj = cls()
        
        obj.source_path = Path(source_path)

        f = open(obj.source_path)

        flux = None
        spectrum = None
        
        for line in  f.readlines():

            words = line.split()

            if len(words)<2:
                continue
            
            key = words[0]
            params = words[1:]
            if len(params)==1:
                params = params[0]
            
            if key == 'Geometry':
                obj.geometry = Path(params)
            elif key == "run0.FileName":
                obj.sim_path = Path(params)
            elif key == "run0.Triggers":
                obj.ntriggers = int(params)
            elif key == "source0.Beam":
                obj.location = SpacecraftCoords.from_megalib(params)
            elif key == "source0.Spectrum":
                # This only gives you the shape
                spectrum = params
            elif key == "source0.Flux":
                flux = params
            
        f.close()

        if obj.geometry is None:
            raise ParsingError("Source file doesn't have geometry information")

        if obj.sim_path is None or obj.ntriggers is None:
            raise ParsingError("Source file has incomplete run definition")

        if obj.location is None or spectrum is None or flux is None:
            raise ParsingError("Source file has incomplete source definition")

        obj.spectrum = Spectrum.from_megalib(spectrum, flux)
        
        return obj
        
    def set_point_source(self, spectrum, location):

        '''
        Indicate that a point source will be simulated
        
        Parameters:
            spectrum (Spectrum): Spectrum to simulate
            location (SpacecraftCoords): Source location
        '''

        self.spectrum = spectrum

        self.location = location
                    
    def write(self, source_path, sim_path):

        '''
        Write file

        Parameters:
            source_path (Path): .source file output path
            sim_path (Path): .sim file output path prefix when this .source file 
                is run. .inc1.id1.sim will be appended, .inc2.id1.sim if it 
                already exists, and so on.

        Note:
            sim_path is relative to the current directory, not to source_path
        '''

        if self.spectrum is None:
            raise UserError("You need to set the source first before calling write()")

        # Make paths absolute in case the current directory changes.
        # Note that in MEGAlib the paths are relative to the current path,
        # not relative to the .source file
        self.source_path = Path(source_path).resolve()
        self.sim_path = Path(sim_path).resolve()
        
        logger.info(("Writing Cosima source file to %s. "
                     "Simulation results will be output to %s*"),
                    self.source_path, self.sim_path)
        
        source_file = open(self.source_path, 'w')

        spectrum,flux = self.spectrum.to_megalib()
        
        source_file.write(("Geometry {geometry}\n" 
                           "PhysicsListEM Livermore\n"
                           "StoreSimulationInfo True\n"
                           "\n"
                           "Run run0\n"
                           "run0.FileName {sim_path}\n"
                           "run0.Triggers {ntriggers}\n"
                           "run0.Source source0\n"
                           "\n"
                           "source0.ParticleType 1\n"
                           "source0.Beam {beam}\n"
                           "source0.Spectrum {spectrum}\n"
                           "source0.Flux {flux}\n").format(
                               geometry = self.geometry,
                               sim_path = self.sim_path,
                               ntriggers = self.ntriggers,
                               beam = " ".join(self.location.to_megalib()),
                               spectrum = " ".join(spectrum),
                               flux = flux))
        
        source_file.close()
    
    def run(self, log_path=None, seed = None):
        '''
        Run the simulations
        
        Args:
            log_path (Path): Path to output Cosima log. To stdout by default.
            seed (int or None): Random seed for Cosima (must be > 0). Use None 
            for a "random" random seed.
        '''

        logger.info("Starting MEGAlib simulation")
        
        output = None
        if log_path is not None:
            log_path = Path(log_path)
            output = open(log_path, 'w')
            logger.info("MEGAlib log output will be redirected to {}"\
                     .format(log_path.resolve()))

        command = ['cosima']

        if seed is not None:
            command += ['-s',str(seed)]

        command += [str(self.source_path)] 
            
        try:
            logger.info("Running command %s", command)
            subprocess.check_call(command,
                                  stdout = output,
                                  stderr = subprocess.STDOUT)
        except subprocess.CalledProcessError:
            logger.error("Call to Cosima has failed")
            raise
        except FileNotFoundError:
            logger.error("Call to Cosima failed. Is MEGAlib installed?")
            raise
            
        if log_path is not None:
            output.close()
            
