
from gdt.core.headers import Header, FileHeaders

import bctools

def new_header_class(name, kwd_dict, class_name = None):

    keywords = []

    for key,value in kwd_dict.items():

        if isinstance(value, list):

            # [value, comment]

            if len(value) != 2:
                raise RuntimeError('Wrong size. Input either value or [value, comments]')

            keywords.append((key, value[0], value[1]))

        else:

            keywords.append((key, value))

    if class_name is None:
        class_name = name
    
    cls = type(class_name, (Header,), {'name':name, 'keywords':keywords})

    return cls
    
def new_file_headers_class(hdr_dict, class_name = None):

    _header_templates = []

    for name,kwd_dict in hdr_dict.items():

        _header_templates += [new_header_class(name, kwd_dict)()]

    if class_name is None:
        class_name = '_'.join(hdr_dict.keys())

    cls = type(class_name, (FileHeaders,), {'_header_templates':_header_templates})

    return cls
