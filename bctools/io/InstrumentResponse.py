"""
All-sky detector response matrices --aka Instrument Response Functions (IRFs)--
"""
import numpy as np

import healpy as hp

import logging
logger = logging.getLogger(__name__)

from bctools.exceptions import NotYetImplemented, UserError
from bctools.util.coords import SpacecraftCoords, SkyCoords
from scoords import Attitude
from bctools.loc import LocTable
from bctools.containers.histogram import Histogram, Axis
from bctools.sims import DRM
import bctools.util.units as u
from bctools.config import get_config, set_config

from yayc import Configurator

from pathlib import Path

import h5py as h5

import argparse

import time
import textwrap

from mhealpy import HealpixBase

class InstrumentResponse(HealpixBase):
    """
    Input/output access of HDF5 files containing Detector Response Matrices (DRMs) 
    for all detectors and multiple points in the sky. 

    There is one DRM (photon energy vs energy channel) per detector per pixel 
    in a HEALPix grid.

    Please be aware that you need to call `close()` when you are done. Or even 
    better, use it as a context manager, e.g.::

        with InstrumentResponse("irf.h5") as irf:
    
            coord = SpacecraftCoords(0, 0)
    
            drm = irf.get_drm("detector1", coords = coord)

            # Do something with drm

    This automatically closes the file, even if an exception occurred. 

    Args:
        filename (Path): Path to HDF5 containing DRMs
        nside (int or None): It not None, a new file will be created with this 
            Healpix nside.
        scheme (str): RING or NESTED Healpix ordering scheme

    """

    """
    HDF5 file structure:
    - root
      * attrs:
        - scheme: NESTED or RING HEALPix scheme
        - nside: HEALPix NSIDE
      * datasets:
        - 3D array DRM, one per detector, named after detector ID
          - Axis 0: pixel number
          - Axis 1: photon energy
          - Axis 2: energy channels
          * attrs:
            - photon_energy_binning: bin edges of MC truth energy
            - energy_channels_binning: bin edges of energy channels
    """

    def __init__(self, filename, nside = None, scheme = 'ring'):

        if nside is None:
            # Read file
            self._file = h5.File(filename,'r')

            # Assume 'ring' scheme for backwards compatibility
            super().__init__(nside = self._file.attrs['nside'],
                             scheme = self._file.attrs.get('scheme','ring'))
            
        else:
            # Create new file
            self._file = h5.File(filename,'w-')

            self._file.attrs['nside'] = nside
            self._file.attrs['scheme'] = scheme

            super().__init__(nside = nside,
                             scheme = scheme)        

    @property
    def filename(self):
        """
        Path to on-disk file containing DRMs
        
        Return:
            Path
        """
        
        return Path(self._file.filename)

    def photon_energies(self, detector):
        """
        Returns the axis corresponding to the MC truth photon energies.

        Args:
            detector (str): Detector ID
        
        Return:
            Axis: Labeled 'energy'
        """

        axis = Axis(self._file[detector].attrs['photon_energy_binning'],
                    label = "energy",
                    scale = 'log')


        return axis
        
    @property
    def detectors(self):
        """
        List of detector IDs
        """

        return [detector for detector in self._file]
        
    def energy_channels(self, detector):
        """
        Returns the axis corresponding to the energy channels

        Args:
            detector (str): Detector ID
        
        Return:
            Axis: Labeled 'channels'
        """

        axis = Axis(self._file[detector].attrs['energy_channels_binning'],
                    label = "channels",
                    scale = 'log')

        return axis

    def get_drm(self, detector, pix = None, coords = None, interp = True):
        """
        Return a single DRM (2D) corresponding to the input pixel or coordinate.

        If interp is true, the output will be a bilinear interpolation from the 
        4 closest pixels to the coordinate. Otherwise, the center of the pixel 
        the input coordinate corresponds to will be used.

        Args:
            detector (str): Detector ID
            pix (int): HEALPix pixel
            coords (SpacecraftCoords): Local coordinate
            interp (bool): whether or not to interpolate

        Return:
            Histogram: Photon energies axis is called "energy", energy channels
                axis is called "channels"
        """

        if coords is not None and interp is True:

            pix,weights = self.get_interp_weights(coords.theta,
                                                  coords.phi)
            
            eff_area = (weights[0]*self._file[detector][pix[0]] + 
                        weights[1]*self._file[detector][pix[1]] + 
                        weights[2]*self._file[detector][pix[2]] + 
                        weights[3]*self._file[detector][pix[3]])

        else:

            if pix is None:

                if coords is None:
                    raise UserError("Either pix or coords must be set")
                    
                pix = self.ang2pix(coords.theta,
                                   coords.phi)
                
            eff_area = self._file[detector][pix] 

        drm = DRM(self.photon_energies(detector),
                  self.energy_channels(detector),
                  eff_area,
                  detector = detector)

        return drm
    
    
    @classmethod
    def from_rsps(cls, rsp_dict, filename, scheme = 'ring'):
        """
        Create an ``InstrumentResponse`` from a list of GBM RSP files

        Args:
            rsp_dict (dict of list of RSP): Lists of .rsp files, one list per 
                detector. Use detector name as dict key. The lists contain
                one entry per pixel in a HEALPix grid.
            filename (Path): Path to HDF5 output file to store the DRM
            nest (bool): True for NESTED scheme. False for RING
        """

        # Check nside is the same for all detectors
        npix = []

        for rsps in rsp_dict.values():
            npix += [len(rsps)]

        if len(np.unique(npix)) != 1:
            raise ValueError("The RSP lists for all detectors "
                             "must have the same size")
        
        # Create file
        try:
            obj = cls(filename, hp.npix2nside(npix[0]), scheme)
        except OSError:
            logger.error("Instrument response file already exists")
            raise

        # Fill IRF from RSPs
        for detector,rsp_list in rsp_dict.items():

            # Set size, including under/overflow
            drm0 = DRM.from_gbm_rsp_file(rsp_list[0])

            drm_size = (obj.npix,
                        drm0.photon_energies.nbins,
                        drm0.energy_channels.nbins)
            
            obj._file.create_dataset(detector, drm_size, dtype = 'f')

            # Fill
            for pix in range(obj.npix):
                
                drm = DRM.from_gbm_rsp_file(rsp_list[pix])

                if drm.axes != drm0.axes:
                    raise ValueError("All RSPs for a detector need to have the "
                                     "same photon energy and energy channel "
                                     "binning")
                
                obj._file[detector][pix] = np.array(drm)

            # Add single detector attributes
            obj._file[detector].attrs['photon_energy_binning'] = \
                                                     drm0.photon_energies.edges
            obj._file[detector].attrs['energy_channels_binning'] = \
                                                     drm0.energy_channels.edges
            
        return obj
                
    def close(self):
        """
        Close the HDF5 file containing the DRMs
        """

        self._file.close()
        
    def __enter__(self):
        """
        Start a context manager
        """

        return self
        
    def __exit__(self, type, value, traceback):
        """
        Exit a context manager
        """

        self.close()


def rsp_extractor(argv=None):
    """
    Obtain the specific RSP file for a given burst.

    Entry point for bc-rsp-extractor
    """
    
    # Logging format
    logging.Formatter.converter = time.gmtime
    logging.basicConfig(format=('%(levelname)s (%(asctime)s) '
                                '[%(filename)s:%(lineno)d] %(message)s'),
                        level=logging.INFO)

    aPar = argparse.ArgumentParser(
                   usage = ("%(prog)s config "
                            "[--help] [options]"),
                   description = textwrap.dedent(
                   """
                   Obtain the RSP for a given burst.

                   This app takes as input a HDF5 file containing the detector
                   response matrices for all directions and all detectors. 
                   It outputs a .rsp FITS file compatible with the gbm-data-tools
                   corresponding to a specific burst for each detector.

                   You can either input coodinate in the spacecraft frame, or input
                   RA/Dec and attitude.
                   """),
                   formatter_class=argparse.RawTextHelpFormatter)

    aPar.add_argument('config',
                      help="YAML config file")

    aPar.add_argument('--irf',
                      help="Input instrument response HDF5 file")

    aPar.add_argument('--spacecraft_coords',
                      help=("Get a time-independent RSP for a given local "
                            "direction [degrees]"),
                      nargs = 2,
                      metavar = ("ZENITH", "AZIMUTH"),
                      type = float)

    aPar.add_argument('--pixel',
                      help=("Get a time-independent RSP for a given "
                            "HEALPix pixel of the IRS in local coordinates"),
                      nargs = '?',
                      metavar = ('pix'),
                      type = int)
    
    aPar.add_argument('--coords',
                      help=("Get a time-independent RSP for a given "
                            "direction in celestial coordinates [degrees]"),
                      nargs = 2,
                      metavar = ("RA", "Dec"),
                      type = float)

    aPar.add_argument('--attitude',
                      help=("Quaternion describing the attitude"),
                      nargs = 4,
                      type = float)

    aPar.add_argument('--prefix',
                      help=("Prefix for output files. "
                            "i.e. <prefix><detector_id>.rsp. "
                            "Use it to specify the output directory as well"),
                      default = '')
    
    aPar.add_argument('--overwrite',
                      help=("whether old versions of the file are overwritten."
                      "Defaults to false."),
                      default = 'False')
    
    args = aPar.parse_args(argv)

    # Config
    config = Configurator.open(args.config)
    set_config(config)
    
    # Do different things depending on the type
    if args.spacecraft_coords is not None:

        # Local coordinates independent of time

        coord = SpacecraftCoords(args.spacecraft_coords[0]*u.deg,
                                 args.spacecraft_coords[1]*u.deg)

    elif args.pixel is not None:

        with InstrumentResponse(args.irf) as irf:

            lon, lat = irf.pix2ang(args.pixel, lonlat = True)

            coord = SpacecraftCoords((90 - lat) * u.deg,
                                     lon * u.deg)

            logger.info(f"Querying pixel {args.pixel} out of {irf.npix}, "
                        "which corresponds to coordinates "
                        f"(lon,lat) = ({lon:.2f}, {lat:.2f}) deg")
        
    elif args.coords is not None and args.attitude is not None:

        attitude = Attitude.from_quat(args.attitude)

        coord = SkyCoords(args.coords[0]*u.deg,
                          args.coords[1]*u.deg).to_spacecraftcoords(attitude)

    else:
        
        raise NotYetImplemented("Specify a coordinate in the spacecraft frame, or RA/Dec and attitude.")
    
    with InstrumentResponse(args.irf) as irf:

        for detector in irf.detectors:

            rsp = irf.get_drm(detector,
                              coords = coord).to_gbm_rsp()

            # Output
            filename = Path("{}{}.rsp".format(args.prefix,
                                              detector))

            rsp.write(directory = filename.parent, filename = filename.name, overwrite=args.overwrite)

