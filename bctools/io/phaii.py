from bctools.config import get_config
from .headers import new_header_class, new_file_headers_class

from gdt.core.phaii import Phaii
from gdt.core.data_primitives import Ebounds, Gti, TimeEnergyBins

class BinnedData(Phaii):

    @property
    def detector(self):
        """(str): The detector name"""
        return self.headers['PRIMARY']['INSTRUME']

    @detector.setter
    def detector(self, detector):
        self.headers['PRIMARY']['INSTRUME'] = detector
    
class ContinousBinnedData(BinnedData):
    pass

class AlertTriggerData(BinnedData):
    pass
    
