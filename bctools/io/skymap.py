from astropy.io import fits
from astropy.time import Time

from bctools.loc import SkyMap

from bctools.config import get_config
from .headers import new_header_class, new_file_headers_class

class BurstCubeSkyMap(SkyMap):

    def write(self,
              filename,
              overwrite = False,
              obs_id = None,
              trigger = None,
              trigtime = None,
              time_range = None,
              exposure = None,
              energy_range = None,
              seqpnum = None,
              *args, **kwargs):

        header = new_file_headers_class(get_config()['io:headers:skymap'])()
        primary_header = header['PRIMARY']
        healpix_header = header['HEALPIX']

        # Common keywords
        user_header = {} 

        if obs_id is not None:
            user_header['OBS_ID'] = obs_id

        if trigger is not None:
            user_header['TRIGGER'] = trigger
            
        if trigtime is not None:
            trigtime = Time(trigtime, format = 'bcmet')
            user_header['TRIGTIME'] = trigtime.bcmet
            user_header['TRIGUTC'] = trigtime.isot

        if time_range is not None:
            tstart = Time(time_range.tstart, format = 'bcmet')
            tstop  = Time(time_range.tstop, format = 'bcmet')
            user_header['TSTART'] = tstart.bcmet
            user_header['TSTOP'] = tstop.bcmet
            user_header['DATE-OBS'] = tstart.isot
            user_header['DATE-END'] = tstop.isot

        if exposure is not None:
            user_header['EXPOSURE'] = exposure.sec
            
        if energy_range is not None:            
            user_header['E_MIN'] = energy_range.emin
            user_header['E_MAX'] = energy_range.emax
            user_header['E_UNIT'] = 'keV' # GDT default

        if seqpnum is not None:
            user_header['SEQPNUM'] = seqnum

        for key,value in user_header.items():

            if key in primary_header:
                primary_header[key] = value

            if key in healpix_header:
                healpix_header[key] = value

        # Healpix hdu
        hdu = self.get_fits_hdu()

        hdu.header.strip() # Remove generic FITS keywords like XTENSION
        
        for key,value in hdu.header.items():
            if key in healpix_header:
                healpix_header[key] = value
                
        healpix_header['LASTPIX'] = self.npix

        header.update() # Update DATE

        hdu = fits.BinTableHDU.from_columns(hdu.columns, header['HEALPIX'])

        # Write
        hdulist = fits.HDUList([fits.PrimaryHDU(header = header['PRIMARY']), hdu])

        hdulist.writeto(filename, overwrite = overwrite)

