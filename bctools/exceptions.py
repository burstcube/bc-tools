'''
Custom exceptions
'''

class UserError(Exception):
    '''
    Raise to blame the user!
    '''

class CommandLineError(UserError):
    '''
    Raise for a missing or incompatible commanline argument. This is usually 
    caught by the main function and use as input for ArgParser.error()
    '''

class ConfigError(UserError):
    '''
    Raise when there is a problem with the configuration
    '''

class NotYetImplemented(Exception):
    '''
    Raise to indicate that a given feature has not been implemented yet.
    Only use as a temporary placeholder.
    '''

class InternalLogicError(RuntimeError):
    '''
    Raise when the program wanders into an option if shouldn't. 
    '''

class ParsingError(Exception):
    '''
    Raise when there as a problem parsing a file
    '''
    
class NotSupported(Exception):
    '''
    Use when the user tries to access a functionality that is not implemented.
    Unlike NotYetImplemented, this indicates a feature that is not planned to
    be implemented in the near future.
    '''
