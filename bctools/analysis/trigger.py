import logging
logger = logging.getLogger(__name__)

from warnings import warn

from gdt.core.background.binned import Polynomial
from gdt.core.binning.binned import rebin_by_edge_index
from gdt.core.data_primitives import TimeBins, TimeRange
from gdt.core.binning.unbinned import bin_by_time
from gdt.core.plot.lightcurve import Lightcurve
from gdt.core.plot.spectrum import Spectrum
from gdt.core.data_primitives import TimeBins
from gdt.core.background.fitter import BackgroundFitter
from gdt.core.background.binned import Polynomial
from gdt.core.binning.binned import combine_into_one, rebin_by_time
from gdt.core.collection import DataCollection
from gdt.core.binning.binned import rebin_by_edge_index
from gdt.core.spectra.functions import PowerLaw, Comptonized, Band
from gdt.core.spectra.fitting import SpectralFitterPgstat, SpectralFitterCstat
from gdt.core.plot.model import ModelFit
from gdt.core.data_primitives import Ebounds

from bctools.io import EventData, SpacecraftFile, AlertTriggerData
from bctools.config import get_config
from bctools.loc import TSMap, NormLocLike, SkyMap
from bctools.util.coords import SpacecraftCoords, SkyCoords
import bctools.util.units as bu
from bctools.io import InstrumentResponse, BurstCubeSkyMap
from bctools.loc import LocalLocTable
from bctools.spectra.spectrum import PowerLaw as PowerLawBC # Prevent conflict with GDT. Eventually bctools shouldn't define it's own
import bctools

from mhealpy import HealpixMap

from scoords import Attitude, SpacecraftFrame

from scipy.spatial.transform import Rotation

from copy import deepcopy, copy

import importlib

import numpy as np

from scipy.stats import chi2, norm, halfnorm
from scipy.signal import peak_prominences, argrelmax

from astropy.time import Time
from astropy.coordinates import SkyCoord
import astropy.units as u
import astropy.constants as c

import matplotlib.pyplot as plt

from .bayesian_blocks import bayesian_blocks

class Trigger:

    def __init__(self):
        """
        Trigger analysis handler.
        """
        
        self._lc = None

        self._signal_range = None
        self._bb_args = None

        self._lc_bayes = None
        self._bkg_times = None
        self._bkg_model = None    

        self._phaii_dict = DataCollection()

        self._sc_file = None

        self._skymap = None
        self._tsmap = None
        
        # Config
        self._config = get_config()

        self._active_detectors = self._config["pipelines:l1:trigger:active_detectors"]
        self._trig_erange = self._config["pipelines:l1:trigger:erange"]
        self._irf_path = self._config.absolute_path(self._config["pipelines:l1:trigger:irf"])

        
    @property
    def active_detectors(self):
        """
        Detectors used in the analysis, whether they triggered or not.
        """
        return self._active_detectors
        
    # TODO, from ATD
    #def set_trigger_data(self):
        
    def set_event_data(self, files, sc_files = None, attitude = None):
        """
        
        TODO: attitude input will be deprecated in favor of S/C HK files
        """

        # config
        ebounds_filenames = self._config["pipelines:l1:trigger:ebounds"]
        for det,path in ebounds_filenames.items():
            ebounds_filenames[det] = self._config.absolute_path(path)

        time_bin_width = self._config["pipelines:l1:trigger:tte:time_bin_width"]
        channel_energy_edges = np.array(self._config["pipelines:l1:trigger:tte:channel_energy_edges"])

        # Ebounds
        ebounds_dict =  DataCollection()
        
        for det in self.active_detectors:
            
            channel, emin, emax = np.loadtxt(ebounds_filenames[det],
                                             dtype = [("CHANNEL",int),
                                                      ("E_MIN",float),
                                                      ("E_MAX",float)],
                                             unpack = True)
            
            ebounds_dict[det] = Ebounds.from_bounds(emin, emax)

        # TTE to ATD
        for file in files:
            
            tte = EventData.open(file) 
            det = tte.detector
            
            # Calibrate
            tte.ebounds = ebounds_dict[det]
            
            # Bin
            phaii = tte.bin_data(bin_by_time, time_bin_width, time_ref = tte.time_range[0])
            phaii = phaii.rebin_energy(rebin_by_edge_index, 
                                       np.digitize(channel_energy_edges, phaii.data.emin) - 1)
 
            self._phaii_dict[det] = phaii

        # SC fils
        if sc_files is not None:
            raise RuntimeError("SC HK files not yet supported, use attitude")

        if attitude is None:
            raise RuntimeError("Attitude needed while SC are supported")
        else:
            warn('Attitude input will be deprecated in favor of S/C HK files  ', DeprecationWarning)

            # TODO multiple attitude for long GRBs. to_skyloctable already supports at list attitudes+durations,
            # other part still need to be updated (earth occultation prior, RSP2 (multiple extensions, one per time))
            if attitude.shape != ():
                raise RuntimeError("Currently only a single attitude is supported.")

            self._sc_file = SpacecraftFile(Time(np.array(tte.time_range) + tte.trigtime, format = 'bcmet'),
                                           Attitude(Rotation.concatenate([attitude.rot, attitude.rot]),
                                                    frame = attitude.frame))

    def set_trigger_data(self, files):

        for n,file in enumerate(files):
            
            atd = AlertTriggerData.open(file)

            self._phaii_dict[atd.detector] = atd.get_binned_data()

            if n == 0:
                # All files as assumed to have the same SC information
                self._sc_file = atd.get_spacecraft_file()
        
            
    def _get_plotall_axes(self):
        
        fig,ax = plt.subplots(self._config["pipelines:l1:trigger:plots:plotall:nrows"],
                              self._config["pipelines:l1:trigger:plots:plotall:ncols"],
                              dpi = 150)
        det_to_ax_index = self._config["pipelines:l1:trigger:plots:plotall:det_to_ax_index"]        

        ax = {det:ax[*index] for det,index in det_to_ax_index.items()}

        return fig,ax
        
    def plot_spectra(self, output):

        fig,ax = self._get_plotall_axes()
        
        # Workaround to keep the plots from GBM data tools in memory, 
        # Otherwise the garbage collector deletes them
        specplot = {}
        
        for det in self.active_detectors:
            spectrum = self._phaii_dict[det].to_spectrum()
            
            specplot[det] = Spectrum(data=spectrum, ax = ax[det])
            
            #ax[det_to_ax_index[det]].set_xlim(20,1400);

        fig.savefig(output)

    def plot_lightcurve(self, output):

        fig,ax = self._get_plotall_axes()
        
        # Workaround to keep the plots from GBM data tools in memory, 
        # Otherwise the garbage collector deletes them
        lcplot = {}

        time_bin_width_plot = self._config["pipelines:l1:trigger:tte:time_bin_width_plot"]
        
        for det in self.active_detectors:

            lc = self._phaii_dict[det].to_lightcurve()

            lcplot[det] = Lightcurve(data=lc.rebin(rebin_by_time, time_bin_width_plot), 
                                     ax = ax[det])

            ax[det].set_ylim(bottom = 0)

        fig.savefig(output)

    def plot_bb_lightcurve(self, output):

        fig,ax = plt.subplots()

        lc = self.get_lightcurve()
        lc_bayes, bb_indices = self.get_bayesian_blocks()

        dt_rebin = np.min(lc_bayes.widths)
        plot = Lightcurve(data=lc, ax = ax)

        plot_bayes = Lightcurve(data=lc_bayes, ax = ax)
        plot_bayes.lightcurve.color = 'navy'

        # Plot background manually
        # Eventually Trigger will return a BackgroundFitter object. Temporary workaround
        ax.plot(lc.centroids, self._lc_bkg_counts/lc.exposure, 
                color = 'red', ls = ':', label = "Fitted background")

        # Vertical lines showing the start and stop of the identified signal
        ax.axvline(self.get_signal_range().tstart, ls = "--", color = 'olive', label = "Signal start/stop")
        ax.axvline(self.get_signal_range().tstop, ls = "--", color = 'olive', label = "Signal start/stop")

        ax.set_ylim(bottom = 0);

        fig.savefig(output)

        
    def get_lightcurve(self, detector = None):
        """
        Get light curve
        
        detecotor is None = summ over all

        Return:
            TimeBins
        """

        if detector is not None:

            return (self._phaii_dict[det].slice_energy(self._trig_erange)
                                         .rebin_energy(combine_into_one)
                                         .to_lightcurve)
        
        if self._lc == None:
            # Not cached

            # TODO: Use only a subset of the detectors to maximize SNR
            
            counts = None

            for det in self.active_detectors:

                phaii = self._phaii_dict[det]

                # Workaround for bug in GBM data tools. 
                # The first and last bin have an incorrect exposure
                phaii = phaii.slice_time([(phaii.data.tstart[1], phaii.data.tstop[-2])])

                # Get lightcurve for a given energy range
                phaii = phaii.slice_energy(self._trig_erange)
                phaii = phaii.rebin_energy(combine_into_one)

                lc = phaii.to_lightcurve()

                if counts is None:
                    counts = lc.counts
                else:
                    counts += lc.counts

            self._lc = TimeBins(counts, lc.lo_edges, lc.hi_edges, lc.exposure)

        return self._lc
        
    def get_bayesian_blocks(self,
                            p0 = 0.05, 
                            buffer_blocks = 5,
                            signal_range = None,
                            max_iter = 100):
        """
        Bayesian blocks with iterative background fitting
        
        Args:
            p0 (float): False alarm probability for bayesian blocks algorithm
            buffer_blocks (int): Define the exclusion zone around the signal
                to compute the background in multiple of the size of the first and last
                bayesian block that are part of the signal
            bkg_times (array-like): Optional: provide a first guess for the time range
              that contained the signal. Must have shape (2,).
           max_iter (int): Maximum number of iterations. 

        Return:
           bb_index (array): Identified bayesian blocks, corresponding to indices of the ligh curve bins
           lc_bayes (TimeBins): Lightcurve bins in resulting bayesian blocks 
        """

        args = {'p0':p0,
                'buffer_blocks':buffer_blocks,
                'signal_range':signal_range,
                'max_iter':max_iter}

        if self._lc_bayes is None or args != self._bb_args:

            # Not cached or need to recompute
            signal_range, bkg_times, bkg_model, bkg_counts, bb_index, lc_bayes = self._get_bayesian_blocks(self.get_lightcurve(), **args)

            # Cache
            self._bb_args = args
            self._signal_range = signal_range
            self._bkg_times = bkg_times
            self._lc_bkg_model = bkg_model
            self._lc_bkg_counts = bkg_counts
            self._lc_bb_index = bb_index
            self._lc_bayes = lc_bayes

        return self._lc_bayes, self._lc_bb_index

    @staticmethod
    def _get_bayesian_blocks(lc,
                             p0 = 0.05, 
                             buffer_blocks = 5,
                             signal_range = None,
                             max_iter = 100):
        """
        Bayesian blocks with iterative background fitting

        Args:
            lc (TimeBins): Light curve
            p0 (float): False alarm probability for bayesian blocks algorithm
            buffer_blocks (int): Define the exclusion zone around the signal
                to compute the background in multiple of the size of the first and last
                bayesian block that are part of the signal
            bkg_times (array-like): Optional: provide a first guess for the time range
              that contained the signal. Must have shape (2,).
           max_iter (int): Maximum number of iterations. 

        Return:
           signal_range (tuple):  Signal start and stop time
           bkg_times (array): List of tuples containing the time ranges used to compute the backgrund 
           bkg_model (funtion): Polynomial fit to the background
           bb_index (array): Identified bayesian blocks, corresponding to indices of the ligh curve bins
           lc_bayes (TimeBins): Lightcurve bins in resulting bayesian blocks 
        """
        
        # Standarize input
        if signal_range is None:
            bkg_times = [(lc.range[0], lc.range[-1])]
        else:
            if len(signal_range) != 2:
                raise ValueError("Wrong signal_range shape.")

            signal_range = np.sort(signal_range)

            bkg_times = [(lc.range[0], signal_range[0]),
                         (signal_range[-1], lc.range[-1])]

        # Iterative method
        previous_bkg_times = []
        
        for i in range(max_iter):

            # ---- Fit bkg ------
            lc_bkg_times = TimeBins.merge([lc.slice(ti,tf) for ti,tf in bkg_times])

            bkg_model = Polynomial(counts = lc_bkg_times.counts[:,np.newaxis], 
                                   tstart = lc_bkg_times.lo_edges, 
                                   tstop = lc_bkg_times.hi_edges, 
                                   exposure = lc_bkg_times.exposure)

            bkg_model.fit(order= min(2,i)) # Remove first mean and linear component

            bkg_rate, bkg_rate_err = bkg_model.interpolate(lc.lo_edges, lc.hi_edges)

            bkg_counts = bkg_rate.flatten() * lc.exposure

            # ---- Bayesian blocks --------

            # Make effective time bin ~bkg rate so the effective rate is
            # constant (homogeneous poisson process)
            # Same as Giacomo's "trick" in 3ML
            # https://github.com/threeML/threeML/blob/e31db70daf8777ce12be7aa694b21efc3f15dae0/threeML/utils/bayesian_blocks.py#L171  

            lc_eff = TimeBins(counts = lc.counts,
                              lo_edges = lc.lo_edges,
                              hi_edges = lc.hi_edges,
                              exposure = bkg_counts) 
            
            bb_index = bayesian_blocks(lc_eff, p0 = p0)

            lc_bayes = lc.rebin(rebin_by_edge_index, bb_index)

            # ----- Find peaks ----
            peaks = argrelmax(lc_bayes.rates)[0]

            if len(peaks) == 0:
                # Need least 1 peak
                raise RuntimeError("Could not identify signal. Try with a different "
                                   "false positive rate or signal range initial guess.")

            prominence,left_base,right_base = peak_prominences(lc_bayes.rates, peaks)

            # Start and end of signal based on peaks, in histogram bins
            leftmost_base = np.min(left_base)
            rightmost_base = np.max(right_base)

            new_start_signal = bb_index[leftmost_base + 1]
            new_stop_signal = bb_index[rightmost_base]

            signal_tstart = lc.lo_edges[new_start_signal]    
            signal_tstop  = lc.hi_edges[new_stop_signal] 

            # Remove an extra chunk the size of the end blocks
            # Do not remove more than half the distance to the ends
            block_widths = lc_bayes.widths

            left_buffer = min(buffer_blocks*block_widths[left_base[0] + 1], (lc.lo_edges[new_start_signal] - lc.range[0])/2)
            right_buffer = min(buffer_blocks*block_widths[right_base[-1] - 1], (lc.range[1] - lc.hi_edges[new_stop_signal])/2)

            new_start,new_stop = np.digitize([lc.lo_edges[new_start_signal] - left_buffer,
                                              lc.hi_edges[new_stop_signal-1] + right_buffer], 
                                             lc.lo_edges) - 1

            # ----- Update background times ------
            # If they didn't change, then it has converge
            bkgex_tstart = lc.lo_edges[new_start]    
            bkgex_tstop  = lc.hi_edges[new_stop] 

            # Assume that the signal is fully contained. i.e. there is some background on both sides
            bkgex_tstart = max(bkgex_tstart, lc.lo_edges[bb_index[1]])
            bkgex_tstop = min(bkgex_tstop, lc.hi_edges[bb_index[-2]-1])
            
            bkg_times = [(lc.lo_edges[0], bkgex_tstart), (bkgex_tstop, lc.hi_edges[-1])]

            # Check if we are repeating a pattern.
            # Usually, when the method converges, 2 consecutive iteration have the same bayesian
            # blocks, but sometimes there is a 2-3 pattern that repeats with almost identical
            # block representations
            if bkg_times in previous_bkg_times and i >= 2:
                break

            previous_bkg_times += [bkg_times]

            if i == max_iter - 1:
                logger.warn("Maximum number of iterations reached without converging.")

        signal_range = (signal_tstart, signal_tstop)
                
        return signal_range, bkg_times, bkg_model, bkg_counts, bb_index, lc_bayes 

    def get_signal_range(self):
        """

        .. note::
            If ``get_bayesian_blocks()`` has not been called already, it will be called 
            with default parameters

        Return:
           (signal_start_time, signal_stop_time)
        """

        # Identify signal or used cached values
        if self._lc_bayes is None:
            # No cache, need to identify signal first
            self.get_bayesian_blocks()

        return TimeRange(*self._signal_range)
            
    def get_duration(self, quantile):
        """
        Compute the symmetric time range that contains a given quantile of bkg-subtracted counts

        .. note::
            If ``get_bayesian_blocks()`` has not been called already, it will be called 
            with default parameters

        Args:
            quantile (float or array): Quantile(s) e.g. .9 for T90, [1,.9,.5] for [T100, T90, T50]

        Return:
           float or array
        """

        # Identify signal or used cached values
        if self._lc_bayes is None:
            # No cache, need to identify signal first
            self.get_bayesian_blocks()

        return self._get_duration(lc = self.get_lightcurve(),
                                  bkg_counts = self._lc_bkg_counts,
                                  signal_range = self._signal_range,
                                  quantile = quantile)
            
    @staticmethod
    def _get_duration(lc,
                      bkg_counts,
                      signal_range,
                      quantile):
        """
        Compute the symmetric time range that contains a given quantile of bkg-subtracted counts

        Args:
            lc (TimeBins): Light curve
            bkg_model (array): Estimated background counts for light curve bin
            signal_range (tuple): Start and stop of signal
            quantile (float or array): Quantile(s) e.g. .9 for T90, [1,.9,.5] for [T100, T90, T50]

        Return:
           float or array: Same shape as quantile
        """
    
        # Percentile calculation
        start_signal,stop_signal = np.digitize(signal_range, lc.lo_edges)-1

        cumtime = lc.hi_edges[start_signal:stop_signal] - lc.lo_edges[start_signal]

        # Background subtraction 
        cumcounts = np.cumsum(lc.counts[start_signal:stop_signal] -
                              bkg_counts[start_signal:stop_signal])


        half_inv_quant = (1-np.array(quantile))/2

        tquant = np.diff(np.interp([half_inv_quant, 1-half_inv_quant],
                                    cumcounts/cumcounts[-1],
                                    cumtime), axis = 0)
        
        if np.isscalar(quantile):
            tquant = tquant.item()
        else:
            tquant = tquant.reshape(np.shape(quantile))

        return tquant
            
    def get_duration_error(self, quantile, containment = .68, nsamples = 100):
        """
        Compute the confidence interval for the burst duration using random sampling.

        .. note::
e            If ``get_bayesian_blocks()`` has not been called already, it will be called 
            with default parameters
        
        Args:
            quantile (float or array): Quantile(s) e.g. .9 for T90, [1,.9,.5] for [T100, T90, T50]
            containment (float or array): Confidence interval containment fraction

        Return:
            array: Negative and positive error. Shaped (2, size(containment), size(quantile)) 
        """
        
        if self._lc_bayes is None:
            # No cache, need to identify signal first
            self.get_bayesian_blocks()

        lc = self.get_lightcurve()
        lc_bayes = self._lc_bayes
        bkg_counts = self._lc_bkg_counts
        signal_range = self._signal_range

        # Create fake LC with mean signal counts on top of calculated background
        bb_mean_counts = lc_bayes.rates[np.digitize(lc.centroids, lc_bayes.lo_edges)-1] * lc.exposure
        
        start_signal,stop_signal = np.digitize(signal_range, lc.lo_edges)-1

        mean_counts = copy(bkg_counts)
        mean_counts[start_signal:stop_signal] = bb_mean_counts[start_signal:stop_signal]
        
        lc_sim = copy(lc)
        
        # Fluctuate counts and get samples
        tquant_samples = np.empty((nsamples,) + np.shape(quantile))

        for s in range(nsamples):

            logger.debug(f"Sample {s}/{nsamples}")

            lc_sim = TimeBins(counts = np.random.poisson(mean_counts),
                              lo_edges = lc_sim.lo_edges,
                              hi_edges = lc_sim.hi_edges,
                              exposure = lc_sim.exposure) 
            
            signal_range, bkg_times, bkg_model, bkg_counts, bb_index, lc_bayes = self._get_bayesian_blocks(lc_sim,
                                                                                            **self._bb_args)

            tquant = self._get_duration(lc = lc_sim,
                                        bkg_counts = bkg_counts,
                                        signal_range = signal_range,
                                        quantile = quantile)
            
            tquant_samples[s] = tquant

        # Quantiles, subtracting the median
        containment = np.asarray(containment)

        tquant_median = np.quantile(tquant_samples,
                                    q = .5,
                                    axis = 0)
        
        tquant_bounds = np.quantile(tquant_samples,
                                    q = [(1-containment)/2, 1-(1-containment)/2],
                                    axis = 0)

        tquant_error = tquant_bounds - tquant_median
        
        # Add the size of the bins to the error
        binning_error = (lc.widths[start_signal] + lc.widths[stop_signal])/2
        tquant_error[0] -= binning_error
        tquant_error[-1] += binning_error
        
        return tquant_error

        
    def get_skymap(self):

        if self._skymap is not None:
            return self._skymap
            
        # ---- Get iunput: signal, bkg and expectation ----
        signal_range = self.get_signal_range()

        data = dict()
        bkg = dict()

        bkg_dict = DataCollection()

        bkg_fit_poly_order = self._config["pipelines:l1:trigger:bkg_fit_poly_order"]
        
        # Unlike the signal identification, here we fit detector by detector.

        # Workaround to keep the plots from GBM data tools in memory, 
        # Otherwise the garbage collector deletes them
        plots = [] 

        fig,axes = plt.subplots(2,2, dpi = 150)

        for det in self.active_detectors:

            phaii = self._phaii_dict[det]

            # Get envents from the nominal energy range
            phaii = phaii.slice_energy(self._trig_erange)
            phaii = phaii.rebin_energy(combine_into_one)

            lc = phaii.to_lightcurve(energy_range=self._trig_erange)

            # Fit the background outside the identified signal region
            bkg_times = [(phaii.time_range[0], signal_range.tstart), 
                         (signal_range.tstop, phaii.time_range[1])]

            backfitter = BackgroundFitter.from_phaii(phaii, 
                                                     Polynomial, 
                                                     time_ranges = bkg_times)

            backfitter.fit(order=bkg_fit_poly_order)

            bkg_dict[det] = backfitter

            bkgd = backfitter.interpolate_bins(phaii.data.tstart, phaii.data.tstop)

            # Integrate the total data and background within the signal region
            lc_signal = lc.slice(signal_range.tstart,signal_range.tstop)

            data[det] = np.sum(lc_signal.counts)

            bkg[det] = backfitter.interpolate_bins(lc_signal.lo_edges, 
                                                  lc_signal.hi_edges).counts.sum()

        # Create lookup table
        loctable_spectrum_dict = self._config['pipelines:l1:trigger:loc:loctable_spectrum']

        loctable_spectrum = getattr(importlib.import_module(loctable_spectrum_dict['module']), 
                                    loctable_spectrum_dict['class'])(*loctable_spectrum_dict.get('args',()),
                                                                     **loctable_spectrum_dict.get('kwargs',{}))

        with InstrumentResponse(self._irf_path) as irf: 

            energy_channels = slice(*irf.energy_channels(self.active_detectors[0]).find_bin(self._trig_erange))

            local_loctable = LocalLocTable.from_irf(irf, loctable_spectrum,
                                                    energy_channels = energy_channels,
                                                    detectors = self.active_detectors)

        duration = (self._sc_file.timestamps[1:] - self._sc_file.timestamps[:-1]).sec
        sky_loctable = local_loctable.to_skyloctable(attitude = self._sc_file.attitude[:-1],
                                                     duration = duration)
        
        sky_loctable.set_background([bkg[det] for det in self.active_detectors])
        sky_loctable.set_data([data[det] for det in self.active_detectors])

        norm_likelihood = NormLocLike(sky_loctable)

        # ====== Sky map ======
        # Use the effective area (proportional to the expectation) of the
        # second best detector. This simulated the trigger condition of at least 2
        # detectors above threshold.
        aeff = np.sort(np.array([sky_loctable.get_expectation_map(det).data for det in self.active_detectors]), 
                       axis = 0)[2,:]

        # Assume for now Euclidean space and no dependency of luminosity vs redshift.
        # This can be later refined with simulations
        prior = HealpixMap(data = np.power(aeff, 3/2), 
                           base = sky_loctable.get_expectation_map(self.active_detectors[0]))
        prior.coordsys = 'icrs'

        # Remove Earth occulted
        # TODO: multiple attitude for long GRBs. This might be best to include local_loctable.to_skyloctable step rather than a
        # prior
        nadir = SkyCoord(az = 0*u.deg, alt = -90*u.deg, frame = 'altaz',
                         obstime = self._sc_file.timestamps[0], location = self._sc_file.orbit[0])
        apparent_earth_radius = np.arcsin(1/(1 + self._sc_file.orbit[0].earth_location.height/c.R_earth))

        prior[prior.query_disc(nadir, apparent_earth_radius.to_value(u.rad))] = 0

        
        skymap_nside = self._config["pipelines:l1:trigger:loc:nside"]
        skymap = BurstCubeSkyMap(nside = skymap_nside, coordsys = 'icrs')
        skymap.compute(norm_likelihood, prior = prior)

        tsmap = TSMap(nside = skymap_nside, coordsys = 'icrs')
        tsmap.compute(norm_likelihood)
        
        self._tsmap = tsmap
        
        self._skymap = skymap

        return self._skymap

    def get_snr(self):

        if self._tsmap is None:
            self.get_skymap()

        return np.sqrt(np.max(self._tsmap))
    
    def plot_skymap(self, output):

        img,ax = self.get_skymap().plot()
        ax.grid(alpha = .5)
        
        img.colorbar.set_label("Cummulative probability")
        
        ax.get_figure().savefig(output)


    def fit_spectrum(self):

        irf_path = self._config.absolute_path(self._config["pipelines:l1:trigger:irf"])

        skymap = self.get_skymap()

        # TODO: Combine multiple attitudes, for LGRBS
        attitude = self._sc_file.attitude[0]
        
        # ---- Get RSPs
        best_skyloc = SkyCoords(skymap.best_loc().ra.deg*bu.deg, 
                                skymap.best_loc().dec.deg*bu.deg)

        best_locloc = best_skyloc.to_spacecraftcoords(attitude = attitude)

        rsp_dict = DataCollection()

        with InstrumentResponse(irf_path) as irf:    

            for det in self.active_detectors:

                drm = irf.get_drm(det, coords = best_locloc)

                rsp_dict[det] = drm.to_gbm_rsp()

                # TODO: doesn't work yet, neet to implement write() for
                # BurstCube's RSP
                #rsp_dict[det].write(str(odir), f"{det}.rsp")

        # ----- Fit bkg
        # TODO: RSP rebinning because temporary mistmatch in channels. This will be fixed.
        channel_energy_edges = np.append(np.append(0,
                                                   np.array(self._config["pipelines:l1:trigger:tte:channel_energy_edges"])),
                                         2000)
        rsps =  DataCollection.from_list([rsp_dict[det].rebin(edge_indices = 
                                                              np.digitize(channel_energy_edges,
                                                                          rsp_dict[det].ebounds.low_edges(), right = False))
                                         for det in self.active_detectors
                                         ], 
                                         names = self.active_detectors)

        phaiis = self._phaii_dict # Temporary, while code is being refactored

        bkg_fit_poly_order = self._config["pipelines:l1:trigger:bkg_fit_poly_order"]

        signal_range = self.get_signal_range()

        phas = DataCollection.from_list(phaiis.to_pha(time_ranges = [(signal_range.tstart,signal_range.tstop)]), names = self.active_detectors)

        backfitters = DataCollection()

        # Remove under/overflow bins
        channel_masks = [pha.channel_mask for pha in phas]
        for mask in channel_masks:
            mask[0] = False
            mask[-1] = False
        
        for det in self.active_detectors:

            phaii = phaiis[det]

            # As opposed to during the signal identification or localization, 
            # here we fit the bkg channel by channel
            bkg_times = [(phaii.time_range[0], signal_range.tstart), (signal_range.tstop, phaii.time_range[1])]

            backfitter = BackgroundFitter.from_phaii(phaii, Polynomial, time_ranges = bkg_times)

            backfitter.fit(order = bkg_fit_poly_order)

            backfitters[det] = backfitter

        bkgds = backfitters.interpolate_bins(phaiis[self.active_detectors[0]].data.tstart, phaiis[self.active_detectors[0]].data.tstop)
        bkgds = DataCollection.from_list(bkgds, names=self.active_detectors)

        # We initialize with our PHAs, backgrounds, and responses:
        specfitter = SpectralFitterPgstat(phas.to_list(), 
                                          bkgds.to_list(), 
                                          rsps.to_list(), 
                                          method='TNC',
                                          channel_masks = channel_masks)

        # Instantiate a Band function
        fun = PowerLaw()

        # Max value of index to 20 instead of inf. Overflow errors otherwise
        fun.max_values[1] = 20

        specfitter.fit(fun, options={'maxiter': 1000})

        self._specfitter = specfitter

        return specfitter
        
    def plot_spectrum(self, output):

        specfitter = self._specfitter
        
        # --- Plot
        model_plot = ModelFit(fitter=specfitter, view='nufnu')

        model_plot.ax.get_figure().savefig(output)

    def plot_spectrum_residuals(self, output):

        specfitter = self._specfitter
        
        residuals_plot = ModelFit(fitter=specfitter)

        residuals_plot.ax.get_figure().savefig(output) 

