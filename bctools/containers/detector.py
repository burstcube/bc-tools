'''
Detector configuration, such as detectors, energy channels, etc.
'''

import logging
logger = logging.getLogger(__name__)

from bctools.util	import units as u
from bctools.exceptions import ConfigError
import numpy as np

class Detector:
    '''
    Contains the detector configuration

    Args:
        config (Configurator): Configurator with detector characteristics
    
    Attributes:
        energy_channel_edges (array): Definition of energy channels
        detectors (list): Detector IDs
    '''

    def __init__(self, config):

        self.energy_channel_edges = np.array(config['detector:energy_channels'])

        self._detectors = config['detector:detector_names']    
                
    @property
    def nchannels(self):

        return len(self.energy_channel_edges) - 1
        
    @property
    def min_energy(self):
        '''
        Lower bound of first energy channel
        '''

        return self.energy_channel_edges[0]

    @property
    def max_energy(self):
        '''
        Upper bound of last energy channepl
        '''

        return self.energy_channel_edges[-1]

    @property
    def detectors(self):
        '''
        Detector list
        '''

        return self._detectors
