import numpy as np

class Hit(object):
    """
    Single interaction with a SiPM
    
    Attributes:
        time (float): Time of the hits
        detector (str): Detector ID
        energy (float): Deposited energy
    """

    def __init__(self, detector, position, time, energy):

        self.time = time
        self.detector = detector
        self.position = position
        self.energy = energy
        
    def __str__(self):
        return "Detector: {}   Time: {:.6f}   Energy: {:.2e}".format(self.detector, self.time, self.energy)
    
    def __lt__(self, other):
        '''
        'Less than' overload for time sorting
        '''
        return self.time < other.time
    

class SimEvent(object):
    """
    Simulation event, with MC truth and possibly some triggered hits

    Args:
        particle (int): Incoming particle type ID (see Cosima manual)
        direction (list of float): [x,y,z] direction of incoming particle
        energy (float): kinetic energy of incoming partigle
        time (float): Time of the event. Hit times are relative to this
        hits (list of Hit): Triggered hits.
    
    Attributes:
        particle (int): Incoming particle type ID (see Cosima manual)
        direction (list of float): [x,y,z] direction of incoming particle
        energy (float): kinetic energy of incoming partigle
        time (float): Time of the event. Hit times are relative to this
        hits (list of Hit): Triggered hits.
    
    """

    def __init__(self, particle, direction,
                 energy, time, hits = []):

        self.particle = particle
        self.direction = direction
        self.energy = energy
        self.time = time

        self.hits = hits
        
    def __str__(self):
        return ("Particle ID: {:d} "
                "Time: {:.3E} s  "
                "Direction: ({:1f}, {:1f}, {:.1f})   "
                "Energy: {:.2E} keV   "
                "nHits: {:d}").format(self.particle,
                                      self.time,
                                      self.direction[0],
                                      self.direction[1],
                                      self.direction[2],
                                      self.energy,
                                      len(self.hits))
        
    def __lt__(self, other):
        '''
        'Less than' overload for time sorting
        '''
        return self.time < other.time

    def add_hit(self, hit):
        """
        Add a hit to the event

        Args:
            hit (Hit): triggered hit (detector, time and energy)
        """
        
        self.hits = np.append(self.hits, hit)
    
    def is_fully_defined(self):
        '''
        Returns True if no attribute is None

        Return:
            bool
        '''

        if self.particle is None or \
           self.direction is None or \
           self.energy is None or \
           self.time is None:
            return False

        return True
        
    
