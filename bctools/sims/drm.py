
from histpy import Histogram, Axis

from bctools.config import get_config

from gdt.core.data_primitives import ResponseMatrix

import numpy as np

from pathlib import Path

import h5py as h5

import logging
logger = logging.getLogger(__name__)

class DRMList:
    """
    Access a file with multiple detector response matrices (DRM).
    """

    @classmethod
    def open(cls, filename, path = '/'):
        """
        Open an HDF5 containing multiple DRMs, one for each detector.
        """

        new = cls()

        new._filename = filename
        new._file = h5.File(filename,'r')
        new._path = path

        return new
        
    def close(self):
        """
        Close the HDF5 file containing the DRMs
        """

        self._file.close()

    @property
    def detectors(self):
        """
        Tuple listing the available detectors
        """
        
        return tuple(self._file[self._path].keys())

    def __getitem__(self, detector):

        return DRM.open(self._filename, detector, path = self._path)
    
    def __enter__(self):
        """
        Start a context manager
        """

        return self
        
    def __exit__(self, type, value, traceback):
        """
        Exit a context manager
        """

        self.close()
    

class DRM(Histogram):
    """
    Holds a 2D matrix of photon energy (MC truth) vs energy channels 
    (estimated energy).

    Args:
        photon_energies (array or Axis): MC truth energy edges
        energy_channels (array or Axis): Estimated energy edges 
            (last bin is overflow)
        matrix (array): Axis 0 corresponds to photon energy, axis 1 to
            energy channels
    """

    def __init__(self, photon_energies, energy_channels, matrix, sumw2 = None, detector = None):

        self._detector = detector

        super().__init__(edges = [photon_energies, energy_channels],
                         contents = matrix,
                         sumw2 = sumw2,
                         labels = ['energy', 'channels'],
                         axis_scale = ['log', 'linear'])
        
        # Cache projection over energy axis
        self._eff_area = None

    @property
    def detector(self):
        return self._detector
        
    @classmethod
    def open(cls, filename, detector, path = '/'):
        """
        Loaf DRM from a HDF5 dataset

        Args:
            filename (str): Path to file
            detector (std): Detector name
            path (str): Path containing the various DRMs in file
        """

        hist = super().open(filename = filename,
                            name = path + "/" + detector)
        
        new = cls(hist.axes['energy'], hist.axes['channels'],
                  hist.full_contents, sumw2 = hist.sumw2,
                  detector = detector)
        
        return new
        
    @property
    def photon_energies(self):
        """
        Returns the axis corresponding to the MC truth photon energies.

        Return:
            Axis: Labeled 'energy'
        """

        return self.axes['energy']

    @property
    def energy_channels(self):
        """
        Returns the axis corresponding to the measured energy channels

        Return:
            Axis: Labeled 'channels'
        """

        return self.axes['channels']

    @classmethod
    def from_gbm_rsp(cls, rsp):
        """
        Create a detector response matrix using the contents of a gbm.data.RSP

        Args:
            rsp (gbm.data.RSP): GBM RSP object

        Return:
            DRM
        """

        # Lower bounds and upper bound are separated
        energy_lowerbounds, energy_upperbounds = rsp.drm.photon_bins
        energy_edges = np.append(energy_lowerbounds, energy_upperbounds[-1])

        # Edges is a numpy.recarray with the following labels
        # [('CHANNEL', '>i2'), ('E_MIN', '>f4'), ('E_MAX', '>f4')]
        channels_lowerbounds = rsp.drm.ebounds.low_edges()
        channels_upperbounds = rsp.drm.ebounds.hi_edges()
        channel_edges = np.append(channels_lowerbounds, channels_upperbounds[-1])

        drm = DRM(energy_edges,
                  channel_edges,
                  matrix = rsp.drm)

        return drm

    @classmethod
    def from_gbm_rsp_file(cls, filename):
        """
        Equivalent to ``from_gbm_rsp(RSP.open(filename), index)``

        Args:
            filename (Path): Path to .rsp(2) file
        """

        # Having the imports here prevents a circular import
        from bctools.io.rsp import RSP

        return cls.from_gbm_rsp(RSP.open(filename))
    
    def to_gbm_rsp(self):
        """
        Convert to GBM RSP object
        
        Return:
            gbm.data.RSP
        """

        # Having the imports here prevents a circular import
        from bctools.io.headers import new_header_class, new_file_headers_class
        from bctools.io.rsp import RSP

        # Matrix primitive
        channels_axis = self.energy_channels
        energy_axis = self.photon_energies

        specresp = ResponseMatrix(matrix = self[:],
                                  emin = energy_axis.lower_bounds,
                                  emax = energy_axis.upper_bounds,
                                  chanlo = channels_axis.lower_bounds,
                                  chanhi = channels_axis.upper_bounds)
        
        # header
        RSPHeaders = new_file_headers_class(get_config()['io:headers:RSP'])
        headers = RSPHeaders()
        headers["PRIMARY"]["INSTRUME"] = self.detector

        # RSP
        rsp = RSP.from_data(specresp, headers = headers)
                            
        return rsp

    def to_gbm_rsp_file(self, filename):
        """
        Save to GBM RSP format

        Args:
            filename (Path): Output path
        """
        
        filename = Path(filename)
        if filename.exists():
            raise FileExistsError("Response file {} already exists"
                                  .format(filename))
        else:
            rsp = self.to_gbm_rsp()

            rsp.write(directory = filename.parent, filename = filename.name)

    def effective_area(self, energy):
        """
        Get the total (interpolated) effective energy for a given energy

        Args:
            energy (float or array): Photon energy
        
        Return:
            float or array
        """
        
        if self._eff_area is None:
            self._eff_area = self.project('energy')

        if np.isscalar(energy):
            return self._eff_area.interp(energy)
        else:
            # Recursive for arrays
            return [self.effective_area(e) for e in energy]
        
        
    def channel_effective_area(self, energy, channel):
        """
        Get the effective area at a given photon energy taking into account only
        the events assigned to a given energy channel.

        Args:
            energy (float or array): Photon energy
            channel (int or array): Energy channel
        
        Return:
            float or array
        """

        if np.isscalar(energy):

            bins,weights = self.axes['energy'].interp_weights(energy)
            
            return (weights[0]*self[bins[0],channel] +
                    weights[1]*self[bins[1],channel])
            
        return np.array([self.channel_effective_area(e,c)
                         for e,c in np.broadcast(energy, channel)])
                
    def fold_spectrum(self, spectrum):
        """
        Return the expected count rate for a given spectrum for each energy 
        channel

        Args:
            spectrum (Spectrum): Flux (keV^-1 cm^-2 s^-1)

        Return:
            array: Counts per second for each energy channel
        """


        # Get the integrated flux for each photon energy bin
        bounds = self.photon_energies.bounds
        
        int_flux = spectrum.integrate(bounds)

        # Convolve with DRM
        counts = np.dot(self.contents.T, int_flux)

        return counts

    def plot(self, *args, **kwargs):

        ax,plot = super().plot(*args, **kwargs)

        ax.set_xscale('log')
        ax.set_yscale('log')

        ax.set_xlim(self.energy_channels.lo_lim.value,
                    self.energy_channels.hi_lim.value)

        ax.set_ylim(self.photon_energies.lo_lim.value,
                    self.photon_energies.hi_lim.value)
        
        return ax,plot


        
