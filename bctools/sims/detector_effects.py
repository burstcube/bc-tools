"""
Classes  that modify a hit in order to reproduce detector effects seen in 
calibration
"""

import sys

from numpy import random
from numpy import sqrt
import numpy as np

from astropy.units import Quantity, Unit
import astropy.units as u

from copy import copy

from abc import ABC, abstractmethod

class SimDetectorEffect(ABC):
    """
    Abstract detector effect class. Derived classes must implement the
    apply() method, which modify a Hit object

    """

    @abstractmethod
    def apply(self, hit):
        """
        Take a Hit output a new Hit that takes into account a detector effect
        
        Args:
           hit (Hit or None): Hit input object

        Return:
           Hit or None
        """
        
    @classmethod
    def from_name(cls, name, *args, **kwargs):
        """
        Instantiate a detector effect from the subclass name. e.g.:
        :code:`SimDetectorEffect.from_name('SimEnergyResolutionFromFit', *args, **kwargs)`
        is equivalent to :code:`SimEnergyResolutionFromFit(*args, **kwargs)`
        """

        try:
            subclass = getattr(sys.modules[__name__], name)
        except AttributeError:
            raise ValueError("Class 'name' is not a subclass of {}".format(cls.__name__))
            
        if not issubclass(subclass, cls):
            raise ValueError("Class 'name' is not a subclass of {}".format(cls.__name__))
        
        return subclass(*args, **kwargs)
                
    @classmethod
    def from_config(cls, config):
        """
        Return a detector effects object that will apply all effects in
        'simulations:detector_effects'

        Args: 
            config (Configurator): Configurator object

        Return:
            SimDetectorEffect
        """
        
        detector_effects = []

        for effect in config['simulations:detector_effects:order']:

            effect_config = config['simulations:detector_effects:effects'][effect]
            
            detector_effects += [SimDetectorEffect.from_name(effect_config['name'],
                                                             **effect_config['args'])]

        return SimDetectorEffects(detector_effects)
        
class SimEnergyResolutionFromFit(SimDetectorEffect):
    """
    Simulates the energy resolution of the instrument from a fit to various
    calibrating sources. 

    This detector effect replaces the energy of the hist with a random value
    obtained from for the energy from a normal distribution centered at the 
    input energy and a width equal to :math:`\Delta E/2.35482` where 
    :math:`\Delta E` is the full width at half maximum. 

    The value of :math:`\Delta E` depends on energy, and comes from a fit of 
    the form

    .. math::

        \\frac{\\Delta E}{E} = \\sqrt{c_0^2 + c_1^2 (E/\\mathrm{keV})^{-1} + c_2^2 (E/\mathrm{keV})^{-2} + ...}

    Note that :math:`\\Delta E/E \sim 1/\sqrt{E}` if the energy 
    resolution is only determined by the statistical error on the number of 
    photoelectrons produces.

    Args:
        coeffs (array, dict): List with coefficients [:math:`c_0`, :math:`c_1`, :math:`c_2`, ...].
            Higher order coefficients are assummed zero. If a dictionary, the
            keys correspond to the detectos IDs.
    """

    def __init__(self, coeffs, unit = u.keV):

        self._coeffs = coeffs

        if isinstance(coeffs, dict):
            self._detector_wise = True
        else:
            self._detector_wise = False

        self.unit = Unit(unit)
            
    def _apply(self, coeffs, hit):
        """
        apply() implementation.

        Args:
            coeffs (array, dict): List with coefficients 
                [:math:`c_0`, :math:`c_1`, :math:`c_2`, ...].
        """

        if hit is None:
            return None
        
        hit = copy(hit)

        energy = hit.energy.to_value(self.unit)
        
        width = (energy
                 * sqrt(np.sum([c*c/(energy**n)
                                for n,c in enumerate(coeffs)]))
                 / 2.35482) # full width at half maximum to sigma
        
        hit.energy = random.normal(loc = energy, scale = width) * self.unit

        return hit
        
    def apply(self, hit):
        """
        Replaces hit energy

        Args:
            hit (Hit or list): Hit to be modified
        """
        
        if self._detector_wise:
            try:
                coeffs = self._coeffs[hit.detector]
            except KeyError:
                raise KeyError("Coefficients for detector '{}' not available"
                               .format(hit.detector))
        else:
            coeffs = self._coeffs
            
        return self._apply(coeffs, hit)
            
class SimDetectorEffects(SimDetectorEffect):
    """
    Apply a collection of detector effects

    Args:
        detector_effects (list of SimDetectorEffect): Detector effecto to be
            applied, in order
    """

    def __init__(self, detector_effects):

        self._detector_effects = detector_effects

    def apply(self, hit):
        """
        Apply all detector effects

        Args:
            hit (Hit or None): Input hit

        Return:
            Hit or None: Output hit
        """
        
        for effect in self._detector_effects:
            hit = effect.apply(hit)

        return hit

class SimEfficiencyFromPoints(SimDetectorEffect):
    """
    Randomly drop a hit based on a arbitrary efficiency vs. energy.
    
    The class takes a set points and interpolates between them. The
    efficiency of an energy before/after the first/last point will
    be the eficciency of the first/last point.

    Args:
        energy (array, Quantity): List of energy point
        efficiency (array): efficiency at each of the energy points
        unit (astropy.unit): Unit of energy.
    """
    
    def __init__(self, energy, efficiency, unit = None):

        if (np.ndim(energy) != 1
            or np.ndim(efficiency) != 1
            or len(energy) != len(efficiency)):

            raise ValueError("'energy' and 'efficiency' must be 1D arrays of same length")
        
        self._energy = Quantity(energy, unit = unit) 
        
        self._efficiency = efficiency

    def apply(self, hit):
        """
        Returns None is the hit is dropped, and the same input hits otherwise.
        On average, it'll let pass a fraction of hits equal to the efficiency
        
        Args:
           hit (Hit or None): Input hit

        Return:
           None or same as hit
        """

        if hit is None:
            return None

        eff = np.interp(hit.energy, self._energy, self._efficiency)

        if random.uniform() < eff:
            return hit
        else:
            return None




    
