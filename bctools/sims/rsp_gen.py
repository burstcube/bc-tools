"""
Code for bc-rsp app.

See also: bctools.io.CosimaSource and bctools.io.CosimaSim
"""

import time
from time import sleep

import logging
logger = logging.getLogger(__name__)

import argparse
import os
import sys
import textwrap
import glob
from copy import copy,deepcopy

import yaml

from pathlib import Path

from bctools.spectra import Spectrum
from bctools.io import CosimaSource, CosimaSim, InstrumentResponse
from bctools.containers import Detector

from bctools.util.coords import SpacecraftCoords
from bctools.exceptions import CommandLineError,UserError
from bctools.config import Configurator
from bctools.sims import SimDetectorEffect, DRM

import astropy.units as u

from histpy import Histogram

import numpy as np
from numpy import random
from numpy import sqrt,log10,ceil

import healpy as hp

def main(argv=None):
    """
    This method will become the entry point (e.g. executable) for user to 
    generate a response file from the commandline
    """

    # Logging format
    logging.Formatter.converter = time.gmtime
    logging.basicConfig(format=('%(levelname)s (%(asctime)s) '
                                '[%(filename)s:%(lineno)d] %(message)s'),
                        level=logging.INFO)

    aPar = argparse.ArgumentParser(
                   usage = ("%(prog)s config_file working_dir [--help] "
                            "--ntriggers NTRIGGERS "
                            "[options]"),
                   description = textwrap.dedent(
                   """
                   Generate BurstCube detector response files
                   
                   The working_dir is the path to the directy where the final
                   detector response file and all intermediate steps will be 
                   saved. If files corresponding to intermediate steps are
                   already present, the program will skip those steps and use 
                   the available files.

                   All input units are in keV, s, cm and deg
                   """),
                   formatter_class=argparse.RawTextHelpFormatter)

    aPar.add_argument('config',
                      help="Configuration file")

    aPar.add_argument("working_dir",
                           help = "Output directory. Empty of non-existent")

    aPar.add_argument("--direction",
                      help=("zenith and azimuth of simulated source. "
                            "Full-sky by default, in a HEALPix grid."),
                      nargs='*',
                      type = float)

    aPar.add_argument('--ntriggers',
                      help="Maximum number of triggered hits (per direction)",
                      type = int)
    
    aPar.add_argument("--distance",
                      help="Distance to source. Default: %(default)s",
                      default = np.inf,
                      type = float)

    aPar.add_argument("--nthreads",
                      help = ("Number of parallel threads. "
                              "Only used for an all-sky detector response"),
                      type = int,
                      default = 1)

    aPar.add_argument("--part",
                      help = ("Usage: --part PART NPARTS "
                              "Divide the number of pixels into NPARTS and"
                              "only compute a single PART (0 indexed)"),
                      nargs = 2,
                      default = [0,1],
                      type = int)
    
    aPar.add_argument('--seed',
                      help="Random seed. Random by default.",
                      type = int)
    
    aPar.add_argument('--override',
                      help=("Override specifig config parameter(s). e.g. "
                            "simulations:geometry=bc.geo"),
                      nargs="*")

    args = aPar.parse_args(argv)

    # Handle config
    logger.info("Command line args: {}".format(vars(args)))
    
    config = Configurator(args.config)

    if args.override is not None:
        config.override(args.override)

    logger.info("Configuration:\n{}"
                .format(config.dump(default_flow_style=None, width=np.inf)))
        
    # Setup paramters
    random.seed(args.seed)

    if args.direction is None:
        # Full-sky detector

        part,nparts = args.part
        
        try:
            
            rsp_gen(args.working_dir,
                    config,
                    args.ntriggers,
                    args.nthreads,
                    nparts,
                    part,
                    args.distance)
            
        except CommandLineError as e:
            aPar.error(e)

    else:
        # Specific direction
        
        if len(args.direction) != 2:
            aPar.error("--direction needs 2 arguments")

        # TODO: use astropy coordinates
        location = SpacecraftCoords(zenith = (args.direction[0]*u.deg).to_value(u.rad),
                                    azimuth = (args.direction[1]*u.deg).to_value(u.rad),
                                    distance = args.distance)

        # Run
        try:
            
            rsp_gen_direction(args.working_dir,
                              config,
                              args.ntriggers,
                              location)
            
        except CommandLineError as e:
            aPar.error(e)

def rsp_gen(working_dir,
            config,
            ntriggers = None,
            nthreads = 1,
            nparts = 1,
            part = 1,
            distance = np.inf):
    """
    Generate an all-sky detector response.

    rsp_gen_direction() is called for each of the directions in a HEALPix grid.

    Args:
         working_dir (Path): Path to working directory
         config (Configurator): Global configuration object
         ntriggers (int): Stopping condition
         location (SpacecraftCoords): Locations of the source
         nthreads (int): Number of threads. One thread handles one direction at a time
         distance (float): Distance to source. Far field by default
    """

    wdir = Path(working_dir)
    
    nside = config['simulations:nside']
    npix = hp.nside2npix(nside)
    
    # ===== Create RSP files for all pixels in part =====
    if nparts > npix or part >= nparts:
        raise CommandLineError("NPARTS must be less or equal to number of pixels, "
                               "and PART less than NPARTS")

    npix_part = int(ceil(npix/nparts))
    
    children = []
    children_pix = []

    det = Detector(config)

    for pix in range(part*npix_part, min(npix, (part+1)*npix_part)):

        # Setup working directory for pixel
        wdir_pix = wdir / ("rsp_nside{nside}_ring_pix{pix:0{ndigits}d}"
                           .format(nside = nside,
                                   pix = pix,
                                   ndigits = int(log10(npix))+1))

        wdir_pix.mkdir(parents = True, exist_ok = True)
 
        pix_log = wdir_pix / "bc-rsp.log"
        
        # Check if RSP files already exist
        if (wdir_pix / "drm.h5").exists():
            logger.info("DRM file for pix %d/%d already exists. Skipping...",
                     pix, npix)
            continue

        # A child process per pixel
        try:
            pid = os.fork()
        except OSError:
            exit("Problem forking child. Zombies?")

        if pid == 0:

            # We are the child computing the RSP

            # Redirect all logging to file
            
            root_logger = logging.getLogger()

            for handler in root_logger.handlers[:]:
                root_logger.removeHandler(handler)

            logging.basicConfig(format=('%(levelname)s (%(asctime)s) '
                                        '[%(filename)s:%(lineno)d] %(message)s'),
                                level=logging.INFO,
                                filename = pix_log)

            logger.info("Computing RSP for pixel {}/{} (PID {})"
                        .format(pix, npix, os.getpid()))
            
            # Launch RSP generator
            theta,phi = hp.pix2ang(nside, pix)
            
            location = SpacecraftCoords(zenith = theta,
                                        azimuth = phi,
                                        distance = distance)
            
            rsp_gen_direction(wdir_pix,
                              config,
                              ntriggers,
                              location)

            exit()

        # We are the parent waiting for the children
        # We'll keep 'nthreads' children alive at a time
        logger.info(("Launched RSP generator for pixel %d/%d (PID %d). "
                     "See logs in %s."),
                    pix, npix, pid, pix_log)
        
        children += [pid]
        children_pix += [pix]
        
        while True:

            # Iterate over a _copy_ ([:]) of all children
            # and check their statuses
            for pid,pix in zip(children[:],children_pix[:]):

                # Non-blocking wait
                pid,status = os.waitpid(pid, os.WNOHANG)

                if pid != 0:
                    # One of the children finished, remove it from the list
                    
                    if status != 0:

                        # If the RSP for a single pixel fails cancel everything
                        # to be on the safe side.
                        logger.error("Error processing RSP for pixel %d", pix)
                        exit(1)

                    logger.info("Finished RSP for pix %d", pix)
                        
                    children.remove(pid)
                    children_pix.remove(pix)

                    break

            if len(children) < nthreads:
                # Ready for another pixel
                break
            else:
                # Still processing, 1s sleep prevents a busy loop
                sleep(1)
                    
    # Wait for the remainning children. We don't want zombies!
    for pid,pix in zip(children[:],children_pix[:]):

        pid,status = os.waitpid(pid, 0)

        if status != 0:
            
            # If the RSP for a single pixel fails cancel everything
            # to be on the safe side.
            logger.error("Error processing RSP for pixel %d", pix)
            exit(1)
            
        logger.info("Finished RSP for pix %d", pix)
            
        children.remove(pid)
        children_pix.remove(pix)

    # ==== Put all RSPs together in an all-sky RSP =====
    if nparts != 1:
        logger.info("NPARTS!=1, all-sky RSP will not be generated")
        exit()

    logger.info("Creating full detector response...")
        
    rsp_list = [("{wdir}/rsp_nside{nside}_ring_pix{pix:0{ndigits}d}"
                 "/drm.h5")
                .format(wdir = wdir,
                        nside = nside,
                        pix = pix,
                        ndigits = int(log10(npix))+1)
                for pix in range(npix)]
    
    rsp_peak_list = [("{wdir}/rsp_nside{nside}_ring_pix{pix:0{ndigits}d}"
                      "/drm_peak.h5")
                     .format(wdir = wdir,
                             nside = nside,
                             pix = pix,
                             ndigits = int(log10(npix))+1)
                     for pix in range(npix)]
    
    irf = InstrumentResponse.from_drm_files(rsp_list, wdir/"irf.h5")
    irf.close()

    irf_peak = InstrumentResponse.from_drm_files(rsp_peak_list, wdir/"irf_peak.h5")
    irf_peak.close()
    
def rsp_gen_direction(working_dir,
                      config,
                      ntriggers = None,
                      location = None):
    """
    Generate a detector response for a single direction.

    If the product from a given step is already available in the working dir, 
    that step will be skept. This is why most parameters are optional.
    
    Args:
         working_dir (Path): Path to working directory
         config (Configurator): Global configuration object
         ntriggers (int): Stopping condition
         location (SpacecraftCoords): Locations of the source
    """

    # Create working dir if needed
    wdir = Path(working_dir)

    try:
        wdir.mkdir(parents = True)
    except FileExistsError:
        if not wdir.is_dir():
            logger.error("Working path '{}' already exists "
                         "and it is not a directory"
                         .format(wdir.resolve()))
            raise
        elif os.listdir(wdir):
            logger.info("Non-empty working directory '{}' already exists"
                        .format(wdir.resolve()))
        else:
            logger.info("Empty working directory '{}' already exists."
                        .format(wdir.resolve()))

    # === Either read or create source file ===
    geometry = config.absolute_path(config['simulations:geometry'])
    
    sourcef_path = wdir / "mc.source"
    simf_suffix = wdir / "mc"    

    det = Detector(config)

    sim_energy_edges = config['simulations:energy_bins']

    if os.path.exists(sourcef_path):

        logger.info("Skipping source file creation, using {}".format(sourcef_path.resolve()))
        sourcef = CosimaSource.open(sourcef_path)
        
    else:

        if ntriggers is None:
            raise CommandLineError("Missing --ntriggers")

        if location is None:
            raise ValueError("Argument 'location' needed")

        sourcef = CosimaSource(Path(geometry), ntriggers)

        spectrum = Spectrum.from_name(config['simulations:spectrum:name'],
                                      **config['simulations:spectrum:args'])

        spectrum.set_elims(sim_energy_edges[0],
                           sim_energy_edges[-1])
                    
        sourcef.set_point_source(spectrum = spectrum,
                                 location = location)
        
        # Write source file
        sourcef.write(source_path = sourcef_path,
                      sim_path = simf_suffix)
            

    # === Run simulation or find sim files ===
    simf_paths = [Path(p) for p in
                  glob.glob("{}*.sim".format(simf_suffix.resolve()))]
    
    # Generate Cosima seed using numpy. This way the result
    # is deterministics if numpy.random.seed was set. If not,
    # we keep the record in the logs
    # This is generated _before_ checking if the .sim files already exist so
    # that the result is the same if the job is re-run with the Cosima outputs
    # alredy there.
    cosima_seed = random.randint(1,sys.maxsize)
    
    if not simf_paths:
        cosima_log = wdir / "mc.log"

        sourcef.run(log_path = cosima_log,
                    seed = cosima_seed)

        simf_paths = [Path(p) for p in
                      glob.glob("{}*.sim".format(simf_suffix.resolve()))]

    else:
        logger.info("Skipping Cosima simulation, using:\n" +
                    "\n".join([str(p) for p in simf_paths]))

    # === Fill detector reponse matrices ===

    # TODO: energies from config file
    drms = {detector:Histogram([sim_energy_edges * u.keV,
                                det.energy_channel_edges * u.keV],
                                labels = ['energy','channels'],
                                sumw2 = True)
            for detector in det.detectors}
    drms_peak = deepcopy(drms)

    # Setup up detector effects (e.g. energy resolution)
    detector_effects = SimDetectorEffect.from_config(config)
    
    sim = CosimaSim()

    for simf_path in simf_paths:

        sim_i = CosimaSim.open(simf_path, geometry, load_events = False)
        
        sim.merge(sim_i, shift = sim.tstop)

        for n,event in enumerate(sim_i.event_iter()):

            # Most events have only one hit, but we'll weight the entries in
            # the histogram by 1/nhits so nothing breaks for future experiments
            nhits = len(event.hits)

            if nhits < 1:
                continue
            
            weight = 1/nhits
            
            for hit in event.hits:

                # Large tolerance since MEGAlib doesn't keep many sigfigs
                full_absorption = np.isclose(hit.energy, event.energy, rtol = 0.01, atol = .01)  
                
                hit = detector_effects.apply(hit)

                if hit is None:
                    # Hit was dropped, most likely due to the
                    # efficiency detector effect
                    continue

                drms[hit.detector].fill(event.energy, hit.energy,
                                        weight = weight)

                if (full_absorption):
                    drms_peak[hit.detector].fill(event.energy, hit.energy,
                                                 weight = weight)

    # Prepare and safe
    for detector,drm in drms.items():
        drm = counts_to_drm(drm, sourcef, sim)
        drm.write(wdir / "drm.h5", detector)
        
    for detector,drm in drms_peak.items():
        drm = counts_to_drm(drm, sourcef, sim)
        drm.write(wdir / "drm_peak.h5", detector)

def counts_to_drm(hist, source, sim):
    """
    Scale a counts to create a detector response matrix and save it as a
    GBM RSP file 

    Args:
        hist (Histogram): Histogram with counts (must have an axis labeled 
            "energy")
        source (CosimaSource): Cosima source object containing the spectrum and 
            beam information
        sim (CosimaSim): Cosima sim object containing the total duration of the 
            simulation.
        filename (Path): Location in disk to save the RSP file
    """    

    # Do not modify input hist
    hist = deepcopy(hist)
    
    # Axes
    photon_energies = hist.axes['energy']
    energy_channels = hist.axes['channels']
        
    # Last channel is really an overflow, so combine
    # last bin with overflow contents.
    hist[hist.expand_dict({'channels':hist.end-1}, default = slice(None))] +=       hist[hist.expand_dict({'channels':hist.end}, default = slice(None))]

    if hist.sumw2 is not None:
        hist.sumw2[hist.expand_dict({'channels':hist.end-1}, default = slice(None))] += hist.sumw2[hist.expand_dict({'channels':hist.end}, default = slice(None))]

    #Convert counts to area
    # TODO: astropy units
    area_per_event = (1
                      /source.spectrum.integrate(photon_energies.bounds.to_value(u.keV))
                      /sim.duration)

    if np.isfinite(source.location.distance):
        # Flux is emission rate (1/s) in this case
        area_per_event *= 4 * np.pi * source.location.distance * source.location.distance

    # e.g. in case he spectrum is 0 in a given bin
    area_per_event[np.logical_not(np.isfinite(area_per_event))] = 0
        
    hist *= hist.axes.expand_dims(area_per_event, 'energy')

    # Write to disk
    drm = DRM(photon_energies,
              energy_channels,
              hist[:],
              sumw2 = None if (hist.sumw2 is None) else hist.sumw2[:])
    
    return drm
