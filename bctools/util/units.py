'''
Base and auxiliary units and constants used througout the library

This help keep track of the units we are usings and makes it easier to 
interact with other libraries or with the user (read in and display).

Usage example:: 

    >>> from bctools.util import units as u
    >>> energy = 500 * u.keV
    >>> print("{} MeV".format(energy/u.MeV))
    0.5 MeV

'''

import math

# NOTE: sphinx does not automatically recognized module member
# constants without a docstring, so we need to add '#:' to show them in the
# API documentation
# See https://github.com/sphinx-doc/sphinx/issues/1063 and fix this if resolved

# Time
second = 1 #: Base time unit
s = second #:

# Distance
cm = 1 #: Base distance unit

meter = 100*cm #:
m = meter #:

cm2 = cm*cm #:

# Energy
keV = 1 #: Base energy units

eV = 1e-3*keV #:
MeV  = 1e6*eV #:
GeV  = 1e9*eV #:
TeV = 1e12*eV #:

# Math constants
pi = 3.1415926535897932384626 #:
twopi = 2*pi #:
halfpi = pi/2 #:
inf = math.inf #:

# Angle
radian = 1 #: Base angle unit

rad = radian #:

deg = pi / 180.0 * radian #:

# Physical constants
