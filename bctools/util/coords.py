"""
Classes to interact with megalib
"""

from numpy import cos,sin,sqrt,arctan2,arccos
import numpy as np

from bctools.exceptions import ParsingError
from bctools.util	import units as u

from astropy.coordinates import PhysicsSphericalRepresentation, SkyCoord
import astropy.units as au

from scoords import SpacecraftFrame

class SphericalCoords:
    """
    Spherical polar coordinate using Physics convention
    
    Arg:
        theta (float): Zenith angle
        phi (float): Azimuth angle. Anti-clockwise from x-axis
        radius (float): Distance to point
    
    Attributes:
        theta (float): Zenith angle
        phi (float): Azimuth angle. Anti-clockwise from x-axis
        radius (float): Distance to point
    """
    
    
    def __init__(self, theta, phi, radius):

        self._coord = PhysicsSphericalRepresentation(phi*au.rad, theta*au.rad, radius)
                
    @property
    def cartesian(self):
        """
        Convert to cartesian coordinates
        """

        x,y,z = self._coord.to_cartesian().xyz
        
        return CartesianCoords(x,y,z)

    @property
    def theta(self):
        return self._coord.theta.rad
    
    @property
    def phi(self):
        return self._coord.phi.rad

    @property
    def radius(self):
        return self._coord.r
    
class CartesianCoords:
    """
    Cartesian coordinates

    Attribute:
        x (float): Distance along x-axis
        y (float): Distance along y-axis
        z (float): Distance along z-axis
    """

    def __init__(self, x,y,z):

        self.x = x
        self.y = y
        self.z = z    

    @property
    def spherical(self):

        radius = sqrt(self.x*self.x + self.y*self.y + self.z*self.z)
        
        return SphericalCoords(theta = arccos(self.z/radius),
                               phi = arctan2(self.y, self.x),
                               radius = radius)

class SpacecraftCoords(SphericalCoords):
    """
    Local spacecraft coordinates

    Arg:
        zenith (float): Zenith angle.
        azimuth (float): Azimuth angle. Anti-clockwise from x-axis
        distance (float): Distance to point
    """

    
    def __init__(self,zenith, azimuth, distance = 1):

        super().__init__(zenith, azimuth, distance)

    @property
    def zenith(self):
        return self.theta

    @property
    def azimuth(self):
        return self.phi

    @property
    def distance(self):
        return self.radius
    
    @classmethod
    def from_megalib(cls, params):
        """
        Construct location from a source.Beam argument in a MEGAlib 
        source file

        Args:
            params (list): List of parameter. Either 
                ['FarFieldPointSource', zenith, azimuth] or 
                ['(Restricted)PointSource', x, y, z]
        """

        if not isinstance(params, (list, tuple, np.ndarray)):
            raise TypeError("Params must be a list")
        
        nparams = len(params)
        
        if not (nparams > 0):
            raise ParsingError("At least one parameter expected")
        
        beam = params[0]

        if beam == 'FarFieldPointSource':

            if not (nparams == 3):
                raise ParsingError("3 parameters expected")

            return cls(zenith = float(params[1]) * u.deg,
                       azimuth = float(params[2]) * u.deg,
                       distance = u.inf)
            
        elif (beam == 'PointSource' or
              beam == 'RestrictedPoint' or
              beam == 'RestrictedPointSource'):

            if not (nparams == 4):
                raise ParsingError("4 parameters expected")

            coords = CartesianCoords(float(params[1]) * u.cm,
                                     float(params[2]) * u.cm,
                                     float(params[3]) * u.cm).spherical
            
            return cls(zenith = coords.theta,
                       azimuth = coords.phi,
                       distance = coords.radius)
        
        else:

            raise ParsingError("Unknown source beam type '{}'".format(beam))
            
    def to_megalib(self):
        """
        Return the location in a format that can be use in MEGAlib in
        the source.Beam argument of a source file

        Return:
            list: Paramters for either a FarFieldPointSource or a 
                RestrictedPointSource
        """

        if np.isfinite(self.distance):

            # Source at finite distance. e.g. for calibration

            cart = self.cartesian
            
            return ['RestrictedPoint',
                      '{:.17E}'.format(cart.x/u.cm),
                      '{:.17E}'.format(cart.y/u.cm),
                      '{:.17E}'.format(cart.z/u.cm)]

        else:

            return ['FarFieldPointSource',
                    '{:.17E}'.format(self.zenith/u.deg),
                    '{:.17E}'.format(self.azimuth/u.deg)]

    def to_skycoords(self, attitude):
        """
        Convert from local to celestial coordinates.

        .. note::
           The output distance is the same, only the direction
           is converted. This is planned to 
           change in the future.

        Args:
            attitude (Attitude): Spacecraft attitude
        
        Return:
            Skycoords
        """

        loc_coord = SkyCoord(self._coord,
                             frame = SpacecraftFrame(attitude = attitude))

        sky_coord = loc_coord.transform_to('icrs')

        return SkyCoords(sky_coord.ra.rad,
                         sky_coord.dec.rad,
                         self.distance)
        
class SkyCoords(SphericalCoords):
    """
    Celestial coordinates. 

    J2000 unless otherwise noticed.
    """

    def __init__(self, ra, dec, distance = 1):

        super().__init__(u.halfpi - dec, ra, distance)
    
    @property
    def ra(self):
        """
        Right ascension
        """

        return self.phi

    @property
    def dec(self):
        """
        Declination
        """
        
        return u.halfpi - self.theta
    
    @property
    def distance(self):
        return self.radius
    
    def to_spacecraftcoords(self, attitude):
        """
        Convert to local coordinates

        .. note::
           The output distance is the same, only the direction
           is converted.  This is planned to 
           change in the future.

        Args:
            attitude (Attitude): Spacecraft attitude
        
        Return:
            SpacecraftCoords
        """

        sky_coord = SkyCoord(self._coord,
                             frame = 'icrs')

        loc_coord = sky_coord.transform_to(SpacecraftFrame(attitude = attitude))

        loc_coord = loc_coord.represent_as('physicsspherical')
        
        return SpacecraftCoords(loc_coord.theta.rad, loc_coord.phi.rad, loc_coord.r)
            
class Attitude:
    """
    Spacecract attitude

    Args:
        quat (array): Quaternion. Array with 4 elements
    """
    
    def __init__(self, quat):

        if len(quat) != 4:
            raise ValueError("Wrong quaternion shape")
        
        self._quat = np.array(quat, dtype=float)

    @property
    def quaternion(self):
        """
        Get attitude in the form of a quaternion (4 element array)

        Return:
            array: 4 elements
        """

        return self._quat
