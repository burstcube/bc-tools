from random import seed, randrange
import numpy as np
import pandas as pd
import pickle

import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier

from process_inputs import calculate_variables

class_labels = {0: 'DISTPAR',
                1: 'GRB',
                2: 'LOCLPAR',
                3: 'SFLARE',
                4: 'SGR'
                }

def eval_from_csv():
    data_file = 'gbm_classification_input.csv'
    model_file = 'gradbdt.pkl'

    df = pd.read_csv(data_file)

    feature_cols = ['Sun_Separation', 'Earth_Separation', 'McIlwain_L', 'Q1', 'Q2', 'Q3', 'Q4', 'Hardness_Ratio']

    X = df.loc[:, feature_cols]
    y = df.loc[:, 'GBM_Class']

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=42)

    model = pickle.load(open(model_file, mode='rb'))

    y_pred = model.predict(X_test)

    print(y_pred, y_test)

def eval_from_fits(filename):

    model_file = 'gradbdt.pkl'
    print(f"Loading {model_file}...\n")
    model = pickle.load(open(model_file, mode='rb'))

    print("Pulling variables from fits file...")
    feature_cols = ['Sun_Separation', 'Earth_Separation', 'McIlwain_L', 'Q1', 'Q2', 'Q3', 'Q4', 'Hardness_Ratio']
    vars = calculate_variables(filename)
    #print(vars)
    del vars[-2:]
    
    data = {feature_cols[i]: [vars[i]] for i in range(len(feature_cols))}
   
    df = pd.DataFrame(data)
    y_pred = model.predict(df)[0]
    y_prob = round(model.predict_proba(df)[0][y_pred], 4)
    print("Predicted class:", class_labels[y_pred], "with probability", y_prob*100., "%\n")

if __name__ == '__main__':
    #eval_from_csv()
    eval_from_fits('glg_trigdat_all_bn230815131_v01.fit') #solar flare example
    eval_from_fits('glg_trigdat_all_bn230819566_v01.fit') #GRB example
