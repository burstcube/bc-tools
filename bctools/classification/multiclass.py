#=================================================================
# multiclass.py: Code for optimizing training, testing, and saving
#                multiclass classifiers
#-----------------------------------------------------------------
# Author: Ava Myers (ava.myers@nasa.gov)
#=================================================================

#=================================================================
# Imports
#=================================================================

# Generic python imports
from csv import reader
from random import seed, randrange
from math import sqrt, exp, pi
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
from collections import Counter
import pickle
import seaborn as sns

# ML specific imports
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

from sklearn.metrics import *
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.inspection import permutation_importance

from imblearn.over_sampling import SMOTE

from xgboost import XGBClassifier

#=================================================================
# Uses a GridSearch CV to find the optimal version of each kind of 
# classifier for comparison
# x: training variable data
# y: true class labels
# returns: dictionary of optimized classifier models
#=================================================================
def optimize_model(x,y):

    rf = RandomForestClassifier(random_state=42)
    svc = SVC(probability=True, random_state=42)
    dt = DecisionTreeClassifier(random_state=42)
    kNN = KNeighborsClassifier()
    gbdt = GradientBoostingClassifier(random_state=42)
    xgb = XGBClassifier()
    mlp = MLPClassifier(random_state=42)

    rf_params = {}
    rf_params['classifier__n_estimators'] = [10, 50, 100, 250]
    rf_params['classifier__max_depth'] = [5, 10, 20]
    rf_params['classifier__class_weight'] = [None, {0:1,1:5}, {0:1,1:10}, {0:1,1:25}]

    svc_params = {}
    svc_params['classifier__C'] = [10**-2, 10**-1, 10**0, 10**1, 10**2]
    svc_params['classifier__class_weight'] = [None, {0:1,1:5}, {0:1,1:10}, {0:1,1:25}]

    dt_params = {}
    dt_params['classifier__max_depth'] = [5,10,25,None]
    dt_params['classifier__min_samples_split'] = [2,5,10]
    dt_params['classifier__class_weight'] = [None, {0:1,1:5}, {0:1,1:10}, {0:1,1:25}]

    kNN_params = {}
    kNN_params['classifier__n_neighbors'] = [2,5,10,25,50]

    gbdt_params = {}
    gbdt_params['classifier__n_estimators'] = [10, 50, 100, 250]
    gbdt_params['classifier__max_depth'] = [5, 10, 20]

    xgb_params = {}
    xgb_params['classifier__min_child_weight'] = [1, 5, 10]
    xgb_params['classifier__gamma'] = [0.5]
    xgb_params['classifier__subsample'] = [0.8]
    xgb_params['classifier__colsample_bytree'] = [1.0]
    xgb_params['classifier__max_depth'] = [1, 3, 5, 10, 20, 50]
    xgb_params['classifier__learning_rate'] = [0.1]

    mlp_params = {}
    #mlp_params['classifier__hidden_layer_sizes'] =[(10, 30, 10), (20,)]
    #mlp_params['classifier__activation'] = ['logistic', 'tanh', 'relu']
    #mlp_params['classifier__alpha'] = [0.0001, 0.001, 0.005]
    #mlp_params['classifier__early_stopping'] = [True, False]

    clfs = [
        ('Random Forest', rf, rf_params),
        ('SVM', svc, svc_params),
        ('kNN', kNN, kNN_params),
        ('Decision Tree', dt, dt_params),
        ('GradBoost BDT', gbdt, gbdt_params),
        ('XGBoost BDT', xgb, xgb_params),
        ('MLP Neural Network', mlp, mlp_params)
    ]

    opt_clfs = []

    for clf_name, clf, param_grid in clfs:

        pipeline = Pipeline(steps=[
            ('scaler', StandardScaler()),
            ('classifier', clf)
        ])

        gs = GridSearchCV(pipeline, param_grid, cv=4, n_jobs=-1, scoring='f1_micro').fit(x,y)

        opt = gs.best_estimator_
        opt_clfs.append((clf_name, opt, gs.best_score_))

        print("Best parameters for {}:".format(clf_name))
        print(gs.best_params_)
        print("Best score: ", gs.best_score_)
        print("----------------------------------------------------")

    return opt_clfs
#=================================================================
# Plots the confusion matrix for a given model
# y_test: true class labels
# y_pred: predicted class labels
# returns: None, displays plot to be saved or not
#=================================================================    
def plot_cm(y_test, y_pred):

    plt.figure(figsize=(18,8))
    sns.heatmap(confusion_matrix(y_test, y_pred), annot=True, cmap='summer')
    plt.xlabel('Predicted Labels')
    plt.ylabel('True Labels')
    plt.show()

#=================================================================
# Calculates true positive rate, false positive rate for plotting 
# One vs Rest ROC curves for each class
# y_test: true class labels
# y_pred: predicted class labels
# returns: true positive rate, false positive rate
#=================================================================
def tpr_fpr(y_test, y_pred):

    cm = confusion_matrix(y_test, y_pred)

    TN = cm[0,0] #true negative
    FP = cm[0,1] #false positive
    FN = cm[1,0] #false negative
    TP = cm[1,1] #true positive

    tpr = TP/(TP+FN)
    fpr = 1 - TN/(TN+FP)

    return tpr, fpr

#=================================================================
# Get x and y axes for each ROC curve (i.e. TPR, FPR)
# y_test: true class labels
# y_prob: class probabilities
# returns TPR, FPR for each class
#=================================================================
def get_roc(y_test, y_prob):

    tpr_list = [0]
    fpr_list = [0]

    for i in range(len(y_prob)):
        thresh = y_prob[i]
        y_pred = y_prob >= thresh

        tpr, fpr = tpr_fpr(y_test, y_pred)
        tpr_list.append(tpr)
        fpr_list.append(fpr)

    return tpr_list, fpr_list

#=================================================================
# Create metric plots for each classifier type
# clf: the classifier model object
# clf_name: the name of the classifier, for labeling purposes
# X_test: input variables for the test dataset
# y_test: true class labels for the test dataset
# returns: None, displays plots to be saved or not
#=================================================================
def plot_rocs(clf, clf_name, X_test, y_test):

    classes = clf.classes_
    bins = [i/20 for i in range(20)]+[1]
    roc_auc_ovr = {}

    y_pred = clf.predict(X_test)
    y_prob = clf.predict_proba(X_test)

    for i in range(len(classes)):
        c = classes[i]

        if c == 0: title = "Distant Particles"
        elif c == 1: title = "GRB"
        elif c == 2: title = "Local Particles"
        elif c == 3: title = "Solar Flare"
        else: title = "Soft Gamma-ray Repeater"

        #define auxiliary dataframe for binarizing class labels
        df_aux = X_test.copy()
        df_aux['class'] = [1 if y==c else 0 for y in y_test]
        df_aux['prob'] = y_prob[:,i]
        df_aux = df_aux.reset_index(drop=True)

        #plot the ROC curve
        ax = plt.subplot(2, 5, i+1)

        tpr, fpr = get_roc(df_aux['class'], df_aux['prob'])

        sns.lineplot(x = fpr, y = tpr, ax = ax)
        sns.lineplot(x = [0, 1], y = [0, 1], color = 'green', ax = ax)
        plt.xlim(-0.05, 1.05)
        plt.ylim(-0.05, 1.05)
        plt.xlabel("False Positive Rate")
        plt.ylabel("True Positive Rate")
        plt.title(title)
    
        #plot the probability distribution for the class vs rest
        ax_bottom = plt.subplot(2, 5, i+6)
        sns.histplot(x="prob", data= df_aux, hue='class', color='b', ax=ax_bottom, bins=bins, stat='density')
        ax_bottom.legend(f"Class: {title}, Rest")
        ax_bottom.set_xlabel(f"P(x = {title})")

    plt.show()

#=================================================================
# Plot variable importance (how often is one variable used in decision)
# clf: classifier model object
# X_test: input variables for test dataset
# y_test: true class labels for test dataset
# feature_cols: input variable labels
# returns: None, displays plot to be saved or not
#=================================================================
def plot_importance(clf, X_test, y_test, feature_cols):

    importances = clf.feature_importances_
    result = permutation_importance(clf, X_test, y_test, n_repeats=10, random_state=42, n_jobs=2)
    #std = np.std([tree.feature_importances_ for tree in clf.estimators_], axis=0)
    mean_importances = pd.Series(result.importances_mean, index=feature_cols)

    fig, ax = plt.subplots()
    mean_importances.plot.bar(ax=ax)#yerr=std, ax=ax)
    ax.set_title("Feature Importance")
    ax.set_ylabel("Mean accuracy decrease")
    fig.tight_layout()
    plt.xticks(rotation=45)
    plt.show()

def plot_scores(y_prob, y, class_name):
    plt.figure()
    plt.hist(y_prob[y==0], bins=30, label="Rest")
    plt.hist(y_prob[y==1], bins=30, label=class_name)
    plt.xlabel('Probability')
    plt.legend()
    plt.show()

#=================================================================
# Save trained and optimized classifier model to pickle file for use later
# clf: classifier model to be saved
# name: name of the output file
# returns: None, creates pickle file
#=================================================================
def save_to_pickle(clf, name="model"):
    filename = name+".pkl"
    print("Saving model to ", filename)
    pickle.dump(clf, open(file=filename, mode='wb'))

#=================================================================
# Train the classifier
# f_in: name of input csv file
# optimize: flag to say whether to run GridSearch to optimize hyperparameters
#           and compare models
# returns: None, saves pickle file and creates performance plots 
#=================================================================
def train(f_in, optimize=True):
    encoder = LabelEncoder()

    df = pd.read_csv(f_in)

    feature_cols = ['Sun_Separation', 'Earth_Separation', 'McIlwain_L', 'Q1', 'Q2', 'Q3', 'Q4', 'Hardness_Ratio']

    X = df.loc[:, feature_cols]
    y = df.loc[:, 'GBM_Class']
    y = encoder.fit_transform(y)

    encoder_map = dict(zip(encoder.classes_, encoder.transform(encoder.classes_)))
    print(encoder_map)

    oversample = SMOTE()
    
    X,y = oversample.fit_resample(X,y)
    counter = Counter(y)

    for k, v in counter.items():
        perc = v/len(y) * 100.
        print('Class= %d, n= %d (%.3f%%)' % (k, v, perc))

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=42)

    if optimize:
        opt_clfs = optimize_model(X_train, y_train)

        for clf_name, clf, score in opt_clfs:
            y_pred = clf.predict(X_test)
            print(classification_report(y_test, y_pred, target_names=['DISTPAR', 'GRB', 'LOCLPAR', 'SFLARE', 'SGR']))
            plot_cm(y_test, y_pred)
            plot_rocs(clf, clf_name, X_test, y_test)


    else: #use most optimal model so far
        rf = RandomForestClassifier(max_depth=20, n_estimators=250, random_state=42, class_weight=None)
        gbdt = GradientBoostingClassifier(max_depth=10, n_estimators=250)
        gbdt.fit(X_train, y_train)
        plot_importance(gbdt, X_test, y_test, feature_cols)

        #plot_rocs(gbdt, "", X_test, y_test)
        y_pred = gbdt.predict(X_test)
        #plot_cm(y_test, y_pred)
        print(classification_report(y_test, y_pred, target_names=['DISTPAR', 'GRB', 'LOCLPAR', 'SFLARE', 'SGR']))

        save_to_pickle(gbdt, name="gradbdt")

#=================================================================
# Start main code body
#=================================================================
if __name__ == '__main__':
    f_in = 'gbm_classification_input.csv'
    train(f_in, optimize=False)