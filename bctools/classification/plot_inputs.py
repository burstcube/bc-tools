import matplotlib.pyplot as plt
import pandas as pd
import csv
from astropy.visualization import hist
import numpy as np

def plot(var, dfs, logy=False):

    for df in dfs:
        hist(df[var], histtype='stepfilled', alpha=0.5, label=df['GBM_Class'])
        
        if logy: plt.yscale("log")

    plt.legend(loc='upper right')
    plt.title(var)
    plt.show()

def main(alg='multiclass'):
    f_in = 'gbm_classification_input.csv'
    df = pd.read_csv(f_in)

    df_grb = df.loc[df['GBM_Class'] == 'GRB']
    df_sflare = df.loc[df['GBM_Class'] == 'SFLARE']
    df_dist = df.loc[df['GBM_Class'] == 'DISTPAR']
    df_loc = df.loc[df['GBM_Class'] == 'LOCLPAR']
    df_sgr = df.loc[df['GBM_Class'] == 'SGR']

    df_yes = df.loc[df['Is_GRB'] == 1]
    df_no = df.loc[df['Is_GRB'] == 0]

    vars = df_grb.columns

    if alg == 'multiclass':
        dfs = [df_grb, df_sflare, df_sgr]
    elif alg == 'binary':
        dfs = [df_yes, df_no]
    else:
        print("Valid algorithms are 'multiclass' and 'binary'. Please choose one of these options")
        return

    for var in vars:
        if 'GBM_Class' in var: continue
        if 'Is_GRB' in var: continue
        plot(var, dfs)


if __name__ == '__main__':
    main()