#================================================================
# process_inputs.py: Code for processing input into the BurstCube
#                    classification algorithm using GBM data
#----------------------------------------------------------------
# Author: Ava Myers (ava.myers@nasa.gov)
#================================================================

#================================================================
# Imports
#================================================================

# GBM data tools/GDT dependencies
#from gbm.finder import TriggerCatalog, TriggerFtp
#from gbm.data import Trigdat
#from gbm.plot import Spectrum, Lightcurve
#from gbm.time import Met

from gdt.missions.fermi.gbm.catalogs import TriggerCatalog
from gdt.missions.fermi.gbm.finders import TriggerFtp
from gdt.missions.fermi.gbm.trigdat import Trigdat

from gdt.core.plot.spectrum import Spectrum
from gdt.core.plot.lightcurve import Lightcurve
from gdt.missions.fermi.time import Time
from gdt.missions.fermi.mcilwainl import calc_mcilwain_l

from gdt.core.coords import SpacecraftFrame
# Generic python dependencies
import csv
from os import system
import math

# Astropy dependencies
from astropy.coordinates import SkyCoord, get_sun, Angle
from astropy.io import fits
from astropy import units as u
from astropy.time import Time

#Numpy and other science imports
import numpy as np

#================================================================
# Helper functions
#================================================================

#----------------------------------------------------------------
# Calculates the hardness ratio of a triggered event
# Default calculation just takes channel 4/channel 3
# TO DO: experiment with slicing around T90
#
# trigdat: input Trigdat file to analyze
# returns: hardness ratio calculated from trigdat
#----------------------------------------------------------------
def calculate_hr(trigdat):

    # use try/catch in case file was somehow corrupted
    try:
        #determine which detectors triggered
        trig_dets = trigdat.triggered_detectors

        #sum the counts from the triggered detectors
        summed_phaii = trigdat.sum_detectors(trig_dets)

        #create an energy spectrum from phaii
        spec = summed_phaii.to_spectrum()

        #default spec binning: [3.4-10, 10-22, 44-95, 95-300, 300-500, 800-2000]
        #hardness ratio should be N(100-300)/N(50-100)ish 

        hr = spec.rates[4]/spec.rates[3]

        return hr
    except:
        return 0.0
    
#----------------------------------------------------------------
# Get information from FSW location
#
# trigdat: input Trigdat file to analyze
# returns: RA, Dec, and top classification calculated by FSW
#----------------------------------------------------------------
def get_localization(trigdat):

    max_idx = len(trigdat._fsw_locations) - 1
    
    try: loc = trigdat._fsw_locations[max_idx]
    except: loc = trigdat._fsw_locations[max_idx - 1]

    ra = loc.location[0]
    dec = loc.location[1]
    class_id = loc.top_classification[0]
    hr = loc.hardness_ratio

    return ra, dec, class_id, hr

#----------------------------------------------------------------
# Calculate angular separation between the source location and the 
# sun/Earth
#
# trigtime: time of trigger from trigdat 
# ra: RA calculated from FSW
# dec: Dec calculated from FSW
# geocenter: the coordinates of the Earth in RA/Dec
# returns: separation from sun, separation from Earth
#----------------------------------------------------------------
def calculate_separation(trigtime, ra, dec, geocenter):
    
    #convert to isotime to create astropy Time object
    #trigtime_met = Met(trigtime)
    #trigtime_iso = trigtime_met.iso()

    time = Time(trigtime, format='fermi')

    coord_src = SkyCoord(ra=ra*u.degree, dec=dec*u.degree)
    coord_sun = get_sun(time)

    sep_sun = coord_src.separation(coord_sun).value

    coord_geo = SkyCoord(ra=geocenter.ra, dec=geocenter.dec)

    sep_geo = coord_src.separation(coord_geo).value
    
    return sep_sun, sep_geo


def geocenter_in_radec(coord):

    unit_vec = -coord/np.linalg.norm(-coord, axis=0)
    dec = np.pi/2.0 - np.arccos(unit_vec[2, np.newaxis])

    ra = np.arctan2(unit_vec[1, np.newaxis], unit_vec[0, np.newaxis])
    ra[ra < 0.0] += 2.0*np.pi

    return np.squeeze(np.rad2deg(ra)), np.squeeze(np.rad2deg(dec))
#----------------------------------------------------------------
# Calculate and assemble all training variables as input to algorithm
#
# filename: name of the Trigdat file pulled from the catalogue
# returns: array of variables and class labels 
# [Sun_Separation, Earth_Separation, McIlwain_L, quaternions, Hardness_Ratio, GBM_Class, isGRB]
#----------------------------------------------------------------
def calculate_variables(filename, debug=False):

    try: trigdat = Trigdat.open(filename)
    except: 
        #print(filename)
        return
    if debug: print("Filename",filename)
    #trigdat = Trigdat.open(filename)

    #define trigger time
    trigtime = trigdat.trigtime

    #get localization info from FSW location
    ra, dec, class_id, hr = get_localization(trigdat)
    if debug: print("Localization (RA, Dec, class):", ra, dec, class_id)

    if 'GRB' in class_id:
        class_id = 'GRB'
        origin_id = 1
    elif 'SFL' in class_id:
        class_id = 'SFLARE'
    elif ('LOCLPAR' in class_id or 'BELOWHZ' in class_id):
        class_id = 'LOCLPAR'
    elif 'DISTPAR' in class_id:
        class_id = 'DISTPAR'
    elif 'SGR' in class_id:
        class_id = 'SGR'
    else:                       # ignore TGFs, etc - BurstCube not particularly sensitive
        return
    

    #get quaternions
    quaternions = trigdat.poshist.quaternion
    #print(quaternions[0])

    q1 = quaternions[0].x
    q2 = quaternions[0].y
    q3 = quaternions[0].z
    q4 = quaternions[0].w

    hardness_ratio = calculate_hr(trigdat)
    if debug: print("Hardness ratio comparison:", hr, hardness_ratio)

    #calculate angular separation between the source and the sun/Earth
    eic = trigdat.poshist.geocenter
    time = Time(trigtime, format='fermi')
    #print(eic[0])
    #geocenter = geocenter_in_radec(eic[0])
    sep_sun, sep_geo = calculate_separation(trigtime, ra, dec, eic[0])
    if debug: print("Angular separation (sun, earth):", sep_sun, sep_geo)

    #flag for isGRB
    origin_id = 0
    frame = trigdat.poshist.at(time)

    #get McIlwain L parameter (geomagnetic coordinate) from trigdat
    lat = frame.earth_location.lat
    lon = frame.earth_location.lon
    if debug: print("Lat/Lon:", lat/u.deg, lon/u.deg)

    try: mcilwain = calc_mcilwain_l(lat/u.deg, lon/u.deg)
    except ValueError: 
        print(filename, lat, lon, class_id)
        return

    if debug: print("Geomagnetic coordinate:",  mcilwain)
    #mcilwain = calc_mcilwain_l(lat, lon)

    return [sep_sun, sep_geo, mcilwain, q1, q2, q3, q4, hardness_ratio, class_id, origin_id]

#----------------------------------------------------------------
# Get triggers from Trigger Catalog using the burst number
#----------------------------------------------------------------
def read_catalog(trig_index=''):

    trig_finder = TriggerFtp()
    cat = TriggerCatalog()

    trig_table = cat.get_table(('trigger_name',))

    trigs = sorted(trig_table['trigger_name'])

    if trig_index != '':
        index = trigs.index(trig_index)

    for trig in trigs:
        if trigs.index(trig) < index: continue
        trig_finder.set_trigger(trig.strip('bn'))
        trigdat = trig_finder.get_trigdat('trigdat/')

    system('ls trigdat/ > trigdat.txt')


def test():

    filename = 'trigdat/glg_trigdat_all_bn230815131_v01.fit'
    variables = calculate_variables(filename, debug=True)
    print(variables)

#----------------------------------------------------------------
# Main
#----------------------------------------------------------------

def main():
    f_out = open('gbm_classification_input.csv', mode='w', newline='')
    writer = csv.writer(f_out, delimiter=',')
    writer.writerow(['Sun_Separation', 'Earth_Separation', 'McIlwain_L', 'Q1', 'Q2', 'Q3', 'Q4', 'Hardness_Ratio', 'GBM_Class', 'Is_GRB'])

    f = open('trigdat.txt', mode='r')
    files = f.readlines()
    f.close()

    for file in files:
        variables = calculate_variables('trigdat/'+file.strip('\n'))

        if variables != None: writer.writerow([var for var in variables])

    f_out.close()

if __name__ == '__main__':
    main()
    #test()