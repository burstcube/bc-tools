'''
A collection of spectrum functions. 

Translate from/to MEGAlib's format
'''

import logging
logger = logging.getLogger(__name__)

from bctools.util	import units as u
from bctools.exceptions import UserError,ParsingError

import numpy as np

from numpy import log

from math import isfinite

import sys

from abc import ABC, abstractmethod

class Spectrum:

    '''
    Abstract class representing an spectral model

    Child classes need to implement eval(), integrate(), _from_megalib(), 
    to_megalib(), __str__(), __eq__() and normalize() and property flux
    '''

    @classmethod
    def from_name(cls, name, *args, **kwargs):
        '''
        Instantiate a spectrum from the subclass name. e.g.:
        :code:`Spectrum.from_name('PowerLaw', *args, **kwargs) <==> PowerLaw(*args, **kwargs)`
        '''

        try:
            subclass = getattr(sys.modules[__name__], name)
        except AttributeError:
            raise ValueError("Class 'name' is not a subclass of {}".format(cls.__name__))
            
        if not issubclass(subclass, cls):
            raise ValueError("Class 'name' is not a subclass of {}".format(cls.__name__))
        
        return subclass(*args, **kwargs)

    @abstractmethod
    def eval(self, energy):

        '''
        Evaluate the spectrum F(E)

        Args:
            energy (iterable): energy :math:`E` values

        Returns:
            array: flux :math:`F` values

        '''

    @abstractmethod
    def integrate(self, bounds):
        '''
        Evaluate the integral between two energy bounds

        Args:
           bound (array): Energy bounds. Either an array with 2 elements or 2D
               array with 2 columns

        Returns:
           float or array: Integral flux between bounds
        '''

    @abstractmethod
    def __str__(self):
        '''
        String with F(E) equation and all parameters        

        Returns:
          str
        '''

    @abstractmethod
    def __eq__(self):
        '''
        Compare two spectra

        Returns:
          eq
        '''

    @abstractmethod
    def normalize(self, flux, *args, **kwargs):
        """
        Change normalization parameters based on an integrated flux.
        """

    @property
    @abstractmethod
    def flux(self):
        """
        Total integrated flux between energy bounds
        """
        
    @classmethod
    def from_megalib(cls, params, flux):

        '''
        Get object from MEGAlibs format. 

        Args:
            params (list): List of paramters. See `str_megalib_format`
            flux (str): Flux value (In MEGAlib the spectrum refers only to the shape)
        '''

        # Note that MEGAlibs uses same units as bc-tools (keV, s, cm)

        if len(params) < 1:
            raise ParsingError("Need at least 2 paramter")
            
        stype = params[0]

        if stype == "PowerLaw":            
            return PowerLaw._from_megalib(params, flux)

        elif stype == "Mono":
            return Mono._from_megalib(params, flux)
        
        else:
            raise ValueError("{} spectrum not yet implemented".format(stype))

    @classmethod
    @abstractmethod
    def _from_megalib(cls, params, flux):
        """
        Actual implementation or MEGAlin spectrum parsing. To be overridden by
        spectrum type
        """

    @abstractmethod
    def to_megalib(self):
        '''
        Return equivalent parameters of MEGAlib's Source.Spectrum and Source.Flux

        Returns:
            list of str: Spectrum type and parameteters for Source.Spectrum
            str: Parameter for Source.Flux

        '''

    def set_elims(self, min_energy=None, max_energy=None):

        '''
        Set energy limits. Function evaluates to 0 outside of these bounds

        Limits stay the same if not specified

        Args:
            min_energy (float): lower energy bound
            max_energy (float): uppper energy bound

        '''
        
        if min_energy is not None:
            self.min_energy = min_energy

        if max_energy is not None:
            self.max_energy = max_energy

        
class PowerLaw(Spectrum):

    r'''
    :math:`F(E) = A \bigl(\frac{E}{1 \mathrm{keV}}\bigr)^{-\alpha}`
    
    Args:
        norm (float): normalization :math:`A`
        index (float): spectral index :math:`\alpha`

    Attributes:
        norm (float): normalization :math:`A`
        index (float): spectral index :math:`\alpha`
        min_energy (float): :math:`F=0` if :math:`E<` `min_energy`
        max_energy (float): :math:`F=0` if :math:`E>` `max_energy`
    
    '''

    def __init__(self, norm, index):

        self.min_energy = 0
        self.max_energy = u.inf

        self.norm = norm
        self.index = index

    def __str__(self):

        return ("F(E) = {:.2E}(E/1keV)^-{:.1f} keV^-1 cm^-2 s^-1. "
                "Range: ({:.2E} keV - {:.2E} keV)").format(self.norm,
                                                           self.index,
                                                           self.min_energy,
                                                           self.max_energy)

    def __eq__(self, other):

        return (self.norm == other.norm and
                self.index == other.index and
                self.min_energy == other.min_energy and
                self.max_energy == other.max_energy)
    
    def eval(self, energy):

        '''
        Evaluate the spectrum F(E)

        Args:
            energy (iterable): energy :math:`E` values

        Returns:
            array: flux :math:`F` values. F = 0 outside of the min/max_energy bounds

        '''

        flux = self.norm * np.array(energy)**(-self.index)

        flux[energy < self.min_energy] = 0
        flux[energy > self.max_energy] = 0

        return flux

    def integrate(self, bounds):
        
        '''
        Evaluate the integral between two energy bounds.

        Note that the spectrum evaluates to 0 out of the overall min/max_energy
        bounds.

        Args:
           bound (array): Energy bounds. Either an array with 2 elements or 2D
               array with 2 columns

        Returns:
           float or array: Integral flux between bounds
        '''

        shape = np.shape(bounds)

        if shape == (2,):
            # Single bound
            min_energy = bounds[0]
            max_energy = bounds[1]
            
            if min_energy > max_energy:
                raise ValueError("Upper bound must be greater than lower bound")

            if max_energy <= self.min_energy or min_energy >= self.max_energy:
                return 0
            
            min_energy = max(min_energy, self.min_energy)
            max_energy = min(max_energy, self.max_energy)
            
            if self.index == 1:
                return self.norm * log(max_energy/min_energy)
            else:
                return (self.norm 
                        * (max_energy**(1-self.index) 
                           - min_energy**(1-self.index)) 
                        / (1-self.index))
            
        elif len(shape) > 1 and shape[1] == 2:

            # Multiple bounds, call recursively
            return np.array([self.integrate(erange) for erange in bounds])

        else:

            raise ValueError("Shape of bounds can either be (2,) or (N,2)")

    def normalize(self, flux, min_energy = None, max_energy = None):
        """
        Change norm based on the total emission between
        min_enegy and max_energy

        Args:
            flux (float): new integral flux between min_energy and max_energy
            min_energy (float): Lower integration bound. Defaults to self.min_energy
            max_energy (float): Upper integration bound. Defaults to self.max_energy
        """

        if min_energy is None:
            min_energy = self.min_energy

        if max_energy is None:
            max_energy = self.max_energy

        self.norm = flux / self.integrate([min_energy, max_energy])

    @property
    def flux(self):

        return self.integrate([self.min_energy, self.max_energy])
        
    @classmethod
    def _from_megalib(cls, params, flux):

        '''
        Initialize from MEGAlibs format (PowerLaw spectrum)

        Args:
            params (list): List of parameters. First parameter must be "PowerLaw"
                See `str_megalib_format`
            flux (str): Flux value (In MEGAlib the spectrum refers only to the shape)
        '''

        if len(params) != 4:
            raise ParsingError("PowerLaw needs 3 parameters")

        if params[0] != "PowerLaw":
            raise ParsingError("This doesn't seem like a power law")

        flux = float(flux)
        index = float(params[3])

        min_energy = float(params[1])
        max_energy = float(params[2])

        obj = PowerLaw(1, index)
        obj.set_elims(min_energy, max_energy)
        obj.normalize(flux)

        return obj
        
    def to_megalib(self):
        '''
        Return equivalent parameters of MEGAlib's Source.Spectrum and Source.Flux

        Returns:
            list of str: Spectrum type and parameteters for Source.Spectrum \
                (i.e. PowerLaw min_energy max_energy photon_index)
            str: Parameter for Source.Flux
        '''
        
        #MEGAlib flux is the integral between min_energy - max_energy
        
        params = ["PowerLaw"]
        
        params += ["{:.17E}".format(self.min_energy)]

        if not isfinite(self.max_energy):
            raise UserError("Set finite energy limits by calling set_elims() "
                            "before calling to_megalib()")
        
        params += ["{:.17E}".format(self.max_energy)]

        params += ["{:.17E}".format(self.index)]

        # MEGAlib's flux is between min_energy and max_energy
        flux = "{:.17E}".format(self.integrate([self.min_energy, self.max_energy])
                               /u.s/u.cm2)
        
        return params,flux
            
class Mono(Spectrum):
    """
    Mono energetic beam

    Args:
        flux (float): Flux
        energy (float): Energy
    """

    def __init__(self, flux, energy):

        self._flux = flux
        self.energy = energy

        self.min_energy = 0
        self.max_energy = u.inf
        
    def __str__(self):

        return f"{self._flux:.2E} cm^-2 s^-1 at {self.energy:.2E} keV"

    def __eq__(self, other):

        return (isinstance(other, MonoE) and
                self._flux == other._flux and
                self.energy == other.energy)

    def eval(self, energy):

        energy = np.array(energy)

        non_zero = (energy >= self.min_enegy and
                    energy < self.max_energy and
                    energy == self.energy)

        flux = np.zeros(energy.shape)

        flux[non_zero] = self._flux
        
    def integrate(self, bounds):
        '''
        Evaluate the integral between two energy bounds

        Args:
           bound (array): Energy bounds. Either an array with 2 elements or 2D
               array with 2 columns

        Returns:
           float or array: Integral flux between bounds
        '''

        shape = np.shape(bounds)

        if shape == (2,):
            # Single bound
            min_energy = bounds[0]
            max_energy = bounds[1]
            
            if min_energy > max_energy:
                raise ValueError("Upper bound must be greater than lower bound")

            if (self.min_energy <= self.energy and
                self.energy < self.max_energy and
                min_energy <= self.energy and
                self.energy < max_energy):
                return self._flux
            else:
                return 0
            
        elif len(shape) > 1 and shape[1] == 2:

            # Multiple bounds, call recursively
            return np.array([self.integrate(erange) for erange in bounds])

        else:

            raise ValueError("Shape of bounds can either be (2,) or (N,2)")

    def normalize(self, flux):
        """
        Update the flux

        Args:
            flux (float): new flux
        """
        
        self._flux = flux
        
    @classmethod
    def _from_megalib(cls, params, flux):

        '''
        Initialize from MEGAlibs format (Mono spectrum)

        Args:
            params (list): List of parameters. First parameter must be "PowerLaw"
                See `str_megalib_format`
            flux (str): Flux value (In MEGAlib the spectrum refers only to the shape)
        '''

        if len(params) != 2:
            raise ParsingError("Mono needs 2 parameters")

        if params[0] != "Mono":
            raise ParsingError("This doesn't seem like a mono-energetic beam")

        flux = float(flux)
        energy = float(params[1])

        obj = cls(flux, energy)

        return obj

    def to_megalib(self):
        '''
        Return equivalent parameters of MEGAlib's Source.Spectrum and Source.Flux

        Returns:
            list of str: Spectrum type and parameteters for Source.Spectrum \
                (i.e. PowerLaw min_energy max_energy photon_index)
            str: Parameter for Source.Flux
        '''
        
        #MEGAlib flux is the integral between min_energy - max_energy
        
        params = ["Mono"]
        
        params += ["{:.17E}".format(self.energy)]

        flux = "{:.17E}".format(self._flux/u.s/u.cm2)
        
        return params,flux

