import logging
logger = logging.getLogger(__name__)

from yayc import Configurator

def set_config(config):

    if not isinstance(config, Configurator):
        raise TypeError('config must be a yayc.Configurator object')
    
    import bctools.io
    
    bctools.io._config = config

def get_config():

    import bctools.io
    
    if not hasattr(bctools.io, '_config'):
        raise RuntimeError("Config not set for bctool.io")

    return bctools.io._config
