// Materials used for all geometries

AbsorptionFileDirectory $(MEGALIB)/resource/examples/geomega/materials

Material Aluminium
Aluminium.Density               2.7
Aluminium.Component             Al  1

Material CsI
CsI.Density                     4.5
CsI.Component                   Cs  1
CsI.Component                   I   1 

Material Pb
Pb.Density               11.35
Pb.Component             Pb  1  

// This is the surround volume material -- it will ruin all simulations if turned on
// So we make sure it is noticed!
Material SurroundingSphereVolumeMaterial
SurroundingSphereVolumeMaterial.Density           10000.0
SurroundingSphereVolumeMaterial.ComponentByAtoms  Pb 1

Material Vacuum                          
Vacuum.Density                  1E-12         
Vacuum.Component                H   1 

