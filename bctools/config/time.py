from astropy.time import Time, TimeUnix

class BCMET(TimeUnix):
    name = "bcmet"
    epoch_val = "2021-01-01 00:00:00"
    epoch_scale = "tai"

class TAI(TimeUnix):
    name = "tai"
    epoch_val = "1958-01-01 00:00:00"
    epoch_scale = "tai"


