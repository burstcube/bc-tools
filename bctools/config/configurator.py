
import logging
logger = logging.getLogger(__name__)

import warnings

import yaml

from pathlib import Path

import numpy as np

from bctools.exceptions import ConfigError

import argparse

class Configurator:
    """
    Access to configuration parameters. 

    The parameters are organized in nested dictionarys. They set values
    can be a number, a string or a list. They can be accessed as e.g. 
    :code:`config['group:subgroup:subsubgroup:parameter']`
    
    Args:
        config_path (Path): Path to yaml configuration file.
    """

    def __init__(self, config_path):

        warnings.warn("bc-tool's Configurator has moved and will be removed. "
                      "Please use yayc's Configurator: https://pypi.org/project/yayc/",
                      DeprecationWarning)
        
        self.config_path = Path(config_path)
        
        logger.info("Using configuration file at %s", config_path)

        with open(self.config_path) as f:
            self._config = yaml.safe_load(f)

    def __getitem__(self, key):

        value = self._config

        keys = key.split(':')
        
        for n,key in enumerate(keys):

            try:
                value = value[key]
            except KeyError:
                raise KeyError("Parameter '{}' doesn't exists".\
                               format(":".join(keys[:n+1])))
                
        return value
        
    def __setitem__(self, key, value):

        keys = key.split(':')
        
        elem = self._config

        for n,key in enumerate(keys[:-1]):

            if key not in elem:
                raise KeyError("Parameter '{}' doesn't exists".\
                               format(":".join(keys[:n+1])))
            
            elem = elem[key]

        if keys[-1] not in elem:
            raise KeyError("Parameter '{}' doesn't exists".\
                           format(":".join(keys)))
            
        elem[keys[-1]] = value

    @staticmethod
    def config_example_path():
        """
        Get path to the example configuration file
        """

        config = Path(__file__).parent / 'bc_config.yaml'
            
        return config

    @staticmethod
    def print_config_example_path(argv = None):
        """
        Get path to the example configuration file
        """

        aPar = argparse.ArgumentParser(usage = "%(prog)s [--help]",
                                       description=("Prints the path in your "
                                                    "system to the BurstCube "
                                                    "configuration file example"))
        args = aPar.parse_args(argv)
        
        print(str(Configurator.config_example_path()))
        
    def override(self, *args):
        """
        Override one or more parameter by parsings strings of the form
        :code:`'group:subgroup:parameter = value'`. Value is parsed the same way
        as yaml would do it, so if it is a string use quotes. 

        The foreseen use case scenario is to let the uses override parameters 
        from command line e.g. :code:`--override "group:parameter = new_value"`.
        Note that the values can be override directly in code by using
        :code:`config['group:parameter'] = new_value"`.

        Args:
            args (string or array): String to parse and override parameters. 
        """
        
        if len(args) == 1:
        
            if np.isscalar(args[0]):
                # Standard, single key
                key,value = args[0].split("=")

                self[key.strip()] = yaml.safe_load(value.strip())

            else:
                #Recursive
                for arg in args[0]:
                    self.override(arg)

        else:
            #Recursive
            for arg in args:
                self.override(arg)
            

    def absolute_path(self, path):
        """
        Turn a path relative to the location of the config file absolute

        Args:
            path (Path): relative path

        Return:
            Path: absolute path
        """

        path = Path(path).expanduser()

        if path.is_absolute():
            return path
        else:
            return (self.config_path.parent / path).resolve()

    def dump(self, *args, **kwargs):
        """
        Dump the configuration contents to a file or a string.
        
        All arguments are passed to yaml.dump(). By default it returns as 
        string. You can specify a file or stream to dump it into in the
        first argument.
        """
        
        return yaml.dump(self._config, *args, **kwargs)
        
