from .configurator import Configurator

from .global_config import get_config, set_config

from pathlib import Path

config_file_example = Path(__file__).parent / 'bc_config.yaml'

def _print_config_example_path(argv = None):
    """
    Get path to the example configuration file
    """

    import argparse
    
    aPar = argparse.ArgumentParser(usage = "%(prog)s [--help]",
                                   description=("Prints the path in your "
                                                "system to the BurstCube "
                                                "configuration file example"))
    args = aPar.parse_args(argv)

    print(str(config_file_example))

