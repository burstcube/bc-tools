# bc-tools-ci contains all pre-requisites
FROM bc-tools-ci:0.1.1

COPY --chown=user:user . ./bc-tools

RUN cd bc-tools && \
    python3 setup.py install --user
